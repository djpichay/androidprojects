package kom.aswang.aswangv2.activity.roles;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.enums.Weather;

/**
 * Created by djpichay on 12/20/17.
 */
public class FishermanActivity extends NormalRoleActivity {

    CheckBox checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (!showActivity()) {
            goToNextActivity();
        }

        super.onCreate(savedInstanceState);
        setContentView(getContentView());

        LinearLayout sailingPanel = (LinearLayout) findViewById(R.id.sailing_panel);

        int random = ThreadLocalRandom.current().nextInt(1, 10);

        Context context = getContext();
        String weather;

        if (random < 5) {
            context.setWeather(Weather.GOOD);
            weather = "Maganda ang panahon sa gabing ito.";
        } else if (random >= 5 && random < 9) {
            context.setWeather(Weather.RAINY);
            weather = "Maulan sa gabing ito.";
        } else {
            context.setWeather(Weather.STORMY);
            weather = "Bumabagyo sa gabing ito.";
        }

        context.log(String.format("The weather is: %s", context.getWeather().name()));

        checkbox = new CheckBox(getApplication());

        checkbox.setText(weather + " Ikaw ba ay maglalayag para mangisda?");
        checkbox.setEnabled(true);

        sailingPanel.addView(checkbox);

    }

    @Override
    protected Role getRole() {
        return Role.MANGINGISDA;
    }

    @Override
    protected int getContentView() {
        return R.layout.fisherman;
    }

    @Override
    protected void performAction() {

        Context context = getContext();

        List<Player> players = context.getPlayers();

        if (checkbox.isChecked()) {
            for (Player player : players) {
                if (player.getRole().equals(Role.MANGINGISDA)) {
                    player.setSailing(true);
                    context.log("The fisherman sailed");
                }
            }
        }

    }

    @Override
    protected Class<?> getNextActivity() {
        return AswangActivity.class;
    }
}
