package kom.aswang.aswangv2.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import kom.aswang.aswangv2.activity.roles.StrangerActivity;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.utils.Constants;

/**
 * Created by djpichay on 12/26/17.
 */
public class LynchingActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lynching);

        if (getContext().isShepherdMourning() || getContext().getEligibleVoters().size() == 0) {
            getContext().log("Town is in a state of mourning or there are no voters present, no trials will take place");
            Intent intent = new Intent(this, DuskActivity.class);
            intent.putExtra(Constants.CONTEXT, getContext());
            startActivity(intent);
        }

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lynching_candidates_id);
        linearLayout.setGravity(Gravity.CENTER);

        LinearLayout namePanel = new LinearLayout(this);
        namePanel.setGravity(Gravity.RIGHT);
        namePanel.setOrientation(LinearLayout.VERTICAL);

        LinearLayout votePanel = new LinearLayout(this);
        votePanel.setGravity(Gravity.LEFT);
        votePanel.setOrientation(LinearLayout.VERTICAL);

        int id = 0;
        for(final Player player : getContext().getPlayers()) {

            if (!player.isAlive()) {
                continue;
            }

            LinearLayout nameLayout = new LinearLayout(this);
            nameLayout.setOrientation(LinearLayout.HORIZONTAL);
            nameLayout.setGravity(Gravity.CENTER);

            TextView playerName = new TextView(this);
            playerName.setTextColor(Color.WHITE);
            if (!getContext().getEligibleVoters().contains(player.getName())) {
                playerName.setTextColor(Color.GRAY);
            }
            playerName.setText(player.getName());
            if (player.isMayor()) {
                playerName.setText(player.getName() + " (Alkalde)");
            }
            playerName.setTextSize(20F);
            nameLayout.addView(playerName);

            Button dummyButton = new Button(this);
            dummyButton.setText("");
            dummyButton.setVisibility(View.INVISIBLE);
            nameLayout.addView(dummyButton);

            namePanel.addView(nameLayout);

            LinearLayout voteLayout = new LinearLayout(this);
            voteLayout.setOrientation(LinearLayout.HORIZONTAL);
            voteLayout.setGravity(Gravity.CENTER);

            final EditText playerVote = new EditText(this);
            playerVote.setEnabled(false);
            playerVote.setText("0");
            playerVote.setId(id++);

            final int editTextId = playerVote.getId();

            Button decrementButton = new Button(this);
            decrementButton.setTextSize(20F);
            decrementButton.setText("-");
            decrementButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int vote = Integer.parseInt(playerVote.getText().toString()) - 1;
                            if (vote < 0) {
                                vote = 0;
                            }
                            setEditTextText(editTextId, "" + vote);
                        }
                    }
            );
            voteLayout.addView(decrementButton);

            Button incrementButton = new Button(this);
            incrementButton.setTextSize(20F);
            incrementButton.setText("+");
            incrementButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setEditTextText(editTextId, "" + (Integer.parseInt(playerVote.getText().toString()) + 1));
                        }
                    }
            );

            voteLayout.addView(playerVote);

            voteLayout.addView(incrementButton);

            votePanel.addView(voteLayout);
        }

        linearLayout.addView(namePanel);
        linearLayout.addView(votePanel);

    }

    public void setEditTextText(int id, String text) {
        EditText editText = (EditText) findViewById(id);
        editText.setText(text);
    }

    public void next(View view) {

        List<String> candidates = new ArrayList<>();
        boolean isMayorAsleepOrSilenced = false;
        for (Player player : getContext().getPlayers()) {
            if (player.isAlive()) {
                if (player.isMayor() && !getContext().getEligibleVoters().contains(player.getName())) {
                    isMayorAsleepOrSilenced = true;
                }
                candidates.add(player.getName());
            }
        }

        Integer[] voteArray = new Integer[candidates.size()];
        for (int i = 0; i < candidates.size(); i++) {
            EditText editText = (EditText) findViewById(i);
            voteArray[i] = Integer.parseInt(editText.getText().toString());
        }

        int highestVote = 0;
        int totalVotes = 0;
        for (int i = 0; i < candidates.size(); i++) {
            totalVotes += voteArray[i];
            if (voteArray[i] > highestVote) {
                highestVote = voteArray[i];
            }
        }

        List<String> voters = getContext().getEligibleVoters();

        int eligibleVotes = voters.size();
        if (!isMayorAsleepOrSilenced) {
            eligibleVotes += 1;
        }
        if (totalVotes != eligibleVotes) {
            Toast.makeText(this,
                    String.format("Mali ang dami ng botong naitala. %d ang naitalang boto sa %d na inaasahan.", totalVotes, eligibleVotes),
                    Toast.LENGTH_LONG).show();
            return;
        }

        for (int i = 0; i < candidates.size(); i++) {
            if (voteArray[i] == highestVote) {
                getContext().addToConvicts(candidates.get(i));
            }
        }

        Intent intent = new Intent(this, StrangerActivity.class);
        intent.putExtra(Constants.CONTEXT, getContext());
        startActivity(intent);

    }

    public void clear(View view) {
        int numberOfCandidates = 0;
        for (Player player : getContext().getPlayers()) {
            if (player.isAlive()) {
                numberOfCandidates++;
            }
        }
        for (int i = 0; i < numberOfCandidates; i++) {
            setEditTextText(i, "0");
        }
    }

}
