package kom.aswang.aswangv2.enums;

public enum DeathCauses {

	TRIED_TO_HEAL("Sinubukang pagalingin ang aswang"),
	EATEN("Kinain ng mga aswang"),
	HANGED("Binitay ng taumbayan dahil pinaghihinalaan"),
	SHOT("Binaril ng mangangaso"),
	ILLEGAL_RECRUITMENT("Napasok ng aswang ang kulto"),
	SLICED("Ginilitan ng Barbero"),
	MELTED("Tinunaw ng Mambabarang"),
	GRIEF("Nagpakamatay sa pangungulila"),
	WOUNDS("Bumuka ang sugat"),
	UBER_FANATIC("Niligtas ang iniidolo kapalit ang kanyang buhay"),
	DROWNED("Nalunod"),
	EPIDEMIC("Namatay sa epidemya"),
	;
	
	private String cause;
	
	DeathCauses(String cause) {
		this.cause = cause;
	}
	
	public String getCause() {
		return cause;
	}
	
}
