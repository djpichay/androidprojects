package kom.aswang.aswangv2.activity.roles;

import java.util.List;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.activity.StolenRoleActivity;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;
import android.content.Intent;
import android.view.View;

public class ThiefActivity extends SpinnerRoleActivity {

	public void revealNewRole(View view) {
		switchRoles();
		ThiefActivity.this.finish();
		Intent intent = new Intent(this, StolenRoleActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private void switchRoles() {
		
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		Role stolenRole = null;

		for (Player player : players) {
			if (name.equals(player.getName())) {
				stolenRole = player.getRole();
			}
		}
		
		int i = 0;
		
		for (Player player : players) {
			if (Role.MAGNANAKAW.equals(player.getRole())) {
				player.setRole(stolenRole);
				if (Role.SAKITIN.equals(stolenRole)) {
					player.setDaysSick(1);
				}
				getContext().setThief(i);
			}
			i++;
		}
		
		getContext().log(String.format("The thief stole the role of %s. Thief is now %s", name, stolenRole.getName()));
		
	}

	@Override
	protected boolean showActivity() {
		boolean thiefIsAliveAndNotDrunk = false;
		boolean isThereDeadPeople = false;
		for (Player player : getContext().getPlayers()) {
			if (getRole().equals(player.getRole()) && player.isAlive() && !player.isDrunk()) {
				thiefIsAliveAndNotDrunk = true;
			}
			if (!isThereDeadPeople) {
				isThereDeadPeople = !player.isAlive();
			}
		}
		return thiefIsAliveAndNotDrunk && isThereDeadPeople;
	}

	@Override
	protected Role getRole() {
		return Role.MAGNANAKAW;
	}
	
	@Override
	protected int getContentView() {
		return R.layout.thief;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.thief_names_spinner;
	}

	@Override
	protected boolean isPlayerQualified(Player player) {
		return !player.isAlive();
	}

	@Override
	protected void performAction() {
		// nothing to do
	}

	@Override
	protected Class<?> getNextActivity() {
		return DoctorActivity.class;
	}

}
