package kom.aswang.aswangv2.activity.roles;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kom.aswang.aswangv2.activity.AbstractActivity;
import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

/**
 * Created by djpichay on 12/21/17.
 */
public class GamblerActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (!showActivity()) {
            nextActivity();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.gamblers);
    }

    public void next(View view) {
        this.finish();
        nextActivity();
    }

    private boolean showActivity() {
        for (Player player : getContext().getPlayers()) {
            if (Role.SUGAROL.equals(player.getRole())) {
                return getContext().isFirstTurn();
            }
        }
        return false;
    }

    private void nextActivity() {
        Intent intent = new Intent(this, DrunkActivity.class);
        intent.putExtra(Constants.CONTEXT, getContext());
        startActivity(intent);
    }

}
