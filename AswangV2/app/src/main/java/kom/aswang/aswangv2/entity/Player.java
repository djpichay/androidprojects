package kom.aswang.aswangv2.entity;

import java.io.Serializable;

import kom.aswang.aswangv2.enums.DeathCauses;
import kom.aswang.aswangv2.enums.Role;

public class Player implements Serializable {
	
	private static final long serialVersionUID = -5790210215778260433L;
	
	private int id;
	private String name;
	private Role role;
	private boolean mayor;
	private boolean alive;
	private boolean drunk;
	private boolean healed;
	private boolean recruited;
	private boolean asleep;
	private boolean predicted;
	private boolean bleeding;
	private boolean silenced;
	private boolean sailing;
	private int daysSick;
	private DeathCauses causeOfDeath;
	
	public Player(int id, Role role) {
		this.id = id;
		this.role = role;
		this.mayor = false;
		this.alive = true;
		this.daysSick = 0;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public boolean isMayor() {
		return mayor;
	}
	public void setMayor(boolean mayor) {
		this.mayor = mayor;
	}
	public boolean isAlive() {
		return alive;
	}
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	public boolean isDrunk() {
		return drunk;
	}
	public void setDrunk(boolean drunk) {
		this.drunk = drunk;
	}
	public boolean isHealed() {
		return healed;
	}
	public void setHealed(boolean healed) {
		this.healed = healed;
	}
	public boolean isRecruited() {
		return recruited;
	}
	public void setRecruited(boolean recruited) {
		this.recruited = recruited;
	}
	public boolean isAsleep() {
		return asleep;
	}
	public void setAsleep(boolean asleep) {
		this.asleep = asleep;
	}
	public boolean isPredicted() {
		return predicted;
	}
	public void setPredicted(boolean predicted) {
		this.predicted = predicted;
	}
	public boolean isBleeding() {
		return bleeding;
	}
	public void setBleeding(boolean bleeding) {
		this.bleeding = bleeding;
	}
	public boolean isSilenced() {
		return silenced;
	}
	public void setSilenced(boolean silenced) {
		this.silenced = silenced;
	}
	public boolean isSailing() { return sailing; }
	public void setSailing(boolean sailing) { this.sailing = sailing; }
	public int getDaysSick() {
		return daysSick;
	}
	public void setDaysSick(int daysSick) {
		this.daysSick = daysSick;
	}
	public DeathCauses getCauseOfDeath() {
		return causeOfDeath;
	}
	public void setCauseOfDeath(DeathCauses causeOfDeath) {
		this.causeOfDeath = causeOfDeath;
	}
	
}
