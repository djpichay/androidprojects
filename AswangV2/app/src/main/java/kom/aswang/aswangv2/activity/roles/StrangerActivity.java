package kom.aswang.aswangv2.activity.roles;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.activity.AbstractActivity;
import kom.aswang.aswangv2.activity.DuskActivity;
import kom.aswang.aswangv2.activity.TieActivity;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

public class StrangerActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		Player stranger = getStranger();
		if (null == stranger) {
			nextActivity(TieActivity.class);
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stranger);
		
		if (null != stranger) {
			getContext().setConvicted(stranger.getName());
		
			LinearLayout linearLayout = (LinearLayout) findViewById(R.id.stranger_id);

			for (Player player : getContext().getPlayers()) {
				if (!Role.ESTRANGHERO.equals(player.getRole()) && player.isAlive()) {
					CheckBox checkbox = new CheckBox(getApplication());
					checkbox.setText(player.getName());
					checkbox.setTextColor(Color.WHITE);
					checkbox.setId(Math.abs(player.getName().hashCode()));
					linearLayout.addView(checkbox);
				}
			}
		
		}
	
	}
	
	public void next(View view) {
		
		Context context = getContext();
		
		for (Player player : context.getPlayers()) {
			if (!Role.ESTRANGHERO.equals(player.getRole()) && player.isAlive()) {
				CheckBox checkbox = (CheckBox) findViewById(Math.abs(player.getName().hashCode()));
				if (checkbox.isChecked()) {
					context.log(String.format("Player %s will vote next trial", player.getName()));
					player.setSilenced(false);
				} else {
					context.log(String.format("Player %s will not vote next trial", player.getName()));
					player.setSilenced(true);
				}
			}
		}
		
		nextActivity(DuskActivity.class);
	}
	
	private Player getStranger() {
		
		if (getContext().getConvicts().size() == 1) {
			return null;
		}
		
		for (Player player : getContext().getPlayers()) {
			if (Role.ESTRANGHERO.equals(player.getRole())) {
				if (player.isAlive()) {
					getContext().log("There is a tie vote, stranger still alive, will be killed instead");
					return player;
				}
			}
		}
		
		return null;
		
	}
	
	private void nextActivity(Class<?> clazz) {
		this.finish();
		Intent intent = new Intent(this, clazz);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	
}
