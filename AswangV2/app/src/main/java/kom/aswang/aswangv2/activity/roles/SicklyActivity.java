package kom.aswang.aswangv2.activity.roles;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.DeathCauses;
import kom.aswang.aswangv2.enums.Role;

/**
 * Created by djpichay on 12/23/17.
 */
public class SicklyActivity extends NormalRoleActivity {

    private boolean sicklyWasDead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (sicklyWasDead) {
            TextView sicklyText = (TextView) findViewById(R.id.sickly_text);
            sicklyText.setText("Nagsimula ng kumalat ang epidemya.");
        }
    }

    @Override
    protected Role getRole() {
        return Role.SAKITIN;
    }

    @Override
    public void next(View view) {
        super.next(view);
    }

    @Override
    protected int getContentView() {
        return R.layout.sickly;
    }

    @Override
    protected Class<?> getNextActivity() {
        return HunterActivity.class;
    }

    @Override
    protected void performAction() {
        // nothing to do
    }

    @Override
    protected boolean showActivity() {
        List<Player> players = getContext().getPlayers();
        int infectedNeighbors = 0;
        for (Player player : players) {
            if (getRole().equals(player.getRole()) && getContext().isFirstTurn()) {
                player.setDaysSick(1);
            } else {
                if (player.getDaysSick() > 0 && player.isAlive()) {
                    player.setDaysSick(player.getDaysSick() + 1);
                }
            }
        }
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            if (player.getDaysSick() > 0 && player.isAlive()) {
                if (willSuccumbToIllness(player)) {
                    infectedNeighbors += infectNeighbors(players, i);
                }
            }
        }
        return infectedNeighbors > 0;
    }

    private int infectNeighbors(List<Player> players, int infectedIndex) {
        int left = infectedIndex - 1;
        if (left < 0) {
            left = players.size() - 1;
        }
        int right = infectedIndex + 1;
        if (right >= players.size()) {
            right = 0;
        }
        int infected = 0;
        Player leftPlayer = players.get(left);
        if (leftPlayer.isAlive() && leftPlayer.getDaysSick() == 0) {
            getContext().log(String.format("Player %s has been infected", leftPlayer.getName()));
            leftPlayer.setDaysSick(1);
            infected++;
        }
        Player rightPlayer = players.get(right);
        if (rightPlayer.isAlive() && rightPlayer.getDaysSick() == 0) {
            getContext().log(String.format("Player %s has been infected", rightPlayer.getName()));
            rightPlayer.setDaysSick(1);
            infected++;
        }
        return infected;
    }

    private boolean willSuccumbToIllness(Player player) {
        boolean death = false;
        if (player.getDaysSick() == 1) {
            return death;
        }
        double probability = 7D * Math.pow(2, player.getDaysSick());
        if (probability > 100) {
            probability = 90;
        }
        if (Role.ASWANG.equals(player.getRole())) {
            probability = probability / 3D;
        }
        int random = ThreadLocalRandom.current().nextInt(1, 101);
        death = (random <= probability);
        if (death) {
            getContext().log(String.format("Player %s has succumbed to the epidemic", player.getName()));
            getContext().addToRecentlyDeceased(player.getName());
            player.setAlive(false);
            player.setCauseOfDeath(DeathCauses.EPIDEMIC);
            if (Role.SAKITIN.equals(player.getRole())) {
                sicklyWasDead = true;
            }
            if (Role.MANGANGASO.equals(player.getRole())) {
                getContext().setHuntersRevenge(true);
                getContext().log(String.format("Player killed is a hunter", player.getName()));
            }
        }
        return death;
    }


}
