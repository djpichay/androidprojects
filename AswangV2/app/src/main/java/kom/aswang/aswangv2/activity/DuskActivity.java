package kom.aswang.aswangv2.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kom.aswang.aswangv2.activity.roles.MayorActivity;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.DeathCauses;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

public class DuskActivity extends AbstractActivity {

	boolean done = false;
	boolean snapElection = false;
	boolean coupleSuicide = false;
	boolean fanaticSave = false;
	boolean hunterRevenge = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dusk);
		
		Context context = getContext();

		if (context.isShepherdMourning()) {
			TextView duskTextView = (TextView) findViewById(R.id.dusk_text);
			duskTextView.setText("Ang buong bayan ay nakikiramay sa pagkamatay ng Pastol, walang paglilitis na gagawin");
			context.setShepherdMourning(false);
		} else if (context.getEligibleVoters().size() == 0) {
			TextView duskTextView = (TextView) findViewById(R.id.dusk_text);
			duskTextView.setText("Ang buong bayan ay isinumpa ng estranghero. Walang paglilitis na gagawin.");
		} else {
			lynching();
		}
		
		context.setDay(false);
		
	}
	
	public void next(View view) {
		this.finish();
		Intent intent = new Intent(this, NightActivity.class);
		if (done) {
			intent = new Intent(this, SummaryActivity.class);
		} else if (snapElection) {
			intent = new Intent(this, MayorActivity.class);
		}
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private void lynching() {
		
		Context context = getContext();
		
		TextView duskTextView = (TextView) findViewById(R.id.dusk_text);
		duskTextView.setText("Ang bibitayin para sa araw na ito ay si...");
		
		TextView executedTextView = (TextView) findViewById(R.id.executed_text);
		executedTextView.setText(context.getConvicted());
		
		String artistName = "";
		String fanaticName = "";
		String hunterVictim = "";
		
		for (Player player : context.getPlayers()) {
			if (context.getConvicted().equals(player.getName()) || context.getRecentlyDeceased().contains(player.getName())) {
				player.setAlive(false);
				if (context.getConvicted().equals(player.getName())) {
					player.setCauseOfDeath(DeathCauses.HANGED);
				}
				if (context.getRecentlyDeceased().contains(player.getName())) {
					player.setAlive(false);
					hunterRevenge = true;
					hunterVictim = player.getName();
					context.getRecentlyDeceased().clear();
				}
				if (player.isMayor()) {
					snapElection = true;
					context.log(String.format("Mayor %s is killed! Call for snap election.", player.getName()));
				}
				if (Role.MAGKASINTAHAN.equals(player.getRole())) {
					coupleSuicide = true;
					context.log(String.format("One of the lovers (%s) is killed.", player.getName()));
				}
				if (Role.PASTOL.equals(player.getRole())) {
					context.setShepherdMourning(true);
					context.log(String.format("Shepherd %s is killed. Town will be in a state of mourning", player.getName()));
				}
				if (Role.ARTISTA.equals(player.getRole())) {
					if (player.isMayor()) {
						snapElection = false;
						context.log(String.format("Mayor %s is not killed!", player.getName()));
					}
					for (Player rPlayer : context.getPlayers()) {
						if (Role.PANATIKO.equals(rPlayer.getRole()) && rPlayer.isAlive()) {
							player.setAlive(true);
							player.setCauseOfDeath(null);
							rPlayer.setAlive(false);
							rPlayer.setCauseOfDeath(DeathCauses.UBER_FANATIC);
							context.log(String.format("The artist %s was saved by the fanatic %s", player.getName(), rPlayer.getName()));
							if (rPlayer.isMayor()) {
								snapElection = true;
								context.log(String.format("Mayor %s is killed! Call for snap election.", rPlayer.getName()));
							}
							fanaticSave = true;
							artistName = player.getName();
							fanaticName = rPlayer.getName();
						}
					}
				}
				if (Role.ABOGADO.equals(player.getRole())) {
					if (player.isMayor()) {
						snapElection = false;
						context.log(String.format("Mayor %s is not killed!", player.getName()));
					}
					player.setAlive(true);
					player.setCauseOfDeath(null);
					duskTextView.setText(String.format("Si %s ay isang abogado. Makakalusot siya sa paglilitis.", context.getConvicted()));
					executedTextView.setText("Walang bibitayin sa araw na ito.");
					context.log(String.format("Atty. %s will not be killed and will escape justice", player.getName()));
				}
			}
		}
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.dusk_id);
		
		if (hunterRevenge) {
			TextView hunterText = new TextView(getApplication());
			hunterText.setText(String.format("Binaril ng mangangaso si %s bago siya ay tuluyang mamatay", hunterVictim));
			hunterText.setTextColor(Color.WHITE);
			hunterText.setTextSize(26F);
			hunterText.setGravity(Gravity.CENTER);
			linearLayout.addView(hunterText);
		}
		
		if (fanaticSave) {
			TextView fanaticText = new TextView(getApplication());
			fanaticText.setText(String.format("Ngunit ang artistang si %s ay nailigtas sa pagkamatay ng kanyang panatikong si %s.", artistName, fanaticName));
			fanaticText.setTextColor(Color.WHITE);
			fanaticText.setTextSize(26F);
			fanaticText.setGravity(Gravity.CENTER);
			linearLayout.addView(fanaticText);
		}
		
		if (coupleSuicide) {
			for (Player player : context.getPlayers()) {
				if (Role.MAGKASINTAHAN.equals(player.getRole()) && player.isAlive()) {
					player.setAlive(false);
					player.setCauseOfDeath(DeathCauses.GRIEF);
					TextView suicideText = new TextView(getApplication());
					suicideText.setText("Si " + player.getName() + " ay nagpakamatay sa pighati sa pagkamatay ng kanyang kasintahan...");
					suicideText.setTextColor(Color.WHITE);
					suicideText.setTextSize(26F);
					suicideText.setGravity(Gravity.CENTER);
					linearLayout.addView(suicideText);
					context.log(String.format("The other lover (%s) commits suicide because of grief.", player.getName()));
				}
			}
		}
		
		int aliveVillagers = 0;
		int aliveAswangs = 0;
		
		for (Player player : context.getPlayers()) {
			if (player.isAlive()) {
				if (Role.ASWANG.equals(player.getRole())) {
					aliveAswangs++;
				} else {
					aliveVillagers++;
				}
			}
		}
		
		TextView endTextView = (TextView) findViewById(R.id.dusk_end_text);
		endTextView.setTextSize(26F);
		
		if (aliveAswangs >= aliveVillagers) {
			done = true;
			endTextView.setText("Tagumpay para sa mga Aswang!");
			context.log(String.format("The aswangs outnumber the villagers, %d to %d", aliveAswangs, aliveVillagers));
		} else if (aliveAswangs == 0) {
			done = true;
			endTextView.setText("Tagumpay para sa mga Taumbayan!");
			context.log("The aswangs are eradicated");
		}
	}
	
}
