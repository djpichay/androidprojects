package kom.aswang.aswangv2.activity;

import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.utils.Constants;
import kom.aswang.aswangv2.utils.ShuffleUtil;

public class Main extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void additionalCharacters(View view) {
		Intent intent = new Intent(this, AdditionalCharacters.class);
		try {
			intent.putExtra(Constants.CONTEXT, initContext());
	        startActivity(intent);
		} catch(Exception e) {
			Toast.makeText(this, "Maglagay ng bilang ng manlalaro at aswang", Toast.LENGTH_SHORT).show();		
		}
	}
	
	public void shuffle(View view) {
		Intent intent = new Intent(this, PlayerActivity.class);
		try {
			Context context = ShuffleUtil.shuffle(initContext());
			context.clearRoles();
			intent.putExtra(Constants.CONTEXT, context);
			startActivity(intent);
		} catch(Exception e) {
			Toast.makeText(this, "Maglagay ng bilang ng manlalaro at aswang", Toast.LENGTH_SHORT).show();		
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private Context initContext() throws Exception {
		Context context = new Context();
		context.setNumAswangs(this.getIntegerValueFromTextField(R.id.bilang_ng_aswang_text));
		context.setNumPlayers(this.getIntegerValueFromTextField(R.id.bilang_ng_manlalaro_text));
		context.setDay(getBooleanFromCheckbox(R.id.umaga_gabi_checkbox));
		context.log(String.format("Moderator picked %d players with %d aswangs", context.getNumPlayers(), context.getNumAswangs()));
		context.log("The game will start on a " + (context.isDay() ? "day" : "night"));
		return context;
	}

}
