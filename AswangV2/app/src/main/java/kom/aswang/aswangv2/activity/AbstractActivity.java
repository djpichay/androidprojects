package kom.aswang.aswangv2.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.utils.Constants;

public class AbstractActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	protected String getStringValueFromTextField(int id) {
		EditText editText = (EditText) findViewById(id);
		Editable stringText = editText.getText();
        return stringText.toString();
	}
	
	protected Integer getIntegerValueFromTextField(int id) throws Exception {
		EditText integerText = (EditText) findViewById(id);
        Integer integerValue = Integer.valueOf(integerText.getText().toString());
        return integerValue;
	}
	
	protected void setTextToTextField(int id, String text) {
		TextView textView = (TextView) findViewById(id);
		textView.setText(text);
	}
	
	protected Boolean getBooleanFromCheckbox(int id) {
		CheckBox checkbox = (CheckBox) findViewById(id);
		return checkbox.isChecked();
	}
	
	protected Context getContext() {
		Context context = (Context) getIntent().getSerializableExtra(Constants.CONTEXT);
		return context;
	}

	@Override
	public void onBackPressed() {
		// do nothing
	}
	
}
