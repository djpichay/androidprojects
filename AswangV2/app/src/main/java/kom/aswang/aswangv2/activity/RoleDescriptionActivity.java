package kom.aswang.aswangv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

/**
 * Created by djpichay on 12/22/17.
 */
public class RoleDescriptionActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.role_description);
        Role role = (Role) getIntent().getSerializableExtra(Constants.ROLE);
        setTextToTextField(R.id.desc_papel_text, role.getName());
        setTextToTextField(R.id.desc_papel_desc, getDescription(role.getDescriptionId()));
    }

    public void back(View view) {
        RoleDescriptionActivity.this.finish();
        Intent intent = new Intent(this, AdditionalCharacters.class);
        intent.putExtra(Constants.CONTEXT, getContext());
        startActivity(intent);
    }

    private String getDescription(int roleId) {
        String description = getResources().getString(roleId);
        return description;
    }

}
