package kom.aswang.aswangv2.activity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kom.aswang.aswangv2.activity.roles.MayorActivity;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.DeathCauses;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.enums.Weather;
import kom.aswang.aswangv2.utils.Constants;

public class DawnActivity extends AbstractActivity {

	boolean done = false;
	boolean snapElection = false;
	boolean coupleSuicide = false;
	boolean fanaticSave = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dawn);
		
		Context context = getContext();
		
		List<String> recentlyDeceased = context.getRecentlyDeceased();
		
		TextView textView = (TextView) findViewById(R.id.dawn_text);
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.dawn_id);

		if (null != context.getWeather()) {
			int random = ThreadLocalRandom.current().nextInt(1, 10);
			Player fisherman = null;
			for (Player player : context.getPlayers()) {
				if (player.getRole().equals(Role.MANGINGISDA)) {
					fisherman = player;
				}
			}
			if (context.getWeather().equals(Weather.RAINY)) {
				if (random < 6) {
					if (fisherman.isSailing()) {
						context.log(String.format("Fisherman %s has drowned due to rainy weather.", fisherman.getName()));
						context.addToRecentlyDeceased(fisherman.getName());
						fisherman.setCauseOfDeath(DeathCauses.DROWNED);
						fisherman.setAlive(false);
					}
				}
			} else if (context.getWeather().equals(Weather.STORMY)) {
				if (random > 2) {
					if (fisherman.isSailing()) {
						context.log(String.format("Fisherman %s has drowned due to stormy weather.", fisherman.getName()));
						context.addToRecentlyDeceased(fisherman.getName());
						fisherman.setCauseOfDeath(DeathCauses.DROWNED);
						fisherman.setAlive(false);
					}
				}
			}
			fisherman.setSailing(false);
		}

		if (context.isTailorBleeding()) {
			for (Player player : context.getPlayers()) {
				if (Role.MANANAHI.equals(player.getRole()) && player.isAlive()) {
					if (player.isBleeding()) {
						context.addToRecentlyDeceased(player.getName());
						player.setCauseOfDeath(DeathCauses.WOUNDS);
						context.log(String.format("Tailor %s died because the stitches failed to completely close the wounds", player.getName()));
						context.setTailorBleeding(false);
					} else {
						player.setBleeding(true);
					}
				}
			}
		}
		
		if (recentlyDeceased.size() > 0) {
			textView.setText("Ang mga pumanaw sa nagdaang gabi...");
			for (String dead: recentlyDeceased) {
				TextView deadText = new TextView(getApplication());
				deadText.setText(dead);
				deadText.setTextColor(Color.WHITE);
				deadText.setTextSize(26F);
				deadText.setGravity(Gravity.CENTER);
				linearLayout.addView(deadText);
			}
		} else {
			textView.setText("Isang mapayapang gabi ang dumaan...");
		}
		
		int aliveVillagers = 0;
		int aliveAswangs = 0;
		
		String sleepingPlayer = "";
		String fanaticName = "";
		String artistName = "";
		
		context.clearVotingSlate();
		
		for (Player player : context.getPlayers()) {
			if (recentlyDeceased.contains(player.getName())) {
				player.setAlive(false);
				if (player.isMayor()) {
					snapElection = true;
					context.log(String.format("Mayor %s is killed! Call for snap election.", player.getName()));
				} 
				if (Role.MAGKASINTAHAN.equals(player.getRole())) {
					coupleSuicide = true;
					context.log(String.format("One of the lovers (%s) is killed.", player.getName()));
				}
				if (Role.PASTOL.equals(player.getRole())) {
					context.setShepherdMourning(true);
					context.log(String.format("Shepherd %s is killed. Town will be in a state of mourning", player.getName()));
				}
				if (Role.ARTISTA.equals(player.getRole())) {
					for (Player rPlayer : context.getPlayers()) {
						if (Role.PANATIKO.equals(rPlayer.getRole()) && rPlayer.isAlive()) {
							player.setAlive(true);
							player.setCauseOfDeath(null);
							rPlayer.setAlive(false);
							rPlayer.setCauseOfDeath(DeathCauses.UBER_FANATIC);
							context.log(String.format("The artist %s was saved by the fanatic %s", player.getName(), rPlayer.getName()));
							if (rPlayer.isMayor()) {
								snapElection = true;
								context.log(String.format("Mayor %s is killed! Call for snap election.", rPlayer.getName()));
							}
							fanaticSave = true;
							artistName = player.getName();
							fanaticName = rPlayer.getName();
						}
					}
				}
			}
		}
		
		if (fanaticSave) {
			TextView fanaticText = new TextView(getApplication());
			fanaticText.setText(String.format("Ngunit ang artistang si %s ay nailigtas sa pagkamatay ng kanyang panatikong si %s.", artistName, fanaticName));
			fanaticText.setTextColor(Color.WHITE);
			fanaticText.setTextSize(26F);
			fanaticText.setGravity(Gravity.CENTER);
			linearLayout.addView(fanaticText);
		}
		
		if (coupleSuicide) {
			for (Player player : context.getPlayers()) {
				if (Role.MAGKASINTAHAN.equals(player.getRole()) && player.isAlive()) {
					player.setAlive(false);
					player.setCauseOfDeath(DeathCauses.GRIEF);
					TextView suicideText = new TextView(getApplication());
					suicideText.setText("Si " + player.getName() + " ay nagpakamatay sa pighati sa pagkamatay ng kanyang kasintahan...");
					suicideText.setTextColor(Color.WHITE);
					suicideText.setTextSize(26F);
					suicideText.setGravity(Gravity.CENTER);
					linearLayout.addView(suicideText);
					context.log(String.format("The other lover (%s) commits suicide because of grief.", player.getName()));
				}
			}
		}
		
		List<String> suspectList = new ArrayList<String>();
		
		for (Player player : context.getPlayers()) {
			if (player.isAlive()) {
				suspectList.add(player.getName());
				if (Role.ASWANG.equals(player.getRole())) {
					aliveAswangs++;
				} else {
					aliveVillagers++;
				}
				if (player.isAsleep()) {
					sleepingPlayer = player.getName();
					player.setAsleep(false);
				} else if (player.isSilenced()) {
					player.setSilenced(false);
				} else {
					context.addEligibleVoter(player.getName());
				}
			}
			player.setDrunk(false);
			player.setHealed(false);
			player.setPredicted(false);
		}
		
		context.setSuspects(suspectList.toArray(new String[suspectList.size()]));
		
		TextView endTextView = (TextView) findViewById(R.id.end_text);
		endTextView.setTextSize(26F);
		
		if (aliveAswangs >= aliveVillagers) {
			done = true;
			endTextView.setText("Tagumpay para sa mga Aswang!");
			context.log(String.format("The aswangs outnumber the villagers, %d to %d", aliveAswangs, aliveVillagers));
		} else if (aliveAswangs == 0) {
			done = true;
			endTextView.setText("Tagumpay para sa mga Taumbayan!");
			context.log("The aswangs are eradicated");
		} else {
			String newDawn = "Imulat ang mata para sa isang bagong umaga...";
			if (!sleepingPlayer.equals("")) {
				newDawn += " Maliban kay " + sleepingPlayer + " na naakit at napagod.."; 
			}
			endTextView.setText(newDawn);
			if (isGossipAlive()) {
				context.log("Gossip is still alive. Generating latest rumor.");
				TextView gossipView = (TextView) findViewById(R.id.gossip_text);
				gossipView.setTextSize(26F);
				gossipView.setText(generateGossip());
			}
			context.log("The game will go on, there are still more villagers");
		}
		
		context.clearForAnotherDay();
		context.setDay(true);
		if (context.isFirstTurn()) {
			context.setFirstTurn(false);
		}
		
	}

	private boolean isGossipAlive() {
		for (Player player : getContext().getPlayers()) {
			if (Role.TSISMOSA.equals(player.getRole())) {
				return player.isAlive();
			}
		}
		return false;
	}

	private String generateGossip() {
		List<String> availablePlayers = new ArrayList<>();
		List<Role> availableRoles = new ArrayList<>();
		Set<Role> uniqueRoles = new HashSet<>();
		for (Player player : getContext().getPlayers()) {
			if (!Role.TSISMOSA.equals(player.getRole())) {
				availablePlayers.add(player.getName());
				availableRoles.add(player.getRole());
				uniqueRoles.add(player.getRole());
			}
		}
		int denominator = ThreadLocalRandom.current().nextInt(5, 7);
		int random = ThreadLocalRandom.current().nextInt(1, denominator);
		int randomPlayerIndex = ThreadLocalRandom.current().nextInt(0, availablePlayers.size());
		String gossipTopic = availablePlayers.get(randomPlayerIndex);
		String gossip = "";
		Role gossipRole = Role.TAUMBAYAN;
		if (random == 1) {
			// tell a true gossip
			getContext().log("Telling a true gossip.");
			gossipRole = availableRoles.get(randomPlayerIndex);
			if (gossipRole.getNumber() > 1) {
				gossip = "isa sa mga " + gossipRole.getName();
			} else {
				gossip = "isang " + gossipRole.getName();
			}
		} else {
			// tell a fake gossip
			getContext().log("Telling a fake gossip.");
			if (uniqueRoles.size() <4) {
				int randomRoleIndex = ThreadLocalRandom.current().nextInt(0, availableRoles.size());
				gossipRole = availableRoles.get(randomRoleIndex);
			} else {
				do {
					int randomRoleIndex = ThreadLocalRandom.current().nextInt(0, uniqueRoles.size());
					int i = 0;
					for (Role r : uniqueRoles) {
						if (i++ == randomRoleIndex) {
							gossipRole = r;
						}
					}
				} while (gossipRole.equals(availableRoles.get(randomPlayerIndex)));
			}
			if (gossipRole.getNumber() > 1) {
				gossip = "isa sa mga " + gossipRole.getName();
			} else {
				gossip = "isang " + gossipRole.getName();
			}
		}
		return String.format("Ayon sa Tsismosa si %s ay %s", gossipTopic, gossip);
	}
	
	public void next(View view) {
		this.finish();
		Intent intent = new Intent(this, LynchingActivity.class);
		if (done) {
			intent = new Intent(this, SummaryActivity.class);
		} else if (snapElection) {
			intent = new Intent(this, MayorActivity.class);
		}
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	
}
