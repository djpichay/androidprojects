package kom.aswang.aswangv2.enums;

import kom.aswang.aswangv2.activity.R;

public enum Role {

	ASWANG("Aswang"),
	TAUMBAYAN("Taumbayan"),
	ABOGADO("Abogado", true, R.string.abogado_desc),
	ALBULARYO("Albularyo", true, R.string.albularyo_desc),
	ARTISTA("Artista"), 
	BARBERO("Barbero", true, R.string.barbero_desc),
	BATANG_ASWANG("Batang Aswang", true, R.string.batang_aswang_desc),
	ESTRANGHERO("Estranghero", true, R.string.estranghero_desc),
	LASENGGO("Lasenggo", true, R.string.lasenggo_desc),
	MAGKASINTAHAN("Magkasintahan", true, 2, R.string.magkasintahan_desc),
	MAGNANAKAW("Magnanakaw", true, R.string.magnanakaw_desc),
	MAGSASAKA("Magsasaka", true, R.string.magsasaka_desc),
	MAMBABARANG("Mambabarang", true, R.string.mangkukulam_desc),
	MANANAHI("Mananahi", true, R.string.mananahi_desc),
	MANGAALIW("Mang-aaliw", true, R.string.mangaaliw_desc),
	MANGANGASO("Mangangaso", true, R.string.mangangaso_desc),
	MANGHUHULA("Manghuhula", true, R.string.manghuhula_desc),
	MANGINGISDA("Mangingisda", true, R.string.mangingisda_desc),
	PANATIKO("Panatiko", true, R.string.panatiko_desc),
	PASTOL("Pastol", true, R.string.pastol_desc),
	PINUNO_NG_KULTO("Pinuno ng Kulto"),
	SAKITIN("Sakitin", true, R.string.sakitin_desc),
	SUGAROL("Sugarol", true, 3, R.string.sugarol_desc),
	TSISMOSA("Tsismosa", true, R.string.tsismosa_desc),
	;
	
	private String name;
	
	private int component = 0;
	
	private int number = 1;

	private int descriptionId;
	
	Role(String name) {
		this.name = name;
	}
	
	Role(String name, boolean addCheckbox, int descriptionId) {
		this.name = name;
		this.descriptionId = descriptionId;
		if (addCheckbox) {
			component = name.hashCode();
			component = Math.abs(component);
		}
	}
	
	Role(String name, boolean addCheckbox, int number, int descriptionId) {
		this(name, addCheckbox, descriptionId);
		this.number = number;
	}
	
	public String getName() {
		return name;
	}
	
	public int getComponent() {
		return component;
	}

	public int getNumber() {
		return number;
	}

	public int getDescriptionId() { return descriptionId; }

}
