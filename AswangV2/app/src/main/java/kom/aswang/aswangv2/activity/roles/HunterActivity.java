package kom.aswang.aswangv2.activity.roles;

import java.util.List;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.activity.DawnActivity;
import kom.aswang.aswangv2.activity.DuskActivity;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.DeathCauses;
import kom.aswang.aswangv2.enums.Role;

public class HunterActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.MANGANGASO;
	}

	@Override
	protected int getContentView() {
		return R.layout.hunter;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.hunter_names_spinner;
	}
	
	@Override
	protected boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (getRole().equals(player.getRole()) && getContext().isHuntersRevenge()) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected boolean isPlayerQualified(Player player) {
		return player.isAlive() && !getRole().equals(player.getRole()) && !isFishermanSailing(player);
	}

	@Override
	protected void performAction() {
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				context.addToRecentlyDeceased(player.getName());
				context.setHuntersRevenge(false);
				player.setCauseOfDeath(DeathCauses.SHOT);
				context.log(String.format("Player %s is shot by the hunter", name));
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		if (getContext().isDay()) {
			return DuskActivity.class;
		}
		return DawnActivity.class;
	}

}
