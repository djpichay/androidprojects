package kom.aswang.aswangv2.activity.roles;

import java.util.List;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;

public class ProstituteActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.MANGAALIW;
	}
	
	@Override
	protected int getContentView() {
		return R.layout.hooker;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.prosti_names_spinner;
	}

	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())
					&& !player.isHealed()
					&& !isBarberGoingToKill(player)
					&& !isFishermanSailing(player)) {
				player.setAsleep(true);
				getContext().log(String.format("The prostitute seduced %s. Will be asleep for next day", name));
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		return SicklyActivity.class;
	}

}
