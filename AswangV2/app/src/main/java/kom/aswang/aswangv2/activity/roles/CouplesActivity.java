package kom.aswang.aswangv2.activity.roles;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.activity.AbstractActivity;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

public class CouplesActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		if (!showActivity()) {
			nextActivity();
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.couples);
	}
	
	public void next(View view) {
		this.finish();
		nextActivity();
	}
	
	private boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (Role.MAGKASINTAHAN.equals(player.getRole())) {
				return getContext().isFirstTurn();
			}
		}
		return false;
	}
	
	private void nextActivity() {
		Intent intent = new Intent(this, GamblerActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
}
