package kom.aswang.aswangv2.activity.roles;

import java.util.List;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.activity.PredictionActivity;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;

public class SeerActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.MANGHUHULA;
	}

	@Override
	protected int getContentView() {
		return R.layout.seer;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.seer_names_spinner;
	}

	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				player.setPredicted(true);
				getContext().log(String.format("Seer guesses player %s role", name));
				isBarberGoingToKill(player);
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		return PredictionActivity.class;
	}

}
