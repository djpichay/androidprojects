package kom.aswang.aswangv2.activity;

import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class RoleActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.role);
		Context context = getContext();
		setTextToTextField(R.id.papel_text, getTextToField(context));
	}
	
	public void back(View view) {
		RoleActivity.this.finish();
		Intent intent = new Intent(this, PlayerActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private String getTextToField(Context context) {
		Player player = context.getPlayerById(context.getCurrentPlayer());
		Role role = player.getRole();
		String roleName = role.getName();
		context.log(String.format("Player %s is %s", player.getName(), roleName));
		if (role.getNumber() > 1) {
			return "Ikaw ay isa sa mga " + roleName;
		} else {
			return "Ikaw ay isang " + roleName;
		}
	}
	
}
