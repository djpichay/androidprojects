package kom.aswang.aswangv2.activity;

import kom.aswang.aswangv2.activity.roles.DoctorActivity;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class StolenRoleActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stolen_role);
		Context context = getContext();
		setTextToTextField(R.id.ninakaw_na_papel_text, getTextToField(context));
	}
	
	public void next(View view) {
		StolenRoleActivity.this.finish();
		Intent intent = new Intent(this, DoctorActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private String getTextToField(Context context) {
		Role role = context.getPlayerById(context.getThief()).getRole();
		String roleName = role.getName();
		if (role.getNumber() > 1) {
			return "Ikaw ay isa na sa mga " + roleName;
		} else {
			return "Ikaw ay naging isa ng " + roleName;
		}
	}
	
}
