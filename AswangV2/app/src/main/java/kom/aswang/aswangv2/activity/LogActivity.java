package kom.aswang.aswangv2.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kom.aswang.aswangv2.utils.Constants;

public class LogActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.logs);
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logs_id);
		
		for (String log : getContext().getLogs()) {
			TextView logText = new TextView(getApplication());
			logText.setText(log);
			logText.setTextSize(10F);
			logText.setTextColor(Color.WHITE);
			linearLayout.addView(logText);
		}
		
	}
	
	public void back(View view) {
		this.finish();
		Intent intent = new Intent(this, SummaryActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
}
