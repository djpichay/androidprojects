package kom.aswang.aswangv2.activity.roles;

import java.util.List;

import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;

public class DrunkActivity extends SpinnerRoleActivity {
	
	@Override
	protected Role getRole() {
		return Role.LASENGGO;
	}

	@Override
	protected int getContentView() {
		return R.layout.drunk;
	}

	@Override
	protected int getSpinnerId() {	
		return R.id.drunk_names_spinner;
	}

	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				if (!isBarberGoingToKill(player)) {
					player.setDrunk(true);
					getContext().log(String.format("The drunk made %s drunk", name));
				}
			}
		}
		
	}

	@Override
	protected Class<?> getNextActivity() {
		return ThiefActivity.class;
	}
	
}
