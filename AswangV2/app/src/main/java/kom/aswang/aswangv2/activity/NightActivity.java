package kom.aswang.aswangv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kom.aswang.aswangv2.activity.roles.CouplesActivity;
import kom.aswang.aswangv2.utils.Constants;

public class NightActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.night);
	}
	
	public void next(View view) {
		this.finish();
		Intent intent = new Intent(this, CouplesActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
}
