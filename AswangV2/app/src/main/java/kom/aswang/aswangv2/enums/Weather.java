package kom.aswang.aswangv2.enums;

/**
 * Created by djpichay on 12/20/17.
 */
public enum Weather {

    GOOD,
    RAINY,
    STORMY

}
