package kom.aswang.aswangv2.activity.roles;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kom.aswang.aswangv2.activity.AbstractActivity;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

/**
 * Created by djpichay on 12/20/17.
 */
public abstract class NormalRoleActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (!showActivity()) {
            goToNextActivity();
        }

        super.onCreate(savedInstanceState);
        setContentView(getContentView());

    }

    public void next(View view) {
        performAction();
        goToNextActivity();
    }

    protected void goToNextActivity() {
        this.finish();
        Intent intent = new Intent(this, getNextActivity());
        intent.putExtra(Constants.CONTEXT, getContext());
        startActivity(intent);
    }

    protected boolean showActivity() {
        for (Player player : getContext().getPlayers()) {
            if (getRole().equals(player.getRole()) && player.isAlive() && !player.isDrunk()) {
                return true;
            }
        }
        return false;
    }

    protected abstract Role getRole();

    protected abstract int getContentView();

    protected abstract void performAction();

    protected abstract Class<?> getNextActivity();

}
