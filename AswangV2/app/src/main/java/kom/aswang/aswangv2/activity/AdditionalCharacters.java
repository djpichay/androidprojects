package kom.aswang.aswangv2.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;
import kom.aswang.aswangv2.utils.ShuffleUtil;

public class AdditionalCharacters extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.additional_characters);
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.additional_characters_id);
		linearLayout.setOrientation(LinearLayout.HORIZONTAL);
		linearLayout.setGravity(Gravity.CENTER);

		List<Role> rolesForDisplay = new ArrayList<>();
		for (Role role : Role.values()) {
			if (0 != role.getComponent()) {
				rolesForDisplay.add(role);
			}
		}

		double numberOfRoles = rolesForDisplay.size();
		double halfNumberOfRoles = Math.ceil(numberOfRoles / 2);

		LinearLayout leftPanel = new LinearLayout(this);
		leftPanel.setOrientation(LinearLayout.VERTICAL);

		LinearLayout rightPanel = new LinearLayout(this);
		rightPanel.setOrientation(LinearLayout.VERTICAL);

		for (int i = 0; i < halfNumberOfRoles; i++) {
			addCheckboxAndRoleName(leftPanel, rolesForDisplay.get(i));

			if (i + halfNumberOfRoles < numberOfRoles) {
				addCheckboxAndRoleName(rightPanel, rolesForDisplay.get(i + (int)halfNumberOfRoles));
			} else {
				LinearLayout layout = new LinearLayout(this);
				layout.setOrientation(LinearLayout.HORIZONTAL);
				CheckBox checkbox = new CheckBox(getApplication());
				checkbox.setVisibility(View.INVISIBLE);
				layout.addView(checkbox);
				rightPanel.addView(layout);
			}
		}

		linearLayout.addView(leftPanel);
		linearLayout.addView(rightPanel);

	}
	
	public void shuffle(View view) {
		Intent intent = new Intent(this, PlayerActivity.class);
		Context context = ShuffleUtil.shuffle(getContext());
		if (null != context) {
			this.finish();
			intent.putExtra(Constants.CONTEXT, ShuffleUtil.shuffle(getContext()));
			startActivity(intent);
		} else {
			Toast.makeText(this, "Ang bilang ng karagdagang tauhan ay kailangang bawasan", Toast.LENGTH_SHORT).show();
		}
	}

	public void back(View view) {
		Intent intent = new Intent(this, Main.class);
		startActivity(intent);
	}

	public void viewRoleDescription(Role role) {
		Intent intent = new Intent(this, RoleDescriptionActivity.class);
		intent.putExtra(Constants.ROLE, role);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}

	private void addCheckboxAndRoleName(ViewGroup view, final Role role) {
		LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		final CheckBox checkbox = new CheckBox(getApplication());
		checkbox.setId(role.getComponent());
		final TextView textView = new TextView(this);
		textView.setText(role.getName());
		textView.setTextColor(Color.WHITE);
		textView.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						viewRoleDescription(role);
					}
				}
		);
		layout.addView(checkbox);
		layout.addView(textView);
		view.addView(layout);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	protected Context getContext() {
		Context context = (Context) getIntent().getSerializableExtra(Constants.CONTEXT);
		context.clearPlayers();
		context.clearRoles();
		for (Role role : Role.values()) {
			int component = role.getComponent();
			if (component != 0 && getBooleanFromCheckbox(component)) {
				for (int i = 0; i < role.getNumber(); i++) {
					context.addRoleAsAvailable(role);
					if (Role.PANATIKO.equals(role)) {
						context.addRoleAsAvailable(Role.ARTISTA);
					}
				}
			}
		}
		return context;
	}
	
}
