package kom.aswang.aswangv2.activity.roles;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import kom.aswang.aswangv2.activity.LynchingActivity;
import kom.aswang.aswangv2.activity.NightActivity;
import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;

/**
 * Created by djpichay on 12/26/17.
 */
public class MayorVoteActivity extends NormalRoleActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.alkalde_candidates_id);
        linearLayout.setGravity(Gravity.CENTER);

        LinearLayout namePanel = new LinearLayout(this);
        namePanel.setGravity(Gravity.RIGHT);
        namePanel.setOrientation(LinearLayout.VERTICAL);

        LinearLayout votePanel = new LinearLayout(this);
        votePanel.setGravity(Gravity.LEFT);
        votePanel.setOrientation(LinearLayout.VERTICAL);

        int id = 0;
        for(final Player player : getContext().getPlayers()) {

            LinearLayout nameLayout = new LinearLayout(this);
            nameLayout.setOrientation(LinearLayout.HORIZONTAL);
            nameLayout.setGravity(Gravity.CENTER);

            TextView playerName = new TextView(this);
            playerName.setTextColor(Color.WHITE);
            playerName.setText(player.getName());
            playerName.setTextSize(20F);
            nameLayout.addView(playerName);

            Button dummyButton = new Button(this);
            dummyButton.setText("");
            dummyButton.setVisibility(View.INVISIBLE);
            nameLayout.addView(dummyButton);

            namePanel.addView(nameLayout);

            LinearLayout voteLayout = new LinearLayout(this);
            voteLayout.setOrientation(LinearLayout.HORIZONTAL);
            voteLayout.setGravity(Gravity.CENTER);

            final EditText playerVote = new EditText(this);
            playerVote.setEnabled(false);
            playerVote.setText("0");
            playerVote.setId(id++);

            final int editTextId = playerVote.getId();

            Button decrementButton = new Button(this);
            decrementButton.setTextSize(20F);
            decrementButton.setText("-");
            decrementButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int vote = Integer.parseInt(playerVote.getText().toString()) - 1;
                            if (vote < 0) {
                                vote = 0;
                            }
                            setEditTextText(editTextId, "" + vote);
                        }
                    }
            );
            voteLayout.addView(decrementButton);

            Button incrementButton = new Button(this);
            incrementButton.setTextSize(20F);
            incrementButton.setText("+");
            incrementButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setEditTextText(editTextId, "" + (Integer.parseInt(playerVote.getText().toString()) + 1));
                        }
                    }
            );
            voteLayout.addView(playerVote);

            voteLayout.addView(incrementButton);

            votePanel.addView(voteLayout);
        }

        linearLayout.addView(namePanel);
        linearLayout.addView(votePanel);

    }

    public void setEditTextText(int id, String text) {
        EditText editText = (EditText) findViewById(id);
        editText.setText(text);
    }

    @Override
    public void next(View view) {
        List<Player> players = getContext().getPlayers();
        Integer[] voteArray = new Integer[players.size()];
        for (int i = 0; i < players.size(); i++) {
            EditText editText = (EditText) findViewById(i);
            voteArray[i] = Integer.parseInt(editText.getText().toString());
        }

        int highestVote = 0;
        int totalVotes = 0;
        for (int i = 0; i < players.size(); i++) {
            totalVotes += voteArray[i];
            if (voteArray[i] > highestVote) {
                highestVote = voteArray[i];
            }
        }

        if (totalVotes != players.size()) {
            Toast.makeText(this,
                    String.format("Mali ang dami ng botong naitala. %d ang naitalang boto sa %d na inaasahan.", totalVotes, players.size()),
                    Toast.LENGTH_LONG).show();
            return;
        }

        int occurrence = 0;
        int mayor = 0;
        for (int i = 0; i < players.size(); i++) {
            if (voteArray[i] == highestVote) {
                occurrence++;
                mayor = i;
            }
        }

        if (occurrence == 1) {
            players.get(mayor).setMayor(true);
            goToNextActivity();
        } else {
            Toast.makeText(this, "Maraming manlalaro ang magkapareho ang boto", Toast.LENGTH_LONG).show();
        }

    }

    public void clear(View view) {
        List<Player> players = getContext().getPlayers();
        for (int i = 0; i < players.size(); i++) {
            setEditTextText(i, "0");
        }
    }

    @Override
    protected void goToNextActivity() {
        super.goToNextActivity();
    }

    @Override
    protected boolean showActivity() {
        return getContext().isFirstTurn();
    }

    @Override
    protected int getContentView() {
        return R.layout.mayor_vote;
    }

    @Override
    protected void performAction() {
        // do nothing
    }

    protected Role getRole() {
        return null;
    }

    @Override
    protected Class<?> getNextActivity() {
        Context context = getContext();
        if (context.isFirstTurn()) {
            context.clearVotingSlate();
            String[] suspects = new String[context.getPlayers().size()];
            int i = 0;
            for (Player player : context.getPlayers()) {
                suspects[i++] = player.getName();
                context.addEligibleVoter(player.getName());
            }
            context.setSuspects(suspects);
        }
        if (!context.isDay()) {
            return NightActivity.class;
        }
        return LynchingActivity.class;
    }
}
