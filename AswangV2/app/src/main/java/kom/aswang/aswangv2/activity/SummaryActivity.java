package kom.aswang.aswangv2.activity;

import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SummaryActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.summary);
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.summary_id);

		int thiefPos = getContext().getThief();
		int childAswangPos = getContext().getChildAswang();
		int ctr = 0;
		
		for (Player player: getContext().getPlayers()) {
			TextView playerView = new TextView(getApplication());
			String deathCause = "";
			String role = player.getRole().getName();
			String playerSummary = "";
			if (thiefPos == ctr) {
				role = Role.MAGNANAKAW.getName() + " na naging " + player.getRole().getName();
			}
			if (childAswangPos == ctr) {
				role = Role.BATANG_ASWANG.getName() + " na naging " + player.getRole().getName();
			}
			if (null != player.getCauseOfDeath()) {
				deathCause = player.getCauseOfDeath().getCause();
			}
			if (deathCause.equals("")) {
				playerSummary = player.getName() + "-" + role;
				playerView.setTextColor(Color.WHITE);
			} else {
				playerSummary = player.getName() + "-" + role+" (" + deathCause + ")";
				playerView.setTextColor(Color.DKGRAY);
			}
			playerView.setText(playerSummary);
			playerView.setTextSize(18F);
			playerView.setGravity(Gravity.CENTER);
			linearLayout.addView(playerView);
			ctr++;
		}
		
	}
	
	public void seeLogs(View view) {
		this.finish();
		Intent intent = new Intent(this, LogActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	public void next(View view) {
		this.finish();
		Intent intent = new Intent(this, Main.class);
		startActivity(intent);
	}
	
}
