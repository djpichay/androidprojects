package kom.aswang.aswangv2.activity.roles;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import kom.aswang.aswangv2.activity.AbstractActivity;
import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.DeathCauses;
import kom.aswang.aswangv2.enums.Role;
import kom.aswang.aswangv2.utils.Constants;

/**
 * Created by djpichay on 12/29/17.
 */
public class BatangAswangActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (!showActivity()) {
            nextActivity();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.batang_aswang);
    }

    public void next(View view) {
        this.finish();
        nextActivity();
    }

    protected boolean showActivity() {
        int i = 0;
        for (Player player : getContext().getPlayers()) {
            if (Role.BATANG_ASWANG.equals(player.getRole())) {
                if (DeathCauses.EATEN.equals(player.getCauseOfDeath())) {
                    getContext().reviveRecentlyDeceased(player.getName());
                    getContext().setRecentAswangVictim("");
                    player.setRole(Role.ASWANG);
                    player.setCauseOfDeath(null);
                    getContext().log(String.format("Player %s is now a true aswang.", player.getName()));
                    getContext().setChildAswang(i);
                    return true;
                }
            }
            i++;
        }
        return false;
    }

    private void nextActivity() {
        Intent intent = new Intent(this, SeerActivity.class);
        intent.putExtra(Constants.CONTEXT, getContext());
        startActivity(intent);
    }

}
