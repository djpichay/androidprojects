package kom.aswang.aswangv2.activity.roles;

import java.util.List;

import kom.aswang.aswangv2.activity.LynchingActivity;
import kom.aswang.aswangv2.activity.R;
import kom.aswang.aswangv2.activity.DayActivity;
import kom.aswang.aswangv2.activity.NightActivity;
import kom.aswang.aswangv2.context.Context;
import kom.aswang.aswangv2.entity.Player;
import kom.aswang.aswangv2.enums.Role;

public class MayorActivity extends SpinnerRoleActivity {

	@Override
	protected boolean showActivity() {
		return true;
	}

	@Override
	protected Role getRole() {
		// this is not called
		return null;
	}
	
	@Override
	protected int getContentView() {
		return R.layout.mayor;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.mayor_names_spinner;
	}

	@Override
	protected boolean isPlayerQualified(Player player) {
		return player.isAlive();
	}

	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				player.setMayor(true);
			}
		}
		
		getContext().log(String.format("Player %s is elected Mayor", name));
	}

	@Override
	protected Class<?> getNextActivity() {
		Context context = getContext();
		if (context.isFirstTurn()) {
			context.clearVotingSlate();
			String[] suspects = new String[context.getPlayers().size()];
			int i = 0;
			for (Player player : context.getPlayers()) {
				suspects[i++] = player.getName();
				context.addEligibleVoter(player.getName());
			}
			context.setSuspects(suspects);
		}
		if (!context.isDay()) {
			return NightActivity.class;
		}
		return LynchingActivity.class;
	}

}
