package com.translator;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.parser.Parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private EditText toSpeechText;

    private Button translateButton;

    private Button clearButton;

    private Spinner voiceSpinner;

    private TextView translatedText;

    private TextView translatedFromText;

    private TextView romanizedFormText;

    private List<String> voiceList = new ArrayList<String>();

    private Map<String, TranslateObject> voiceMap = new HashMap<>();

    private boolean voicesLoaded = false;

    private int defaultPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!isNetworkConnected()) {
            Toast.makeText(this.getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        toSpeechText = (EditText) findViewById(R.id.editText);

        translateButton = (Button) findViewById(R.id.translate_button);

        clearButton = (Button) findViewById(R.id.clear_button);

        voiceSpinner = (Spinner) findViewById(R.id.spinner);

        translatedText = (TextView) findViewById(R.id.translated_text);

        translatedFromText = (TextView) findViewById(R.id.translated_from);

        romanizedFormText = (TextView) findViewById(R.id.romanized_form);

        translateButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        buttonClick();
                    }
                }
        );

        clearButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clearButton();
                    }
                }
        );

        new Thread(new GetVoicesThread()).start();

        while (!voicesLoaded) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                Toast.makeText(this.getApplicationContext(), "Error encountered", Toast.LENGTH_LONG).show();
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, voiceList);
            voiceSpinner.setAdapter(adapter);
            voiceSpinner.setSelection(defaultPosition);
        }

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void clearButton() {
        toSpeechText.setText("");
        translatedText.setText("Translated Text");
        translatedFromText.setText("Translated From");
        romanizedFormText.setText("");
    }

    public void buttonClick()  {

        if (!isNetworkConnected()) {
            Toast.makeText(this.getApplicationContext(), "No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        String text = toSpeechText.getText().toString();

        if (text.trim().length() == 0) {
            Toast.makeText(this.getApplicationContext(), "Input is required", Toast.LENGTH_LONG).show();
        } else {
            new Thread(new SynthesizeSpeechThread()).start();
        }

    }

    public class GetVoicesThread implements Runnable {

        private Context context;

        @Override
        public void run() {
            try {
                URL url = new URL("https://ggnuozaqfj.execute-api.us-east-1.amazonaws.com/poc/voices");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String response = "";
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }

                    Log.i("MainActivity", response);

                    JSONObject jsonResponse = new JSONObject(response);
                    JSONArray jsonArray = jsonResponse.getJSONArray("Voices");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject json = (JSONObject) jsonArray.get(i);
                        String key = json.get("LanguageName") + " - " + json.get("Gender");
                        int origLength = key.length();
                        Log.i("x", key);
                        while (voiceMap.containsKey(key)) {
                            if (key.length() == origLength) {
                                key += " 2";
                            } else {
                                int x = Integer.parseInt(key.substring(key.length()-1, key.length())) + 1;
                                key = key.substring(0, key.length() - 1);
                                key += x;
                            }
                        }
                        voiceMap.put(key, new TranslateObject(json.getString("Id"), json.getString("LanguageCode")));
                        voiceList.add(key);
                    }

                    voiceList.add("Cebuano - Male");
                    voiceMap.put("Cebuano - Male", new TranslateObject("Brian", "ceb"));
                    voiceList.add("Cebuano - Female");
                    voiceMap.put("Cebuano - Female", new TranslateObject("Emma", "ceb"));

                    voiceList.add("Tagalog - Male");
                    voiceMap.put("Tagalog - Male", new TranslateObject("Brian", "tl"));
                    voiceList.add("Tagalog - Female");
                    voiceMap.put("Tagalog - Female", new TranslateObject("Emma", "tl"));

                    voiceList.add("Scottish - Male");
                    voiceMap.put("Scottish - Male", new TranslateObject("Brian", "gd"));
                    voiceList.add("Scottish - Female");
                    voiceMap.put("Scottish - Female", new TranslateObject("Emma", "gd"));

                    voiceList.add("Chinese - Female");
                    voiceMap.put("Chinese - Female", new TranslateObject("Meixui", "zh"));

                    voiceList.add("Tamil - Male");
                    voiceMap.put("Tamil - Male", new TranslateObject("Aditya", "ta"));

                    voiceList.add("Hindi - Female");
                    voiceMap.put("Hindi - Female", new TranslateObject("Chandra", "hi"));

                    voiceList.add("Indonesian - Female");
                    voiceMap.put("Indonesian - Female", new TranslateObject("Putri", "id"));

                    voiceList.add("Serbian - Male");
                    voiceMap.put("Serbian - Male", new TranslateObject("Zoran", "sr"));

                    voiceList.add("Vietnamese - Female");
                    voiceMap.put("Vietnamese - Female", new TranslateObject("Bian", "vi"));

                    voiceList.add("Afrikaans - Male");
                    voiceMap.put("Afrikaans - Male", new TranslateObject("Mugabe", "af"));

                    voiceList.add("Arabic - Female");
                    voiceMap.put("Arabic - Female", new TranslateObject("Fatima", "ar"));

                    voiceList.add("Croatian - Male");
                    voiceMap.put("Croatian - Male", new TranslateObject("Toni", "hr"));

                    voiceList.add("Czech - Female");
                    voiceMap.put("Czech - Female", new TranslateObject("Lenka", "cs"));

                    voiceList.add("Finnish - Female");
                    voiceMap.put("Finnish - Female", new TranslateObject("Lenna", "fi"));

                    voiceList.add("Latin - Male");
                    voiceMap.put("Latin - Male", new TranslateObject("Aquilon", "la"));

                    voiceList.add("Bangla - Male");
                    voiceMap.put("Bangla - Male", new TranslateObject("Parthiv", "bn"));

                    voiceList.add("Khmer - Female");
                    voiceMap.put("Khmer - Female", new TranslateObject("Kim", "km"));

                    voiceList.add("Thai - Female");
                    voiceMap.put("Thai - Female", new TranslateObject("Anggun", "th"));

                    voiceList.add("Swahili - Male");
                    voiceMap.put("Swahili - Male", new TranslateObject("Jumaane", "sw"));

                    voiceList.add("Ukrainian - Female");
                    voiceMap.put("Ukrainian - Female", new TranslateObject("Fadeyushka", "uk"));

                    voiceList.add("Sinhala - Female");
                    voiceMap.put("Sinhala - Female", new TranslateObject("Kalathma", "si"));

                    voiceList.add("Nepali - Female");
                    voiceMap.put("Nepali - Female", new TranslateObject("Lakshmi", "ne"));

                    voiceList.add("Greek - Female");
                    voiceMap.put("Greek - Female", new TranslateObject("Megaira", "el"));

                    voiceList.add("Albanian - Male");
                    voiceMap.put("Albanian - Male", new TranslateObject("Konstandin", "sq"));

                    voiceList.add("Armenian - Male");
                    voiceMap.put("Armenian - Male", new TranslateObject("Narek", "hy"));

                    voiceList.add("Esperanto - Male");
                    voiceMap.put("Esperanto - Male", new TranslateObject("Aleksandro", "eo"));

                    voiceList.add("Hungarian - Female");
                    voiceMap.put("Hungarian - Female", new TranslateObject("Luca", "hu"));

                    voiceList.add("Slovak - Female");
                    voiceMap.put("Slovak - Female", new TranslateObject("Petra", "sk"));

                    voiceList.add("Catalan - Male");
                    voiceMap.put("Catalan - Male", new TranslateObject("Felipe", "ca"));

                    voiceList.add("Estonian - Male");
                    voiceMap.put("Estonian - Male", new TranslateObject("Kaspar", "et"));

                    voiceList.add("Bosnian - Male");
                    voiceMap.put("Bosnian - Male", new TranslateObject("Vedad", "bs"));

                    voiceList.add("Luxembourgish - Male");
                    voiceMap.put("Luxembourgish - Male", new TranslateObject("Hans", "lb"));
                    voiceList.add("Luxembourgish - Female");
                    voiceMap.put("Luxembourgish - Female", new TranslateObject("Vicki", "lb"));

                    Collections.sort(voiceList);

                    int position = 0;
                    for (String string : voiceList) {
                        if (string.equals("US English - Female")) {
                            defaultPosition = position;
                        }
                        position++;
                    }

                    voicesLoaded = true;

                }
            } catch (Exception e) {
                Log.e("GetVoicesThread", "Exception encountered: " + e);
                runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error on getting available languages", Toast.LENGTH_LONG).show();
                            }
                        }
                );
            }
        }

    }

    public class SynthesizeSpeechThread implements Runnable {

        @Override
        public void run() {
            try {

                String selectedVoice = voiceSpinner.getSelectedItem().toString();
                TranslateObject to = voiceMap.get(selectedVoice);
                String voice = to.getId();
                String lang = to.getCode();

                Log.i("SynthesizeSpeechThread", voice);

                URL url = new URL("https://ggnuozaqfj.execute-api.us-east-1.amazonaws.com/poc/synthesize/v2");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestProperty("Content-Type","application/json");

                JSONObject payload = new JSONObject();
                payload.put("message", toSpeechText.getText());
                payload.put("voice", voice);
                payload.put("lang", lang);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));

                Log.i("SynthesizeSpeechThread", payload.toString());

                writer.write(payload.toString());
                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String response = "";
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }

                    onSuccessResponse(response);
                }

            } catch (Exception e) {
                Log.e("SynthesizeSpeechThread", "Exception encountered: " + e);
                runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error on synthesizing input", Toast.LENGTH_LONG).show();
                            }
                        }
                );
            }
        }
    }

    public void onSuccessResponse(String string) {

        try {
            Log.i("MainActivity", string);
            JSONObject json = new JSONObject(string);
            final String translatedMessage = json.getString("translatedMessage");
            Log.i("MainActivity", translatedMessage);

            String selected = voiceSpinner.getSelectedItem().toString();
            final String romanticized = romanticize(translatedMessage);

            final String translatedFrom = json.getString("translatedFrom");
            final String translatedLanguage = new Locale(translatedFrom).getDisplayLanguage();

            Log.i("MainActivity", translatedLanguage);

            MediaPlayer mp = new MediaPlayer();

            File convertedFile = File.createTempFile("convertedFile", ".dat", getDir("filez", 0));
            FileOutputStream out = new FileOutputStream(convertedFile);
            FileInputStream fis = null;

            JSONObject audioObject = json.getJSONObject("audio");
            JSONArray data = audioObject.getJSONArray("data");

            byte[] byteArray = new byte[data.length()];

            for (int ctr = 0 ; ctr < data.length(); ctr++) {
                byteArray[ctr] = ((Integer)data.get(ctr)).byteValue();
            }

            out.write(byteArray);

            fis = new FileInputStream(convertedFile);

            mp.setDataSource(fis.getFD());
            mp.prepare();
            mp.start();

            runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            translatedText.setText(translatedMessage);
                            translatedFromText.setText("From: " + translatedLanguage);
                            if (!romanticized.equals(translatedMessage)) {
                                romanizedFormText.setText(romanticized);
                            } else {
                                romanizedFormText.setText("");
                            }
                        }
                    }
            );

            fis.close();

        } catch (Exception e) {
            Log.e("onSuccessResponse", "Exception encountered: " + e);
            runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Error on processing response", Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }

    }

    private String romanticize(String string) throws Exception {
        String selected = voiceSpinner.getSelectedItem().toString();
        String romanticized = string;
        if (selected.contains("Japanese")) {
            romanticized = romanticizeJapanese(string);
        } else {
            romanticized = romanticizeOthers(string);
        }
        return romanticized;
    }

    private String romanticizeKorean(String input) throws Exception {

        URL url = new URL("http://roman.cs.pusan.ac.kr/eng_result_all.aspx?input=" + URLEncoder.encode(input, "UTF-8"));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(15000);
        conn.setConnectTimeout(15000);

        String koreanConvert = input;

        int responseCode=conn.getResponseCode();

        if (responseCode == HttpsURLConnection.HTTP_OK) {
            String response = "";
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }

            response = response.substring(response.indexOf("outputRMNormal") + 2, response.length());

            response = response.substring(response.indexOf(">") + 1, response.indexOf("<"));

            koreanConvert = response;
        }

        return koreanConvert;

    }

    private String romanticizeJapanese(String input) throws Exception {

        URL url = new URL("http://nihongo.j-talk.com/");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(15000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        writer.write("kanji="+input+"&Submit=Translate Now&kana_output=romaji&converter=detailed&uniqid=2724abe8de293dff6e79e7bfb5e606375ecde9a5");
        writer.flush();
        writer.close();

        int responseCode = conn.getResponseCode();

        if (responseCode != HttpURLConnection.HTTP_OK) {
            return input;
        }

        String response = "";
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        while ((line = br.readLine()) != null) {
            response += line;
        }

        StringBuilder sb = new StringBuilder();
        String td = "";
        String word = "";

        int i = 0;
        while(true) {
            int index = response.indexOf("span class=\'romaji\'");
            if (index == -1) {
                break;
            }
            response = response.substring(index + 2, response.length());

            td = response.substring(response.indexOf(">") + 1, response.indexOf("</td>"));
            if (td.contains("syln")) {
                word = td.replace("<span class=\"syln\">", "").replace("</span>", "");
            } else {
                word = response.substring(response.indexOf(">") + 1, response.indexOf("<"));
            }

            sb.append(Parser.unescapeEntities(word, false) + " ");
        }

        return sb.toString();

    }

    private String romanticizeChinese(String input) throws Exception {

        String escaped = StringEscapeUtils.escapeHtml(input);

        String encoded = URLEncoder.encode(escaped, "UTF-8");

        URL url = new URL("https://www.chinesetools.eu/tools/zhuyin?texte=" + encoded + "&from=chinois&to=pinyin&Submit=Convert");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(15000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("GET");
        conn.setDoOutput(true);


        if (conn.getResponseCode() == 200) {

            String response = "";
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }
            response = response.substring(response.indexOf("<form"), response.length());

            response = response.substring(response.indexOf("<div style=\"padding:10px;\">"), response.length());
            response = response.substring(response.indexOf(">") + 1, response.indexOf("</div>"));

            return response;

        }

        return input;

    }

    private String romanticizeOthers(String input) throws Exception {

        URL url = new URL("https://www.textmagic.com/free-tools/text-transliteration-tool/convert");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(15000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        writer.write("str="+input);
        writer.flush();
        writer.close();

        int responseCode = conn.getResponseCode();

        if (responseCode != HttpURLConnection.HTTP_OK) {
            return input;
        }

        String response = "";
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        while ((line = br.readLine()) != null) {
            response += line;
        }

        JSONObject json = new JSONObject(response);

        if (json.getBoolean("success")) {
            json = json.getJSONObject("data");
            return json.getString("converted");
        }

        return input;

    }

    private String romanticizeRussian(String input) {

        String car = input;

        car = car.replace("а", "a");
        car = car.replace("б", "b");
        car = car.replace("в", "v");
        car = car.replace("г", "g");
        car = car.replace("д", "d");
        car = car.replace("е", "e");
        car = car.replace("ё", "ë");
        car = car.replace("ж", "ž");
        car = car.replace("з", "z");
        car = car.replace("и", "i");
        car = car.replace("й", "j");
        car = car.replace("к", "k");
        car = car.replace("л", "l");
        car = car.replace("м", "m");
        car = car.replace("н", "n");
        car = car.replace("о", "o");
        car = car.replace("п", "p");
        car = car.replace("р", "r");
        car = car.replace("с", "s");
        car = car.replace("т", "t");
        car = car.replace("у", "u");
        car = car.replace("ф", "f");
        car = car.replace("х", "h");
        car = car.replace("ц", "c");
        car = car.replace("ч", "č");
        car = car.replace("ш", "š");
        car = car.replace("щ", "ŝ");
        car = car.replace("ъ", "’’");
        car = car.replace("ы", "y");
        car = car.replace("ь", "’");
        car = car.replace("э", "è");
        car = car.replace("ю", "ju");
        car = car.replace("я", "ja");

        car = car.replace("А", "A");
        car = car.replace("Б", "B");
        car = car.replace("В", "V");
        car = car.replace("Г", "G");
        car = car.replace("Д", "D");
        car = car.replace("Е", "E");
        car = car.replace("Ё", "Ë");
        car = car.replace("Ж", "Ž");
        car = car.replace("З", "Z");
        car = car.replace("И", "I");
        car = car.replace("Й", "J");
        car = car.replace("К", "K");
        car = car.replace("Л", "L");
        car = car.replace("М", "M");
        car = car.replace("Н", "N");
        car = car.replace("О", "O");
        car = car.replace("П", "P");
        car = car.replace("Р", "R");
        car = car.replace("С", "S");
        car = car.replace("Т", "T");
        car = car.replace("У", "U");
        car = car.replace("Ф", "F");
        car = car.replace("Х", "H");
        car = car.replace("Ц", "C");
        car = car.replace("Ч", "Č");
        car = car.replace("Ш", "Š");
        car = car.replace("Щ", "Ŝ");
        car = car.replace("Ъ", "’’");
        car = car.replace("Ы", "Y");
        car = car.replace("Ь", "’");
        car = car.replace("Э", "È");
        car = car.replace("Ю", "Ju");
        car = car.replace("Я", "Ja");

        return car;
    }

    private String romanticizeTamil(String input) {

        String car = input;

        car = car.replace("அ", "a");
        car = car.replace("ஆ", "a");

        car = car.replace("ப்", "b");
        car = car.replace("ப", "ba");
        car = car.replace("ெ", "be");
        car = car.replace("ி", "bi");
        car = car.replace("ொ", "bo");
        car = car.replace("பு", "bu");

        car = car.replace("ஃ", "ka");
        car = car.replace("க்", "k");
        car = car.replace("க", "ka");
        car = car.replace("கெ", "ke");
        car = car.replace("கி", "ki");
        car = car.replace("கொ", "ko");
        car = car.replace("க", "ku");

        car = car.replace("ட்", "t");
        car = car.replace("ட", "ta");
        car = car.replace("டெ", "te");
        car = car.replace("டி", "ti");
        car = car.replace("டொ", "to");
        car = car.replace("டு", "tu");
        car = car.replace("த", "ta");

        car = car.replace("எ", "e");
        car = car.replace("ஏ", "ē");
        car = car.replace("ே", "ē");

        car = car.replace("க்", "h");
        car = car.replace("க", "ha");
        car = car.replace("கெ", "he");
        car = car.replace("கி", "hi");
        car = car.replace("கொ", "ho");
        car = car.replace("கு", "hu");
        car = car.replace("ஹ", "ha");

        car = car.replace("இ", "i");
        car = car.replace("ஈ", "ī");
        car = car.replace("ை", "i");

        car = car.replace("ஜ்", "j");
        car = car.replace("ஜ", "ja");
        car = car.replace("ஜெ", "je");
        car = car.replace("ஜி", "ji");
        car = car.replace("ஜொ", "jo");
        car = car.replace("ஜு", "ju");

        car = car.replace("ள", "la");
        car = car.replace("ல்", "l");
        car = car.replace("ல", "la");
        car = car.replace("லெ", "le");
        car = car.replace("லி", "li");
        car = car.replace("லொ", "lo");
        car = car.replace("லு", "lu");

        car = car.replace("ம்", "m");
        car = car.replace("ம", "ma");
        car = car.replace("மெ", "me");
        car = car.replace("மி", "mi");
        car = car.replace("மொ", "mo");
        car = car.replace("மு", "mu");

        car = car.replace("ம்", "n");
        car = car.replace("ந", "na");
        car = car.replace("நெ", "ne");
        car = car.replace("நி", "ni");
        car = car.replace("நொ", "no");
        car = car.replace("நு", "nu");
        car = car.replace("ண", "na");
        car = car.replace("ன", "na");
        car = car.replace("ங", "na");
        car = car.replace("ஞ", "ña");

        car = car.replace("ஒ", "o");
        car = car.replace("ஓ", "ō");
        car = car.replace("ோ", "ō");

        car = car.replace("ப்", "p");
        car = car.replace("ப", "pa");
        car = car.replace("பெ", "pe");
        car = car.replace("பி", "pi");
        car = car.replace("பொ", "po");
        car = car.replace("பு", "pu");

        car = car.replace("ற", "ra");
        car = car.replace("ர்", "r");
        car = car.replace("ர", "ra");
        car = car.replace("ரெ", "re");
        car = car.replace("ரி", "ri");
        car = car.replace("ரொ", "ro");
        car = car.replace("ரு", "ru");

        car = car.replace("ச்", "s");
        car = car.replace("ச", "sa");
        car = car.replace("செ", "se");
        car = car.replace("சி", "si");
        car = car.replace("சொ", "so");
        car = car.replace("சு", "su");
        car = car.replace("ஶ", "sa");
        car = car.replace("ஷ", "sha");
        car = car.replace("ஸ", "sa");

        car = car.replace("உ", "u");
        car = car.replace("ஊ", "ū");

        car = car.replace("வ்", "v");
        car = car.replace("வ", "va");
        car = car.replace("வெ", "ve");
        car = car.replace("வி", "vi");
        car = car.replace("வொ", "vo");
        car = car.replace("வு", "vu");

        car = car.replace("ந்", "w");
        car = car.replace("ந", "wa");
        car = car.replace("நெ", "we");
        car = car.replace("நி", "wi");
        car = car.replace("நொ", "wo");
        car = car.replace("நு", "wu");

        car = car.replace("ய்", "y");
        car = car.replace("ய", "ya");
        car = car.replace("யெ", "ye");
        car = car.replace("யி", "yi");
        car = car.replace("யொ", "yo");
        car = car.replace("யு", "yu");

        car = car.replace("ழ்", "z");
        car = car.replace("ழ", "za");
        car = car.replace("ழெ", "ze");
        car = car.replace("ழி", "zi");
        car = car.replace("ழொ", "zo");
        car = car.replace("ழு", "zu");

        car = car.replace("ஔ", "au");
        car = car.replace("ஐ", "ai");
        car = car.replace("ா", "");
        car = car.replace("ு", "");

        car = car.replace("a்", "");

        car = car.replace("்", "");

        car = car.replace("ூ", "u");
        car = car.replace("ு", "u");
        car = car.replace("ீ", "i");
        car = car.replace("ி", "i");

        return car;

    }

    public class TranslateObject {

        private String id;
        private String code;

        TranslateObject(String id, String code) {
            this.id = id;
            this.code = code;
        }

        public String getId() {
            return id;
        }

        public String getCode() {
            return code;
        }

    }

}
