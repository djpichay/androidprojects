package kom.aswang.enums;

public enum Role {

	ASWANG("Aswang"),
	TAUMBAYAN("Taumbayan"),
	ALBULARYO("Albularyo", true),
	ARTISTA("Artista"), 
	BARBERO("Barbero"),
	KALAPATI("Kalapating Mababa ang Lipad"),
	KARTERO("Kartero"),
	LASENGGO("Lasenggo"),
	MAGNANAKAW("Magnanakaw", true),
	MAGSASAKA("Magsasaka"),
	MAMBABARANG("Mambabarang", true),
	MANANAHI("Mananahi"),
	MANGANGASO("Mangangaso"),
	MANGHUHULA("Manghuhula", true),
	MAGKASINTAHAN("Magkasintahan", true, 2),
	PANATIKO("Panatiko"),
	PASTOL("Pastol"),
	PINUNO_NG_KULTO("Pinuno ng Kulto");
	
	private String name;
	
	private int component = 0;
	
	private int number = 1;
	
	private Role(String name) {
		this.name = name;
	}
	
	private Role(String name, boolean addCheckbox) {
		this.name = name;
		if (addCheckbox) {
			component = name.hashCode();
			component = Math.abs(component);
		}
	}
	
	private Role(String name, boolean addCheckbox, int number) {
		this(name, addCheckbox);
		this.number = number;
	}
	
	public String getName() {
		return name;
	}
	
	public int getComponent() {
		return component;
	}

	public int getNumber() {
		return number;
	}
	
}
