package kom.aswang.enums;

public enum DeathCauses {

	TRIED_TO_HEAL("Sinubukang pagalingin ang aswang"),
	EATEN("Kinain ng mga aswang"),
	BURNED("Sinunog ng taumbayan dahil pinaghihinalaan"),
	SHOT("Binaril ng mangangaso"),
	ILLEGAL_RECRUITMENT("Napasok ng aswang ang kulto"),
	SLICED("Ginilitan ng Barbero"),
	MELTED("Tinunaw ng Mambabarang"),
	GRIEF("Nagpakamatay sa pangungulila"),
	WOUNDS("Bumuka ang sugat"),
	UBER_FANATIC("Niligtas ang iniidolo kapalit ang kanyang buhay"),
	;
	
	private String cause;
	
	private DeathCauses(String cause) {
		this.cause = cause;
	}
	
	public String getCause() {
		return cause;
	}
	
}
