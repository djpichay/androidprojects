package kom.aswang.activity;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.activity.roles.MayorActivity;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class PlayerActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.player);
		Context context = getContext();
		Player player = context.getPlayerById(context.getCurrentPlayer());
		if (null != player.getName()) {
			setTextToTextField(R.id.pangalan_text, player.getName());
		}
	}
	
	public void next(View view) {
		String name = getStringValueFromTextField(R.id.pangalan_text);
		if (!name.trim().isEmpty()) {
			if (!isNameAlreadyExist(name)) {
				Context context = getContext();
				Player player = context.getPlayerById(context.getCurrentPlayer());
				player.setName(name);
				context.incrementCurrentPlayer();
				this.finish();
				Intent intent = new Intent(this, PlayerActivity.class);
				if (context.allPlayersAccountedFor()) {
					intent = new Intent(this, MayorActivity.class);
				} 
				intent.putExtra(Constants.CONTEXT, context);
				startActivity(intent);
			} else {
				Toast.makeText(this, "Mayroon ng manlalarong nagngangalang : " + name, Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(this, "Ilagay ang pangalan ng manlalaro", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void revealRole(View view) {
		String name = getStringValueFromTextField(R.id.pangalan_text);
		if (!name.trim().isEmpty()) {
			if (!isNameAlreadyExist(name)) {
				Context context = getContext();
				Player player = context.getPlayerById(context.getCurrentPlayer());
				player.setName(name);
				this.finish();
				Intent intent = new Intent(this, RoleActivity.class);
				intent.putExtra(Constants.CONTEXT, context);
				startActivity(intent);
			}else {
				Toast.makeText(this, "Mayroon ng manlalarong nagngangalang : " + name, Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(this, "Ilagay ang pangalan ng manlalaro", Toast.LENGTH_SHORT).show();
		}
	}
	
	private boolean isNameAlreadyExist(String name) {
		
		List<Player> players = getContext().getPlayers();
		
		if (players.size() == 0) {
			return false;
		}
		
		for (int i = 0; i < getContext().getCurrentPlayer(); i++) {
			if (name.equals(players.get(i).getName())) {
				return true;
			}
		}
		
		return false;
		
	}
	
}
