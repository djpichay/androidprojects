package kom.aswang.activity.roles;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import kom.aswang.activity.AbstractActivity;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.DeathCauses;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;

public abstract class SpinnerRoleActivity extends AbstractActivity {

	protected Spinner namesSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (!showActivity()) {
			goToNextActivity();
		}
		
		super.onCreate(savedInstanceState);
		setContentView(getContentView());
		
		namesSpinner = (Spinner) findViewById(getSpinnerId());
		
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		List<Player> qualifiedPlayers =  new ArrayList<Player>();
		
		for (Player player : players) {
			if (isPlayerQualified(player)) {
				qualifiedPlayers.add(player);
			}
		}
		
		String[] playerNames = new String[qualifiedPlayers.size()];
		
		int i = 0;
		
		for (Player player : qualifiedPlayers) {
			playerNames[i++] = player.getName();
		}
		
		ArrayAdapter<String> adp = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_dropdown_item, playerNames);
		namesSpinner.setAdapter(adp);
		
	}
	
	public void next(View view) {
		performAction();
		goToNextActivity();
	}
	
	protected void goToNextActivity() {
		this.finish();
		Intent intent = new Intent(this, getNextActivity());
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	protected boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (getRole().equals(player.getRole()) && player.isAlive() && !player.isDrunk()) {
				return true;
			}
		}
		return false;
	}
	
	protected boolean isPlayerQualified(Player player) {
		return player.isAlive() && !getRole().equals(player.getRole());
	}
	
	protected boolean isBarberGoingToKill(Player suspectedBarber) {
		if (Role.BARBERO.equals(suspectedBarber.getRole())) {
			for (Player player : getContext().getPlayers()) {
				if (getRole().equals(player.getRole())) {
					getContext().addToRecentlyDeceased(player.getName());
					player.setCauseOfDeath(DeathCauses.SLICED);
					getContext().log(String.format("%s %s was sliced for trying something on the barber", getRole().getName(), player.getName()));
					return true;
				}
			}
		}
		return false;
	}
	
	protected abstract Role getRole();
	
	protected abstract int getContentView();
	
	protected abstract int getSpinnerId();
	
	protected abstract void performAction();
	
	protected abstract Class<?> getNextActivity();
	
}
