package kom.aswang.activity;

import kom.aswang.activity.roles.MailmanActivity;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class DayActivity extends AbstractActivity {

	TextView textView;
	Spinner namesSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Context context = getContext();
		
		if (context.isShepherdMourning() || context.getEligibleVoters().size() == 0) {
			context.log("Town is in a state of mourning or there are no voters present, no trials will take place");
			goToActivity(DuskActivity.class);
		} 
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.day);
		
		textView = (TextView) findViewById(R.id.day_text);
		namesSpinner = (Spinner) findViewById(R.id.day_names_spinner);
		
		if (context.getEligibleVoters().size() > 0) {
			textView.setText(context.getCurrentVoter());
		}
		
		String[] suspects = removeCurrentPlayerFromSuspects();
		
		ArrayAdapter<String> adp = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_dropdown_item, suspects);
		namesSpinner.setAdapter(adp);
	}
	
	public void next(View view) {
		this.finish();
		Context context = getContext();
		String votedPerson = namesSpinner.getSelectedItem().toString();
		context.log(String.format("Player %s voted for %s", context.getCurrentVoter(), votedPerson));
		context.voteFor(votedPerson, isMayor());
		Intent intent = new Intent(this, DayActivity.class);
		context.incrementCurrentPlayer();
		if (context.getConvicts().size() > 0) {
			intent = new Intent(this, MailmanActivity.class);
		}
		intent.putExtra(Constants.CONTEXT, context);
		startActivity(intent);
	}
	
	private String[] removeCurrentPlayerFromSuspects() {
		
		Context context = getContext();
		
		String[] suspects = context.getSuspects();
		String[] updatedSuspects = new String[suspects.length - 1];
		
		int j = 0;
		
		if (context.getEligibleVoters().size() > 0) {
			for (int i = 0; i < suspects.length; i++) {
				if (!suspects[i].equals(context.getCurrentVoter())) {
					updatedSuspects[j++] = suspects[i];	
				}
			}
		}
		
		
		return updatedSuspects;
		
	}
	
	private void goToActivity(Class<?> clazz) {
		this.finish();
		Intent intent = new Intent(this, clazz);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private boolean isMayor() {
		String currentVoter = textView.getText().toString();
		for (Player player : getContext().getPlayers()) {
			if (currentVoter.equals(player.getName())) {
				return player.isMayor();
			}
		}
		return false;
	}
	
}
