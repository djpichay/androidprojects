package kom.aswang.activity;

import kom.aswang.entity.Player;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SummaryActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.summary);
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.summary_id);
		
		for (Player player: getContext().getPlayers()) {
			TextView playerView = new TextView(getApplication());
			String deathCause = "";
			String playerSummary = "";
			if (null != player.getCauseOfDeath()) {
				deathCause = player.getCauseOfDeath().getCause();
			}
			if (deathCause.equals("")) {
				playerSummary = player.getName() + "-" + player.getRole().getName();
				playerView.setTextColor(Color.WHITE);
			} else {
				playerSummary = player.getName() + "-" + player.getRole().getName()+" (" + deathCause + ")";
				playerView.setTextColor(Color.DKGRAY);
			}
			playerView.setText(playerSummary);
			playerView.setTextSize(18F);
			playerView.setGravity(Gravity.CENTER);
			linearLayout.addView(playerView);
		}
		
	}
	
	public void seeLogs(View view) {
		this.finish();
		Intent intent = new Intent(this, LogActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	public void next(View view) {
		this.finish();
		Intent intent = new Intent(this, Main.class);
		startActivity(intent);
	}
	
}
