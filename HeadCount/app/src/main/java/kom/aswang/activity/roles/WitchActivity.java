package kom.aswang.activity.roles;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Spinner;
import kom.aswang.activity.R;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.DeathCauses;
import kom.aswang.enums.Role;

public class WitchActivity extends SpinnerRoleActivity {

	CheckBox checkbox;
	
	private final String DO_NOT_USE = "--Huwag Gamitin--";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		if (!showActivity()) {
			goToNextActivity();
		}
		
		super.onCreate(savedInstanceState);
		setContentView(getContentView());
		
		namesSpinner = (Spinner) findViewById(getSpinnerId());
		
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		List<Player> qualifiedPlayers =  new ArrayList<Player>();
		
		for (Player player : players) {
			if (isPlayerQualified(player)) {
				qualifiedPlayers.add(player);
			}
		}
		
		String[] playerNames = new String[qualifiedPlayers.size() + 1];
		playerNames[0] = DO_NOT_USE;
		
		int i = 1;
		
		for (Player player : qualifiedPlayers) {
			playerNames[i++] = player.getName();
		}
		
		ArrayAdapter<String> adp = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_dropdown_item, playerNames);
		namesSpinner.setAdapter(adp);	
		namesSpinner.setEnabled(!getContext().isWitchDeathPotionUsed());
		
		LinearLayout linearLayour = (LinearLayout) findViewById(R.id.choose_person_to_revive_panel);
		checkbox = new CheckBox(getApplication());
		if (!getContext().getRecentAswangVictim().equals("")) {
			checkbox.setText("Gamitin ang pampabuhay kay " + getContext().getRecentAswangVictim() + "?");
			checkbox.setEnabled(!getContext().isWitchRevivingPotionUsed());
		} else {
			checkbox.setText("Walang mamamayang napatay ang mga aswang");
			checkbox.setEnabled(false);
		}
		checkbox.setTextColor(getContext().isWitchRevivingPotionUsed() ? Color.GRAY : Color.WHITE);
		checkbox.setTextSize(18F);
		linearLayour.addView(checkbox);
		
	}
	
	@Override
	protected Role getRole() {
		return Role.MAMBABARANG;
	}

	@Override
	protected int getContentView() {
		return R.layout.witch;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.witch_names_spinner;
	}

	@Override
	protected void performAction() {
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		if (!context.isWitchRevivingPotionUsed()) {
			
			String name = context.getRecentAswangVictim();
			
			if (checkbox.isChecked()) {
				for (Player player : players) {
					if (name.equals(player.getName())) {
						context.reviveRecentlyDeceased(name);
						player.setCauseOfDeath(null);
						context.setWitchRevivingPotionUsed(true);
						context.setRecentAswangVictim("");
						context.log(String.format("The witch revived %s", name));
						isBarberGoingToKill(player);
					}
				}
			}
			
		}
		
		if (!context.isWitchDeathPotionUsed()) {
		
			String name = namesSpinner.getSelectedItem().toString();
		
			if (!DO_NOT_USE.equals(name)) {
				for (Player player : players) {
					if (name.equals(player.getName())) {
						if (!isBarberGoingToKill(player)) {
							context.addToRecentlyDeceased(player.getName());
							player.setCauseOfDeath(DeathCauses.MELTED);
							context.setWitchDeathPotionUsed(true);
							context.log(String.format("The witch melted %s", name));
							isBarberGoingToKill(player);
							if (Role.MANGANGASO.equals(player.getRole())) {
								context.setHuntersRevenge(true);
								context.log(String.format("Player killed is a hunter", name));
							}
						}
					}
				}
			}
			
		}
		
	}

	@Override
	protected Class<?> getNextActivity() {
		return CultActivity.class;
	}

}
