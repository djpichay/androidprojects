package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.DayActivity;
import kom.aswang.activity.NightActivity;
import kom.aswang.activity.R;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;

public class MayorActivity extends SpinnerRoleActivity {

	@Override
	protected boolean showActivity() {
		return true;
	}

	@Override
	protected Role getRole() {
		// this is not called
		return null;
	}
	
	@Override
	protected int getContentView() {
		return R.layout.mayor;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.mayor_names_spinner;
	}

	@Override
	protected boolean isPlayerQualified(Player player) {
		return player.isAlive();
	}

	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				player.setMayor(true);
			}
		}
		
		getContext().log(String.format("Player %s is elected Mayor", name));
	}

	@Override
	protected Class<?> getNextActivity() {
		if (!getContext().isDay()) {
			return NightActivity.class;
		}
		return DayActivity.class;
	}

}
