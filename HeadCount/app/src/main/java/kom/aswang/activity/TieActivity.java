package kom.aswang.activity;

import java.util.List;

import kom.aswang.activity.roles.HunterActivity;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class TieActivity extends AbstractActivity {

	protected Spinner namesSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		Context context = getContext();
		
		if (context.getConvicts().size() == 1) {
			String convict = context.getConvicts().get(0);
			context.setConvicted(convict);
			context.setHuntersRevenge(isHunterKilled(convict));
			context.log(String.format("Player %s is convicted for lynching", convict));
			nextActivity(HunterActivity.class);
		} 
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tie);
		
		namesSpinner = (Spinner) findViewById(R.id.tie_names_spinner);
		
		List<String> convictsList = context.getConvicts();
		
		StringBuffer sb = new StringBuffer("There is a tie vote. Mayor will choose among the convicts ");
		for (String name : convictsList) {
			sb.append(name + " ");
		}
		context.log(sb.toString());
		
		String[] convictNames = convictsList.toArray(new String[convictsList.size()]);
		
		ArrayAdapter<String> adp = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_dropdown_item, convictNames);
		namesSpinner.setAdapter(adp);
		
	}
	
	public void next(View view) {
		Context context = getContext();
		
		String convicted = namesSpinner.getSelectedItem().toString();
		context.setConvicted(convicted);
		context.log(String.format("Player %s is selected by the mayor for lynching", convicted));
		context.setHuntersRevenge(isHunterKilled(convicted));	
		
		nextActivity(HunterActivity.class);
	}
	
	private void nextActivity(Class<?> clazz) {
		this.finish();
		Intent intent = new Intent(this, clazz);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private boolean isHunterKilled(String convicted) {
		
		for (Player player : getContext().getPlayers()) {
			if (convicted.equals(player.getName()) && Role.MANGANGASO.equals(player.getRole())) {
				getContext().log("Player lynched is a hunter");
				return true;
			}
		}
		
		return false;
		
	}
	
}
