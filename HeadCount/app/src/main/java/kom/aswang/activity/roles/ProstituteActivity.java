package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;

public class ProstituteActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.KALAPATI;
	}
	
	@Override
	protected int getContentView() {
		return R.layout.patutot;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.prosti_names_spinner;
	}

	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName()) && !player.isHealed()) {
				if (!isBarberGoingToKill(player)) {
					player.setAsleep(true);
					getContext().log(String.format("The prostitute seduced %s. Will be asleep for next day", name));
				}
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		return HunterActivity.class;
	}

}
