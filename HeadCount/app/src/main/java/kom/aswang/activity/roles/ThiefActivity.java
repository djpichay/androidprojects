package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.activity.StolenRoleActivity;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.view.View;

public class ThiefActivity extends SpinnerRoleActivity {
	
	public void revealNewRole(View view) {
		switchRoles();
		ThiefActivity.this.finish();
		Intent intent = new Intent(this, StolenRoleActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private void switchRoles() {
		
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		Role stolenRole = null;
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				stolenRole = player.getRole();
				player.setRole(Role.TAUMBAYAN);
			}
		}
		
		int i = 0;
		
		for (Player player : players) {
			if (Role.MAGNANAKAW.equals(player.getRole())) {
				player.setRole(stolenRole);
				getContext().setThief(i);
			}
			i++;
		}
		
		getContext().log(String.format("The thief stole the role of %s. Thief is now %s", name, stolenRole.getName()));
		
	}
	
	@Override
	protected boolean showActivity() {
		return super.showActivity() && getContext().isFirstNight();
	}

	@Override
	protected Role getRole() {
		return Role.MAGNANAKAW;
	}
	
	@Override
	protected int getContentView() {
		return R.layout.thief;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.thief_names_spinner;
	}

	@Override
	protected boolean isPlayerQualified(Player player) {
		return !Role.MAGNANAKAW.equals(player.getRole());
	}

	@Override
	protected void performAction() {
		// nothing to do
	}

	@Override
	protected Class<?> getNextActivity() {
		return CouplesActivity.class;
	}

}
