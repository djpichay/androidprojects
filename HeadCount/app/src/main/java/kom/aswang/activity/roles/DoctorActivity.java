package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.entity.Player;
import kom.aswang.enums.DeathCauses;
import kom.aswang.enums.Role;

public class DoctorActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.ALBULARYO;
	}

	@Override
	protected int getContentView() {
		return R.layout.doctor;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.doctor_names_spinner;
	}
	
	@Override
	protected boolean isPlayerQualified(Player player) {
		return player.isAlive() && !player.getName().equals(getContext().getRecentlyHealed());
	}
	
	@Override
	protected void performAction() {
		List<Player> players = getContext().getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				getContext().log(String.format("The doctor will heal %s", name));
				if (Role.ASWANG.equals(player.getRole()) || Role.BARBERO.equals(player.getRole())) {
					killTheDoctor(player.getRole());
				} else {
					player.setHealed(true);
					getContext().setRecentlyHealed(player.getName());
				}
			}
		}
		
	}

	@Override
	protected Class<?> getNextActivity() {
		return AswangActivity.class;
	}
	
	private void killTheDoctor(Role killer) {
		for (Player player : getContext().getPlayers()) {
			if (getRole().equals(player.getRole())) {
				getContext().addToRecentlyDeceased(player.getName());
				if (killer.equals(Role.ASWANG)) {
					player.setCauseOfDeath(DeathCauses.TRIED_TO_HEAL);
					getContext().log(String.format("Doctor %s died for trying to heal an aswang", player.getName()));
				} else if (killer.equals(Role.BARBERO)) {
					player.setCauseOfDeath(DeathCauses.SLICED);
					getContext().log(String.format("Doctor %s was sliced for trying to heal the barber", player.getName()));
				}
			}
		}
	}
	
}
