package kom.aswang.activity.roles;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import kom.aswang.activity.AbstractActivity;
import kom.aswang.activity.DuskActivity;
import kom.aswang.activity.R;
import kom.aswang.activity.TieActivity;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;

public class MailmanActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		Player mailman = getMailman();
		if (null == mailman) {
			nextActivity(TieActivity.class);
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mailman);
		
		if (null != mailman) {
			getContext().setConvicted(mailman.getName());
		
			LinearLayout linearLayout = (LinearLayout) findViewById(R.id.mailman_id);
			
			for (Player player : getContext().getPlayers()) {
				if (!Role.KARTERO.equals(player.getRole()) && player.isAlive()) {
					CheckBox checkbox = new CheckBox(getApplication());
					checkbox.setText(player.getName());
					checkbox.setTextColor(Color.WHITE);
					checkbox.setId(Math.abs(player.getName().hashCode()));
					linearLayout.addView(checkbox);
				}
			}
		
		}
	
	}
	
	public void next(View view) {
		
		Context context = getContext();
		
		for (Player player : context.getPlayers()) {
			if (!Role.KARTERO.equals(player.getRole()) && player.isAlive()) {
				CheckBox checkbox = (CheckBox) findViewById(Math.abs(player.getName().hashCode()));
				if (checkbox.isChecked()) {
					context.log(String.format("Player %s will vote next trial", player.getName()));
					player.setSilenced(false);
				} else {
					context.log(String.format("Player %s will not vote next trial", player.getName()));
					player.setSilenced(true);
				}
			}
		}
		
		nextActivity(DuskActivity.class);
	}
	
	private Player getMailman() {
		
		if (getContext().getConvicts().size() == 1) {
			return null;
		}
		
		for (Player player : getContext().getPlayers()) {
			if (Role.KARTERO.equals(player.getRole())) {
				if (player.isAlive()) {
					getContext().log("There is a tie vote, mailman still alive, will be killed instead");
					return player;
				}
			}
		}
		
		return null;
		
	}
	
	private void nextActivity(Class<?> clazz) {
		this.finish();
		Intent intent = new Intent(this, clazz);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	
}
