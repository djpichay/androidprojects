package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.DawnActivity;
import kom.aswang.activity.DuskActivity;
import kom.aswang.activity.R;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.DeathCauses;
import kom.aswang.enums.Role;

public class HunterActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.MANGANGASO;
	}

	@Override
	protected int getContentView() {
		return R.layout.hunter;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.hunter_names_spinner;
	}
	
	@Override
	protected boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (getRole().equals(player.getRole()) && getContext().isHuntersRevenge()) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected void performAction() {
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				context.addToRecentlyDeceased(player.getName());
				context.setHuntersRevenge(false);
				player.setCauseOfDeath(DeathCauses.SHOT);
				context.log(String.format("Player %s is shot by the hunter", name));
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		if (getContext().isDay()) {
			return DuskActivity.class;
		}
		return DawnActivity.class;
	}

}
