package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.DeathCauses;
import kom.aswang.enums.Role;

public class CultActivity extends SpinnerRoleActivity {

	@Override
	protected Role getRole() {
		return Role.PINUNO_NG_KULTO;
	}

	@Override
	protected int getContentView() {
		return R.layout.kulto;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.kulto_names_spinner;
	}

	@Override
	protected boolean isPlayerQualified(Player player) {
		return super.isPlayerQualified(player) && !player.isRecruited();
	}

	@Override
	protected void performAction() {
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				if (Role.ASWANG.equals(player.getRole())) {
					context.log(String.format("The cult leader recruited aswang %s, cult is infiltrated", name));
					killTheCult();
				} else {
					if (!isBarberGoingToKill(player)) {
						player.setRecruited(true);
						context.log(String.format("The cult leader recruited %s", name));
					}
				}
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		return ProstituteActivity.class;
	}
	
	private void killTheCult() {
		Context context = getContext();
		
		for (Player player : context.getPlayers()) {
			if (getRole().equals(player.getRole()) || player.isRecruited()) {
				context.addToRecentlyDeceased(player.getName());
				player.setCauseOfDeath(DeathCauses.ILLEGAL_RECRUITMENT);
				context.log(String.format("Player %s is killed for being a part of an aswang-infested cult", player.getName()));
				if (Role.MANGANGASO.equals(player.getRole())) {
					context.setHuntersRevenge(true);
					context.log("Player killed is a hunter");
				}
			}
		}
	}

}
