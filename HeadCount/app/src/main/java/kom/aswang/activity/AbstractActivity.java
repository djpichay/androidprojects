package kom.aswang.activity;

import kom.aswang.context.Context;
import kom.aswang.utils.Constants;
import android.app.Activity;
import android.text.Editable;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class AbstractActivity extends Activity {

	protected String getStringValueFromTextField(int id) {
		EditText editText = (EditText) findViewById(id);
		Editable stringText = editText.getText();
        return stringText.toString();
	}
	
	protected Integer getIntegerValueFromTextField(int id) throws Exception {
		EditText integerText = (EditText) findViewById(id);
        Integer integerValue = Integer.valueOf(integerText.getText().toString());
        return integerValue;
	}
	
	protected void setTextToTextField(int id, String text) {
		TextView textView = (TextView) findViewById(id);
		textView.setText(text);
	}
	
	protected Boolean getBooleanFromCheckbox(int id) {
		CheckBox checkbox = (CheckBox) findViewById(id);
		return checkbox.isChecked();
	}
	
	protected Context getContext() {
		Context context = (Context) getIntent().getSerializableExtra(Constants.CONTEXT);
		return context;
	}
	
}
