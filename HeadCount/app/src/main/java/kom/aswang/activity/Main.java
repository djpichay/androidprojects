package kom.aswang.activity;

import kom.aswang.activity.R;
import kom.aswang.context.Context;
import kom.aswang.utils.Constants;
import kom.aswang.utils.ShuffleUtil;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class Main extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void additionalCharacters(View view) {
		Intent intent = new Intent(this, AdditionalCharacters.class);
		try {
			intent.putExtra(Constants.CONTEXT, initContext());
	        startActivity(intent);
		} catch(Exception e) {
			Toast.makeText(this, "Maglagay ng bilang ng manlalaro at aswang", Toast.LENGTH_SHORT).show();		
		}
	}
	
	public void shuffle(View view) {
		Intent intent = new Intent(this, PlayerActivity.class);
		try {
			Context context = ShuffleUtil.shuffle(initContext());
			context.clearRoles();
			intent.putExtra(Constants.CONTEXT, context);
			startActivity(intent);
		} catch(Exception e) {
			Toast.makeText(this, "Maglagay ng bilang ng manlalaro at aswang", Toast.LENGTH_SHORT).show();		
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private Context initContext() throws Exception {
		Context context = new Context();
		context.setNumAswangs(this.getIntegerValueFromTextField(R.id.bilang_ng_aswang_text));
		context.setNumPlayers(this.getIntegerValueFromTextField(R.id.bilang_ng_manlalaro_text));
		context.log(String.format("Moderator picked %d players with %d aswangs", context.getNumPlayers(), context.getNumAswangs()));
		return context;
	}

}
