package kom.aswang.activity;

import kom.aswang.activity.roles.WitchActivity;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PredictionActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		if (!showActivity()) {
			nextActivity();
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.prediction);
		
		TextView predictionText = (TextView) findViewById(R.id.prediction_text);
		
		for (Player player : getContext().getPlayers()) {
			if (player.isPredicted()) {
				predictionText.setText("Si " + player.getName() + " ay isang " + player.getRole().getName());
			}
		}
		
	}
	
	public void next(View view) {
		this.finish();
		nextActivity();
	}
	
	private void nextActivity() {
		Intent intent = new Intent(this, WitchActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (Role.MANGHUHULA.equals(player.getRole()) && player.isAlive() && !player.isDrunk()) {
				return true;
			}
		}
		return false;
	}
	
}
