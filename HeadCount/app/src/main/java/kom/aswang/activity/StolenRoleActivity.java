package kom.aswang.activity;

import kom.aswang.activity.R;
import kom.aswang.activity.roles.CouplesActivity;
import kom.aswang.context.Context;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class StolenRoleActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stolen_role);
		Context context = getContext();
		setTextToTextField(R.id.ninakaw_na_papel_text, getTextToField(context));
	}
	
	public void next(View view) {
		StolenRoleActivity.this.finish();
		Intent intent = new Intent(this, CouplesActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
	private String getTextToField(Context context) {
		Role role = context.getPlayerById(context.getThief()).getRole();
		String roleName = role.getName();
		if (role.getNumber() > 1) {
			return "Ikaw ay isa na sa mga " + roleName;
		} else {
			return "Ikaw ay naging isa ng " + roleName;
		}
	}
	
}
