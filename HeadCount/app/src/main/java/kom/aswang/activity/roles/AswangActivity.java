package kom.aswang.activity.roles;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.DeathCauses;
import kom.aswang.enums.Role;

public class AswangActivity extends SpinnerRoleActivity {

	@Override
	protected boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (Role.ASWANG.equals(player.getRole())) {
				if (player.isAlive() && !player.isDrunk()) {
					return true;
				}
			}
		}
		getContext().log("All aswangs are incapacitated. No eating tonight");
		return false;
	}
	
	@Override
	protected Role getRole() {
		return Role.ASWANG;
	}

	@Override
	protected int getContentView() {
		return R.layout.aswang;
	}

	@Override
	protected int getSpinnerId() {
		return R.id.aswang_names_spinner;
	}

	@Override
	protected void performAction() {
		Context context = getContext();
		
		List<Player> players = context.getPlayers();
		
		String name = namesSpinner.getSelectedItem().toString();
		
		for (Player player : players) {
			if (name.equals(player.getName())) {
				context.setRecentAswangVictim(player.getName());
				if (!player.isHealed()) {
					if (!Role.MAGSASAKA.equals(player.getRole()) && !Role.MANANAHI.equals(player.getRole())) {
						context.addToRecentlyDeceased(player.getName());
						player.setCauseOfDeath(DeathCauses.EATEN);
						context.log(String.format("Player %s is devoured by the aswangs", name));
						if (Role.MANGANGASO.equals(player.getRole())) {
							context.setHuntersRevenge(true);
							context.log(String.format("Player killed is a hunter"));
						}
					} else {
						if (Role.MAGSASAKA.equals(player.getRole())) {
							if (context.isFarmerToughness()) {
								context.setFarmerToughness(false);
								context.setRecentAswangVictim("");
								context.log(String.format("Farmer's toughness saved him from being devoured", name));
							} else {
								context.addToRecentlyDeceased(player.getName());
								player.setCauseOfDeath(DeathCauses.EATEN);
								context.log(String.format("Farmer %s finally devoured by the aswangs", name));
							}
						}
						if (Role.MANANAHI.equals(player.getRole())) {
							if (!context.isTailorBleeding()) {
								context.setTailorBleeding(true);
								context.log(String.format("Tailor is attacked, but stitches self. Wounds would reopen next night", name));
							}
							context.setRecentAswangVictim("");
						}
					}
				}
			}
		}
	}

	@Override
	protected Class<?> getNextActivity() {
		return SeerActivity.class;
	}

}
