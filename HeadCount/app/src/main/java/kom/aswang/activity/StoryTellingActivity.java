package kom.aswang.activity;

import java.util.List;

import kom.aswang.activity.R;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.Switch;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class StoryTellingActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.story_telling);
		List<Player> players = getContext().getPlayers();
		if (null != players && players.size() > 0) {
			
			TextView aswangNumberText = (TextView) findViewById(R.id.bilang_ng_aswang_text2);
			aswangNumberText.setText("" + getContext().getNumAswangs());
			
			TextView mamamayanNumberText = (TextView) findViewById(R.id.bilang_ng_mamamayan_text);
			mamamayanNumberText.setText(""+(getContext().getNumPlayers() - getContext().getNumAswangs()));
			
			LinearLayout linearLayout = (LinearLayout) findViewById(R.id.story_telling_id);
			for (final Player player : players) {
				final Switch pswitch = new Switch(getApplication());
	            pswitch.setText(player.getName()+ " (" + player.getRole().getName() + ")" + (player.isMayor() ? "(Alkalde)":""));
	            pswitch.setChecked(true);
	            pswitch.setTextSize(18F);
	            pswitch.setTextColor(Color.WHITE);
	            pswitch.setOnClickListener( new View.OnClickListener() {	
					@Override
					public void onClick(View v) {
						if (Role.ASWANG.equals(player.getRole())) {
							if (pswitch.isChecked()) {
								TextView aswangNumberText = (TextView) findViewById(R.id.bilang_ng_aswang_text2);
								aswangNumberText.setText("" + (Integer.parseInt(aswangNumberText.getText().toString()) + 1));
							} else {
								TextView aswangNumberText = (TextView) findViewById(R.id.bilang_ng_aswang_text2);
								aswangNumberText.setText("" + (Integer.parseInt(aswangNumberText.getText().toString()) - 1));
							}
						} else {
							if (pswitch.isChecked()) {
								TextView mamamayanNumberText = (TextView) findViewById(R.id.bilang_ng_mamamayan_text);
								mamamayanNumberText.setText("" + (Integer.parseInt(mamamayanNumberText.getText().toString()) + 1));
							} else {
								TextView mamamayanNumberText = (TextView) findViewById(R.id.bilang_ng_mamamayan_text);
								mamamayanNumberText.setText("" + (Integer.parseInt(mamamayanNumberText.getText().toString()) - 1));
							}
						}
					}
				});
	            LinearLayout.LayoutParams layoutParams =
	                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
	                            LinearLayout.LayoutParams.WRAP_CONTENT);
	            pswitch.setLayoutParams(layoutParams);
	            linearLayout.addView(pswitch);
			}
		}
	}
	
	public void finish(View view) {
		this.finish();
		Intent intent = new Intent(this, Main.class);
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
}
