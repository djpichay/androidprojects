package kom.aswang.activity;

import kom.aswang.activity.roles.ThiefActivity;
import kom.aswang.utils.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class NightActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.night);
	}
	
	public void next(View view) {
		this.finish();
		Intent intent = new Intent(this, ThiefActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
}
