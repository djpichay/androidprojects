package kom.aswang.activity;

import kom.aswang.activity.R;
import kom.aswang.context.Context;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;
import kom.aswang.utils.ShuffleUtil;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AdditionalCharacters extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.additional_characters);
		
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.additional_characters_id);
		for (Role role : Role.values()) {
			if (0 != role.getComponent()) {
				final CheckBox checkbox = new CheckBox(getApplication());
				checkbox.setText(role.getName());
				checkbox.setTextColor(Color.WHITE);
				checkbox.setId(role.getComponent());
				linearLayout.addView(checkbox);
			}
		}
	}
	
	public void shuffle(View view) {
		Intent intent = new Intent(this, PlayerActivity.class);
		Context context = ShuffleUtil.shuffle(getContext());
		if (null != context) {
			this.finish();
			intent.putExtra(Constants.CONTEXT, ShuffleUtil.shuffle(getContext()));
			startActivity(intent);
		} else {
			Toast.makeText(this, "Ang bilang ng karagdagang tauhan ay kailangang bawasan", Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	protected Context getContext() {
		Context context = (Context) getIntent().getSerializableExtra(Constants.CONTEXT);
		context.clearPlayers();
		context.clearRoles();
		for (Role role : Role.values()) {
			int component = role.getComponent();
			if (component != 0 && getBooleanFromCheckbox(component)) {
				for (int i = 0; i < role.getNumber(); i++) {
					context.addRoleAsAvailable(role);
					if (Role.PANATIKO.equals(role)) {
						context.addRoleAsAvailable(Role.ARTISTA);
					}
				}
			}
		}
		return context;
	}
	
}
