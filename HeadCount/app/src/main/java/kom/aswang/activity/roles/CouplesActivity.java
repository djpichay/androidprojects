package kom.aswang.activity.roles;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import kom.aswang.activity.AbstractActivity;
import kom.aswang.activity.R;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;
import kom.aswang.utils.Constants;

public class CouplesActivity extends AbstractActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		if (!showActivity()) {
			nextActivity();
		}
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.couples);
	}
	
	public void next(View view) {
		this.finish();
		nextActivity();
	}
	
	private boolean showActivity() {
		for (Player player : getContext().getPlayers()) {
			if (Role.MAGKASINTAHAN.equals(player.getRole())) {
				return getContext().isFirstNight();
			}
		}
		return false;
	}
	
	private void nextActivity() {
		Intent intent = new Intent(this, DrunkActivity.class);
		intent.putExtra(Constants.CONTEXT, getContext());
		startActivity(intent);
	}
	
}
