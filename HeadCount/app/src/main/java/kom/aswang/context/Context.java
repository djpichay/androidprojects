package kom.aswang.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kom.aswang.entity.Player;
import kom.aswang.enums.Role;

public class Context implements Serializable {
	
	private static final long serialVersionUID = 1327184495638399658L;
	
	private boolean day;
	private boolean firstNight;
	
	private int currentPlayer;
	
	private int thief;
	
	private int numPlayers;
	private int numAswangs;
	
	private List<Player> players;
	private List<Role> availableRoles;

	private String recentlyHealed;
	
	private String recentAswangVictim;
	private List<String> recentlyDeceased;
	
	private boolean mayorVoted;
	private List<String> eligibleVoters;
	private String[] suspects;
	private Map<String, Integer> voteMap;
	private List<String> convicts;
	private String convicted;
	
	private boolean witchDeathPotionUsed;
	private boolean witchRevivingPotionUsed;
	
	private boolean huntersRevenge;
	private boolean farmerToughness;
	private boolean tailorBleeding;
	private boolean shepherdMourning;
	
	private List<String> logs;
	
	public Context() {
		day = false;
		firstNight = true;
		farmerToughness = true;
		currentPlayer = 0;
		thief = 0;
		players = new ArrayList<Player>();
		availableRoles = new ArrayList<Role>();
		recentlyDeceased = new ArrayList<String>(); 
		eligibleVoters = new ArrayList<String>();
		voteMap = new HashMap<String, Integer>();
		convicts = new ArrayList<String>();
		logs = new ArrayList<String>();
		recentAswangVictim = "";
	}
		
	public boolean isDay() {
		return day;
	}

	public void setDay(boolean day) {
		this.day = day;
	}
	
	public boolean isFirstNight() {
		return firstNight;
	}

	public void setFirstNight(boolean firstNight) {
		this.firstNight = firstNight;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}
	
	public void incrementCurrentPlayer() {
		currentPlayer++;
	}
	
	public boolean allPlayersAccountedFor() {
		return currentPlayer >= numPlayers;
	}
	
	public int getThief() {
		return thief;
	}

	public void setThief(int thief) {
		this.thief = thief;
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public int getNumAswangs() {
		return numAswangs;
	}

	public void setNumAswangs(int numAswangs) {
		this.numAswangs = numAswangs;
	}

	public List<Player> getPlayers() {
		return players;
	}
	
	public Player getPlayerById(int id) {
		return players.get(id);
	}

	public void addPlayer(Player player) {
		players.add(player);
	}
	
	public List<Role> getAvailableRoles() {
		return availableRoles;
	}
	
	public void addRoleAsAvailable(Role role) {
		availableRoles.add(role);
	}
	
	public List<String> getRecentlyDeceased() {
		return recentlyDeceased;
	}

	public void addToRecentlyDeceased(String name) {
		if (!recentlyDeceased.contains(name)) {
			recentlyDeceased.add(name);	
		}
	}
	
	public void reviveRecentlyDeceased(String name) {
		if (recentlyDeceased.contains(name)) {
			recentlyDeceased.remove(name);
		}
	}
	
	public String getRecentlyHealed() {
		return recentlyHealed;
	}

	public void setRecentlyHealed(String recentlyHealed) {
		this.recentlyHealed = recentlyHealed;
	}

	public String getRecentAswangVictim() {
		return recentAswangVictim;
	}

	public void setRecentAswangVictim(String recentAswangVictim) {
		this.recentAswangVictim = recentAswangVictim;
	}
	
	public List<String> getEligibleVoters() {
		return eligibleVoters;
	}

	public String getCurrentVoter() {
		return eligibleVoters.get(currentPlayer);
	}
	
	public void addEligibleVoter(String name) {
		eligibleVoters.add(name);
	}

	public String[] getSuspects() {
		return suspects;
	}
	
	public void setSuspects(String[] suspects) {
		this.suspects = suspects;
	}
	
	public void voteFor(String name, boolean isMayor) {
		
		Integer vote = 1;
		
		if (isMayor) {
			this.log("This is the mayor casting his vote");
			vote = 2;
		}
		
		mayorVoted = isMayor;
		
		if (voteMap.containsKey(name)) {
			vote = voteMap.get(name) + 1;
			if (isMayor) {
				vote++;
			}
		}
		
		voteMap.put(name, vote);
		
		int highestNumberOfVote = 0;
		int secondHighestNumberOfVote = 0;
		List<String> leadingSuspects = new ArrayList<String>();
		
		for (Map.Entry<String, Integer> entry : voteMap.entrySet()) {
			this.logs.add(String.format("The votes for %s is %d", entry.getKey(), entry.getValue()));
			if (entry.getValue() > highestNumberOfVote) {
				secondHighestNumberOfVote = highestNumberOfVote;
				highestNumberOfVote = entry.getValue();
				leadingSuspects.clear();
				leadingSuspects.add(entry.getKey());
				this.logs.add(String.format("Player %s is currently leading the lynching.", entry.getKey()));
			} else if (entry.getValue() == highestNumberOfVote) {
				leadingSuspects.add(entry.getKey());
				this.logs.add(String.format("Player %s is now tied with the highest vote getter.", entry.getKey()));
			} else if (entry.getValue() > secondHighestNumberOfVote) {
				secondHighestNumberOfVote = entry.getValue();
			}
		}
		
		int remainingVotes = eligibleVoters.size() - (currentPlayer + 1);
		
		if (!mayorVoted) {
			remainingVotes++;
		}
		
		if (highestNumberOfVote > secondHighestNumberOfVote + remainingVotes && leadingSuspects.size() == 1) {
			convicts = leadingSuspects;
			this.logs.add(String.format("The number of remaining votes %d, will not help to unlynch the highest vote %d", remainingVotes, highestNumberOfVote));
		} 
		if (currentPlayer + 1 >= eligibleVoters.size()) {
			convicts = leadingSuspects;
		}
		
	}
	
	public List<String> getConvicts() {
		return convicts;
	}
	
	public String getConvicted() {
		return convicted;
	}

	public void setConvicted(String convicted) {
		this.convicted = convicted;
	}

	public boolean isWitchDeathPotionUsed() {
		return witchDeathPotionUsed;
	}

	public void setWitchDeathPotionUsed(boolean witchDeathPotionUsed) {
		this.witchDeathPotionUsed = witchDeathPotionUsed;
	}

	public boolean isWitchRevivingPotionUsed() {
		return witchRevivingPotionUsed;
	}

	public void setWitchRevivingPotionUsed(boolean witchRevivingPotionUsed) {
		this.witchRevivingPotionUsed = witchRevivingPotionUsed;
	}
	
	public boolean isHuntersRevenge() {
		return huntersRevenge;
	}

	public void setHuntersRevenge(boolean huntersRevenge) {
		this.huntersRevenge = huntersRevenge;
	}
	
	public boolean isFarmerToughness() {
		return farmerToughness;
	}

	public void setFarmerToughness(boolean farmerToughness) {
		this.farmerToughness = farmerToughness;
	}
	
	public boolean isTailorBleeding() {
		return tailorBleeding;
	}

	public void setTailorBleeding(boolean tailorBleeding) {
		this.tailorBleeding = tailorBleeding;
	}
	
	public boolean isShepherdMourning() {
		return shepherdMourning;
	}

	public void setShepherdMourning(boolean shepherdMourning) {
		this.shepherdMourning = shepherdMourning;
	}

	public List<String> getLogs() {
		return logs;
	}
	
	public void log(String log) {
		logs.add(log);
	}

	public void clearPlayers() {
		players.clear();
	}
	
	public void clearRoles() {
		availableRoles.clear();
	}
	
	public void clearForAnotherDay() {
		recentAswangVictim = "";
		recentlyDeceased.clear();
	}
	
	public void clearVotingSlate() {
		eligibleVoters.clear();
		suspects = null;
		voteMap.clear();
		currentPlayer = 0;
		convicts.clear();
		mayorVoted = false;
	}
	
}
