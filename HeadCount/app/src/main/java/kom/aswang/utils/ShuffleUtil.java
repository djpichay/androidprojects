package kom.aswang.utils;

import kom.aswang.context.Context;
import kom.aswang.entity.Player;
import kom.aswang.enums.Role;

public class ShuffleUtil {

	public static Context shuffle(Context context) {
		
		int playerSize = context.getNumPlayers();
		int aswangSize = context.getNumAswangs();
		int otherRolesSize = context.getAvailableRoles().size();
		int civilianSize = playerSize - aswangSize - otherRolesSize;
		
		if(civilianSize < 0) {
			return null;
		}
		
		for (int i = 0; i < aswangSize; i++) {
			context.addRoleAsAvailable(Role.ASWANG);
		}
		
		for (int i = 0; i < civilianSize; i++) {
			context.addRoleAsAvailable(Role.TAUMBAYAN);
		}
		
		for (int i = 0; i < playerSize; i++) {
			int availableRolesSize = context.getAvailableRoles().size();
			int random = (int) ((Math.random() * 100) % availableRolesSize);
			Role role = context.getAvailableRoles().get(random);
			context.addPlayer(new Player(i, role));
			context.getAvailableRoles().remove(role);
		}
		
		return context;
		
	}
	
	
	
}
