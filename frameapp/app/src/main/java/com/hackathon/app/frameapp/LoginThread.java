package com.hackathon.app.frameapp;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * Created by DOMINADOR.PICHAY on 6/18/2015.
 */
public class LoginThread implements Runnable {

    private static final String LOG_TAG = "LoginThread";

    private Context context;

    private JSONObject data;

    private String error;

    public LoginThread(Context context, JSONObject data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public void run() {

        synchronized (this) {

            String hostname = ResourceManager.getResourceString(context, R.string.host);

            if (!ConnectivityUtils.isHostAvailable(hostname)) {
                error = "Host is not available.";
                Log.i(LOG_TAG, "Host " + hostname + " not available..");
                notify();
                return;
            }

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost(
                    hostname
                            + ResourceManager.getResourceString(context, R.string.account_api)
                            + ResourceManager.getResourceString(context, R.string.login_api));
            post.setHeader("content-type", "application/json");

            try {

                StringEntity entity = new StringEntity(data.toString());
                post.setEntity(entity);

                HttpResponse resp = httpClient.execute(post);

                String respStr = EntityUtils.toString(resp.getEntity());
                JSONObject response = new JSONObject(respStr);

                Log.i(LOG_TAG, "Response: " + respStr);
                final String CODE = ResourceManager.getResourceString(context, R.string.code);
                if (response.has(CODE)) {
                    String code = (String)response.get(CODE);
                    if (code.equals("0")) {
                        error = "";
                    } else if (code.equals("1")) {
                        error = "Unknown username.";
                    } else if (code.equals("2")) {
                        error = "Incorrect password.";
                    }
                }
            } catch (Exception e) {
                error = "Encountered an exception on logging in.";
                Log.e(LOG_TAG, "Encountered an exception: " + e.getCause());;
            } finally {
                notify();
            }

        }
    }

    public String getError() {
        return error;
    }
}
