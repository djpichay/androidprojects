package com.hackathon.app.frameapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class CustomIntentScheduler {

    private static final long MILLISECOND_MULTIPLIER = 1000;

    private Context context;
    private String intent;
    private long interval;
    private long startDelay;

    private CustomIntentScheduler(Builder builder) {
        this.context = builder.context;
        this.intent = builder.intent;
        this.interval = builder.interval;
        this.startDelay = builder.startDelay;
    }

    public static Builder forIntent(String intent) {
        return new Builder(intent);
    }

    private void schedule() {
        Intent action = new Intent();
        action.setAction(intent);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        long timeInterval = MILLISECOND_MULTIPLIER * interval;
        long timeStart = System.currentTimeMillis() + (startDelay * MILLISECOND_MULTIPLIER);
        am.setRepeating(AlarmManager.RTC_WAKEUP, timeStart, timeInterval, pi);
    }

    private void cancel() {
        Intent action = new Intent();
        action.setAction(intent);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
        pi.cancel();
    }

    public static class Builder {

        private Context context;
        private String intent;
        private long interval;
        private long startDelay;

        public Builder(String intent) {
            this.intent = intent;
            this.startDelay = 0;
        }

        public Builder setContext(Context context) {
            this.context = context;
            return this;
        }

        public Builder startAt(long startDelay) {
            this.startDelay = startDelay;
            return this;
        }

        public Builder withInterval(long interval) {
            this.interval = interval;
            return this;
        }

        public void schedule() {
            new CustomIntentScheduler(this).schedule();
        }

        public void cancel() {
            new CustomIntentScheduler(this).cancel();
        }

    }

}
