package com.hackathon.app.frameapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class FrameAppReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "FrameAppReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action  = intent.getAction();
        Log.i(LOG_TAG, "Receive action: " + action);

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            String enabledFileContent = ResourceManager.getFileContent(context, R.string.enabled_file);
            String enabled = ResourceManager.getResourceString(context, R.string.enabled);
            if (enabled.equals(enabledFileContent)) {
                context.startService(new Intent(context, CameraService.class));
                CustomIntentScheduler.forIntent(PhotoUploader.UPLOADER_NAME)
                        .setContext(context)
                        .withInterval(ResourceManager.getResourceInt(context, R.integer.camera_service_interval))
                        .startAt(ResourceManager.getResourceInt(context, R.integer.camera_service_interval))
                        .schedule();
            }
        } else if (action.equals(PhotoUploader.UPLOADER_NAME)) {
            Log.i(LOG_TAG, "Uploading photos..");
            PhotoUploader
                    .forContext(context)
                    .upload();
        } else if (action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            Log.i(LOG_TAG, "Received text message..");
            if (SMSReceiver
                    .forContext(context)
                    .withIntent(intent)
                    .process()) {
                Log.i(LOG_TAG, "Aborting broadcast of text message..");
                abortBroadcast();
            }
        }
    }

}
