package com.hackathon.app.frameapp;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class PhotoUploaderThread implements Runnable {

    private static final String LOG_TAG = "UploaderThread";

    private Context context;

    private JSONObject data;

    public PhotoUploaderThread(Context context, JSONObject data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public void run() {

        String hostname = ResourceManager.getResourceString(context, R.string.host);

        if (!ConnectivityUtils.isHostAvailable(hostname)) {
            Log.i(LOG_TAG, "Host " + hostname + " not available..");
            return;
        }

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(
                hostname
                        + ResourceManager.getResourceString(context, R.string.upload_api));
        post.setHeader("content-type", "application/json");

        try {

            StringEntity entity = new StringEntity(data.toString());
            post.setEntity(entity);

            HttpResponse resp = httpClient.execute(post);

            String respStr = EntityUtils.toString(resp.getEntity());
            JSONObject response = new JSONObject(respStr);

            Log.i(LOG_TAG, "Response: " + respStr);

            if (response.has("success") && response.getBoolean("success")) {
                File file = new File("/storage/extSdCard/" + data.getString("filename"));
                file.delete();
            }

        } catch (Exception e) {
            Log.e(LOG_TAG, "Encountered an exception: " + e.getCause());
        }

    }

}
