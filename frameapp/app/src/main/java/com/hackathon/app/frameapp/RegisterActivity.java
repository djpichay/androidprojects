package com.hackathon.app.frameapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends Activity {

    private static final String LOG_TAG = "RegisterActivity";

    private EditText usernameText;
    private EditText passwordText;
    private EditText confirmPasswordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.registration);

        usernameText = (EditText)findViewById(R.id.username_text);
        passwordText = (EditText)findViewById(R.id.password_text);
        confirmPasswordText = (EditText)findViewById(R.id.confirm_password_text);

        Button loginButton = (Button) findViewById(R.id.btnLogin);

        // Listening to register new account link
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                register(v);
            }
        });

    }

    public void register(View view) {

        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();
        String confirmPassword = confirmPasswordText.getText().toString();

        if (!hasLength(username) || !hasLength(password) || !hasLength(confirmPassword)) {
            Toast.makeText(getApplicationContext(), "All fields are required.",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if (!password.equals(confirmPassword)) {
            Toast.makeText(getApplicationContext(), "Password and Confirm Password are not the same.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        JSONObject data = new JSONObject();

        try {
            data.put(ResourceManager.getResourceString(getApplicationContext(), R.string.username_json),
                    usernameText.getText());
            data.put(ResourceManager.getResourceString(getApplicationContext(), R.string.password_json),
                    passwordText.getText());
        } catch(JSONException e) {
            Log.e(LOG_TAG, "Error encountered on registering your account:" + e.getMessage());
            Toast.makeText(getApplicationContext(), "Error encountered on registering your account", Toast.LENGTH_LONG).show();
            return;
        }

        Runnable provisionThread = new ProvisionThread(getApplicationContext(), data);
        Thread thread = new Thread(provisionThread);
        thread.start();

        synchronized (provisionThread) {
            try {
                Log.i(LOG_TAG, "Waiting for provisioning to complete..");
                provisionThread.wait();
            } catch(InterruptedException e){
                Log.e(LOG_TAG, "Encountered exception on registering: " + e.getMessage());
            }
        }

        Log.i(LOG_TAG, "Checking if provisioned");

        String provisionedFileContent = ResourceManager.getFileContent(getApplicationContext(), R.string.provisioned_file);
        if (hasLength(provisionedFileContent)) {
            this.finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Cannot register your account. " + ((ProvisionThread)provisionThread).getError(),
                    Toast.LENGTH_LONG).show();
        }

    }

    private boolean hasLength(String string) {
        if (null != string && string.trim().length() > 0) {
            return true;
        }
        return false;
    }
}
