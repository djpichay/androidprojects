package com.hackathon.app.frameapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class PhotoUploader {

    public static final String UPLOADER_NAME = "com.hackathon.app.frameapp.PhotoUploader";

    private static final String LOG_TAG = "PhotoUploader";

    private Context context;

    public static Builder forContext(Context context) {
        return new Builder(context);
    }

    private PhotoUploader(Builder b) {
        this.context = b.context;
    }

    public void sendPhotos() {

        if (!ConnectivityUtils.isConnectedToWifi(context)) {
            Log.i(LOG_TAG, "Not connected to wifi..");
            return;
        }

        File dir = new File("/storage/extSdCard/");
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                if (file.getName().startsWith("image")) {
                    Log.i(LOG_TAG, "Uploading " + file.getName());
                    sendPhoto(file);
                }
            }
        }

    }

    private void sendPhoto(File file) {
        JSONObject data = new JSONObject();

        try {
            data.put("gcmId", ResourceManager.getFileContent(context, R.string.provisioned_file));
            data.put("filename", file.getName());

            GPSTracker tracker = new GPSTracker(context);

            double latitude = 0D;
            double longitude = 0D;

            if (tracker.canGetLocation()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
            }

            data.put("latitude", latitude);
            data.put("longitude", longitude);

            FileInputStream fis = new FileInputStream(file);
            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();

            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            data.put("data", encodedImage);

        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }

        Runnable runnable = new PhotoUploaderThread(context, data);
        Thread thread = new Thread(runnable);

        Log.i(LOG_TAG, "Uploading data..");

        thread.start();

    }

    public static class Builder {

        private Context context;

        public Builder(Context context) {
            this.context = context;
        }

        public void upload() {
            new PhotoUploader(this).sendPhotos();
        }

    }

}
