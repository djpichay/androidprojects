package com.hackathon.app.frameapp;

import android.app.Service;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.IBinder;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class CameraService extends Service {

    public static final String SERVICE_NAME = "com.hackathon.app.frameapp.CAMERA_SERVICE";

    private static final String LOG_TAG = "CameraService";

    private Camera mCamera;

    private Camera.Parameters parameters;

    @Override
    public void onCreate()
    {
        super.onCreate();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        prepareCamera();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "Stopping Camera Service!");
        mCamera.setFaceDetectionListener(null);
        mCamera.release();
        mCamera = null;
        super.onDestroy();
    }

    Camera.PictureCallback mCall = new Camera.PictureCallback()
    {

        public void onPictureTaken(byte[] data, Camera camera)
        {
            //decode the data obtained by the camera into a Bitmap

            FileOutputStream outStream = null;
            try{
                Log.i(LOG_TAG, "Saving Picture!");
                outStream = new FileOutputStream("/storage/extSdCard/image_"+new Date().getTime()+".jpg");
                outStream.write(data);
                outStream.close();
            } catch (FileNotFoundException e){
                Log.e(LOG_TAG, e.getMessage());
            } catch (IOException e){
                Log.e(LOG_TAG, e.getMessage());
            }

        }

    };


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Camera.FaceDetectionListener faceDetectionListener = new Camera.FaceDetectionListener() {
        @Override
        public void onFaceDetection(Camera.Face[] faces, Camera camera) {
            try {
                if (faces.length == 0) {
                    Log.i(LOG_TAG, "No face detected!");
                    return;
                } else if (faces.length == 1) {
                    Log.i(LOG_TAG, "Face detected, taking picture!");
                    mCamera.takePicture(null, null, mCall);
                    Thread.sleep(20000);
                    prepareCamera();
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    };

    private void prepareCamera() {
        try {
            safeCameraOpen(getFrontCameraId());
            SurfaceTexture surfaceTexture = new SurfaceTexture(10);
            mCamera.setPreviewTexture(surfaceTexture);
            parameters = mCamera.getParameters();
            //set camera parameters
            mCamera.setParameters(parameters);
            mCamera.startPreview();

            mCamera.setFaceDetectionListener(faceDetectionListener);
            mCamera.startFaceDetection();

        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    private void safeCameraOpen(int id) {

        try {
            releaseCameraAndPreview();
            mCamera = Camera.open(id);
        } catch (Exception e) {
            Log.e(LOG_TAG, "failed to open Camera");
        }

    }

    private void releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    private int getFrontCameraId(){
        int camId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo ci = new Camera.CameraInfo();

        for(int i = 0;i < numberOfCameras;i++){
            Camera.getCameraInfo(i,ci);
            if(ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT){
                camId = i;
            }
        }

        return camId;
    }

}
