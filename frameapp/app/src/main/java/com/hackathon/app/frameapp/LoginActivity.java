package com.hackathon.app.frameapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity {

    private static final String LOG_TAG = "LoginActivity";

    private EditText usernameText;
    private EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);

        usernameText = (EditText)findViewById(R.id.login_username_text);
        passwordText = (EditText)findViewById(R.id.login_password_text);

    }

    public void login(View view) {

        String username = usernameText.getText().toString();
        String password = passwordText.getText().toString();

        if (!hasLength(username) || !hasLength(password)) {
            Toast.makeText(getApplicationContext(), "All fields are required.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        JSONObject data = new JSONObject();

        try {
            data.put(ResourceManager.getResourceString(getApplicationContext(), R.string.username_json),
                    usernameText.getText());
            data.put(ResourceManager.getResourceString(getApplicationContext(), R.string.password_json),
                    passwordText.getText());
        } catch(JSONException e) {
            Log.e(LOG_TAG, "Error encountered on registering your account:" + e.getMessage());
            Toast.makeText(getApplicationContext(), "Error encountered on registering your account", Toast.LENGTH_LONG).show();
            return;
        }

        Runnable loginThread = new LoginThread(getApplicationContext(), data);
        Thread thread = new Thread(loginThread);
        thread.start();

        synchronized (loginThread) {
            try {
                Log.i(LOG_TAG, "Waiting for logging in to complete..");
                loginThread.wait();
            } catch(InterruptedException e){
                Log.e(LOG_TAG, "Encountered exception on logging in: " + e.getMessage());
            }
        }

        String error = ((LoginThread)loginThread).getError();

        if (hasLength(error)) {
            Toast.makeText(getApplicationContext(), "Login failed. " + error,
                    Toast.LENGTH_LONG).show();
        } else {
            this.finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

    }

    private boolean hasLength(String string) {
        if (null != string && string.trim().length() > 0) {
            return true;
        }
        return false;
    }

}
