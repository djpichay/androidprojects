package com.hackathon.app.frameapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DOMINADOR.PICHAY on 6/18/2015.
 */
public class SMSReceiver {

    private static final String LOG_TAG = "SMSReceiver";

    private Context context;

    private Intent intent;

    public static Builder forContext(Context context) {
        return new Builder(context);
    }

    private SMSReceiver(Builder b) {
        this.context = b.context;
        this.intent = b.intent;
    }

    boolean process() {

        boolean result = false;

        Bundle extras = intent.getExtras();
        if (extras != null) {
            Object[] pdus = (Object[])extras.get("pdus");

            if (pdus.length < 1) return result; // Invalid SMS. Not sure that it's possible.

            StringBuilder sb = new StringBuilder();
            String sender = null;
            for (int i = 0; i < pdus.length; i++) {
                SmsMessage message = SmsMessage.createFromPdu((byte[]) pdus[i]);
                if (sender == null) sender = message.getOriginatingAddress();
                String text = message.getMessageBody();
                if (text != null) sb.append(text);
            }

            String textMessage = sb.toString();

            String[] tokens = textMessage.split(" ");
            if (tokens.length == 3) {
                String username = tokens[1];
                String password = tokens[2];
                if (login(username, password)) {
                    Log.i(LOG_TAG, "Credentials are correct updating lost tag!");
                    result = true;
                    if ("lost".equalsIgnoreCase(tokens[0])) {
                        Log.i(LOG_TAG, "Device is lost!");
                        updateLost(true);
                    } else if ("found".equalsIgnoreCase(tokens[0])) {
                        Log.i(LOG_TAG, "Device is found!");
                        updateLost(false);
                    }
                }
            }
        }

        return result;

    }

    private boolean login(String username, String password) {

        JSONObject data = new JSONObject();

        try {
            data.put(ResourceManager.getResourceString(context, R.string.username_json),
                    username);
            data.put(ResourceManager.getResourceString(context, R.string.password_json),
                    password);
        } catch(JSONException e) {
            return false;
        }

        Runnable loginThread = new LoginThread(context, data);
        Thread thread = new Thread(loginThread);
        thread.start();

        synchronized (loginThread) {
            try {
                Log.i(LOG_TAG, "Waiting for logging in to complete..");
                loginThread.wait();
            } catch(InterruptedException e){
                Log.e(LOG_TAG, "Encountered exception on logging in: " + e.getMessage());
                return false;
            }
        }

        String error = ((LoginThread)loginThread).getError();

        if (null != error && error.length() > 0) {
            return false;
        } else {
            return true;
        }

    }

    private void updateLost(boolean lost) {

        Runnable lostUpdaterThread = new LostUpdaterThread(context, lost);
        Thread thread = new Thread(lostUpdaterThread);
        thread.start();

        synchronized (lostUpdaterThread) {
            try {
                Log.i(LOG_TAG, "Waiting for update to complete..");
                lostUpdaterThread.wait();
            } catch(InterruptedException e){
                Log.e(LOG_TAG, "Encountered exception on updating: " + e.getMessage());
            }
        }

    }

    public static class Builder {

        private Context context;

        private Intent intent;

        Builder(Context context) {
            this.context = context;
        }

        public Builder withIntent(Intent intent) {
            this.intent = intent;
            return this;
        }

        public boolean process() {
            return new SMSReceiver(this).process();
        }

    }

}
