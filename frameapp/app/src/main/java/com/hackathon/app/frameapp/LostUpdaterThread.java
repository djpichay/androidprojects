package com.hackathon.app.frameapp;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * Created by DOMINADOR.PICHAY on 6/18/2015.
 */
public class LostUpdaterThread implements Runnable {

    private static final String LOG_TAG = "LoginThread";

    private Context context;

    private boolean lost;

    public LostUpdaterThread(Context context, boolean lost) {
        this.context = context;
        this.lost = lost;
    }

    @Override
    public void run() {

        synchronized (this) {

            String hostname = ResourceManager.getResourceString(context, R.string.host);

            if (!ConnectivityUtils.isHostAvailable(hostname)) {
                Log.i(LOG_TAG, "Host " + hostname + " not available..");
                notify();
                return;
            }

            HttpClient httpClient = new DefaultHttpClient();
            HttpPut put = new HttpPut(
                    hostname
                            + ResourceManager.getResourceString(context, R.string.account_api) + "/"
                            + ResourceManager.getFileContent(context, R.string.provisioned_file)
                            + ResourceManager.getResourceString(context, R.string.lost_api));
            put.setHeader("content-type", "application/json");

            try {

                JSONObject data = new JSONObject();
                data.put(ResourceManager.getResourceString(context, R.string.lost), lost);

                StringEntity entity = new StringEntity(data.toString());
                put.setEntity(entity);

                HttpResponse resp = httpClient.execute(put);

                String respStr = EntityUtils.toString(resp.getEntity());
                JSONObject response = new JSONObject(respStr);

                Log.i(LOG_TAG, "Response: " + respStr);

            } catch (Exception e) {
                Log.e(LOG_TAG, "Encountered an exception: " + e.getCause());;
            } finally {
                notify();
            }

        }
    }

}
