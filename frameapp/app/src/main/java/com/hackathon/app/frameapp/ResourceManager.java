package com.hackathon.app.frameapp;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class ResourceManager {

    private static final String LOG_TAG = "ResourceManager";

    public static String getResourceString(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static int getResourceInt(Context context, int id) {
        return context.getResources().getInteger(id);
    }

    public static String getFileContent(Context context, int id) {

        StringBuffer sb = new StringBuffer();
        String filename = getResourceString(context, id);
        FileInputStream inputStream;
        InputStreamReader inputStreamReader;
        BufferedReader bufferedReader;

        try {
            inputStream = context.openFileInput(filename);
            inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            String contents = "";

            while ( (contents = bufferedReader.readLine()) != null ) {
                sb.append(contents);
            }

            Log.i(LOG_TAG, "Got this from file: " + sb.toString());

            return sb.toString();

        } catch (Exception e) {
            Log.e(LOG_TAG, "File is not found!");
        }

        return "";

    }

    public static void setFileContent(Context context, int id, int content) {
        String filename = getResourceString(context, id);
        String status = getResourceString(context, content);
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(status.getBytes());
            outputStream.close();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Encountered an exception: " + e.getCause());
        }
    }

    public static void setFileContent(Context context, int id, String content) {
        String filename = getResourceString(context, id);
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Encountered an exception: " + e.getCause());
        }
    }

}
