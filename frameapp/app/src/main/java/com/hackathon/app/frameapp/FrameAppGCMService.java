package com.hackathon.app.frameapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class FrameAppGCMService extends GcmListenerService {

    private static final String LOG_TAG = "GCMService";

    @Override
    public void onMessageReceived(String from, Bundle data) {

        String message = data.getString("message");
        Log.i(LOG_TAG, "Message received: " + message);

        Context context = getApplicationContext();

        if (Boolean.valueOf(message)) {
            // start camera service, set enabled file and start uploader service
            Log.i(LOG_TAG, "Starting Camera and Upload Services..");
            startService(new Intent(CameraService.SERVICE_NAME));
            ResourceManager.setFileContent(context, R.string.enabled_file, R.string.enabled);
            CustomIntentScheduler.forIntent(PhotoUploader.UPLOADER_NAME)
                    .setContext(context)
                    .withInterval(ResourceManager.getResourceInt(context, R.integer.camera_service_interval))
                    .startAt(ResourceManager.getResourceInt(context, R.integer.camera_service_interval))
                    .schedule();
        } else {
            // stop camera service, set enabled file to disabled and stop uploader service
            Log.i(LOG_TAG, "Stopping Camera and Upload Services..");
            stopService(new Intent(CameraService.SERVICE_NAME));
            ResourceManager.setFileContent(context, R.string.enabled_file, R.string.disabled);
            CustomIntentScheduler.forIntent(PhotoUploader.UPLOADER_NAME)
                    .setContext(context).cancel();
        }

    }

}
