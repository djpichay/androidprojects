package com.hackathon.app.frameapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class ConnectivityUtils {

    private static final String LOG_TAG = "ConnectivityUtils";

    public static boolean isConnectedToWifi(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMan.getActiveNetworkInfo();
        return netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean isHostAvailable(String hostname) {
        Log.i(LOG_TAG, "Checking if host is alive: " + hostname);
        boolean result = false;
        try {
            URL url = new URL(hostname);
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "Android Application");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(10 * 1000);
            urlc.connect();
            result = (urlc.getResponseCode() == 200);
        } catch (Exception e) {
            Log.i(LOG_TAG, "Exception encountered: " + e.getMessage());
        }
        return result;
    }

}
