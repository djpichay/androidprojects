package com.hackathon.app.frameapp;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by DOMINADOR.PICHAY on 6/15/2015.
 */
public class ProvisionThread implements Runnable {

    private static final String LOG_TAG = "ProvisionThread";

    private Context context;

    private JSONObject data;

    private String error;

    public ProvisionThread(Context context, JSONObject data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public void run() {

        synchronized (this) {

            String hostname = ResourceManager.getResourceString(context, R.string.host);

            if (!ConnectivityUtils.isHostAvailable(hostname)) {
                error = "Host is not available.";
                Log.i(LOG_TAG, "Host " + hostname + " not available..");
                notify();
                return;
            }

            String token;

            try {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
                token = gcm.register(ResourceManager.getResourceString(context, R.string.app_id));
                Log.i(LOG_TAG, "Got registration id: " + token);
            } catch (IOException e) {
                error = "GCM Registration failed.";
                Log.e(LOG_TAG, "Exception on getting registration token: " + e.getMessage());
                notify();
                return;
            }

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost(
                    hostname
                            + ResourceManager.getResourceString(context, R.string.account_api) + "/" + token);
            post.setHeader("content-type", "application/json");

            try {

                StringEntity entity = new StringEntity(data.toString());
                post.setEntity(entity);

                HttpResponse resp = httpClient.execute(post);

                String respStr = EntityUtils.toString(resp.getEntity());
                JSONObject response = new JSONObject(respStr);

                Log.i(LOG_TAG, "Response: " + respStr);
                final String SUCCESS = ResourceManager.getResourceString(context, R.string.success);
                if (response.has(SUCCESS) && response.getBoolean(SUCCESS)) {
                    ResourceManager.setFileContent(context, R.string.provisioned_file, token);
                    error = "";
                    Thread.sleep(5000);
                } else {
                    error = "Registration unsuccessful.";
                }

            } catch (Exception e) {
                Log.e(LOG_TAG, "Encountered an exception: " + e.getCause());
            } finally {
                notify();
            }

        }

    }

    public String getError() {
        return error;
    }

}
