package com.hackathon.onthespot;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeSettings();
        retrieve();

        if (!isMyServiceRunning(OnTheSpotService.class)) {
            this.getApplicationContext().startService(new Intent(this.getApplicationContext(), OnTheSpotService.class));
        }

        if (null != this.getIntent().getStringExtra("appMessage")) {
            Toast.makeText(this, this.getIntent().getStringExtra("appMessage"), Toast.LENGTH_SHORT).show();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        DrawableCompat.setTintList(DrawableCompat.wrap(fab.getDrawable()), getApplicationContext().getResources().getColorStateList(R.color.white));
        DrawableCompat.setTintList(DrawableCompat.wrap(fab.getBackground()), getApplicationContext().getResources().getColorStateList(R.color.black));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    private void initializeSettings() {

        int count = AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().count();

        if (count == 0) {

            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().insert(new SettingsEntity(SettingsEnum.CHECK_INTERVAL.getColumn(), "60"));
            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().insert(new SettingsEntity(SettingsEnum.RADIUS.getColumn(), "200"));
            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().insert(new SettingsEntity(SettingsEnum.MESSAGE_CYCLE.getColumn(), "24"));

        }

    }

    private void retrieve() {

        List<OnTheSpotEntity> entities = AppDatabase.getAppDatabase(this.getApplicationContext()).entityDao().getAll();

        LinearLayout linearLayout = findViewById(R.id.list);

        linearLayout.removeAllViews();

        FloatingActionButton addButton = findViewById(R.id.fab);
        addButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        add();
                    }
                }
        );

        if (null == entities || entities.size() == 0) {
            TextView textView = new TextView(this);
            textView.setText("No items to show. Add items by pressing the button below.");
            textView.setTextSize(20);
            textView.setTextColor(Color.BLACK);
            linearLayout.addView(textView);
            return;
        }

        Collections.sort(entities, new Comparator<OnTheSpotEntity>() {
            @Override
            public int compare(OnTheSpotEntity entity1, OnTheSpotEntity entity2) {
                return entity1.getName().toLowerCase().compareTo(entity2.getName().toLowerCase());
            }
        });

        for (final OnTheSpotEntity entity : entities) {

            CardView cardView = new CardView(this);

            cardView.setUseCompatPadding(true);
            cardView.setContentPadding(30, 30, 30, 30);


            RelativeLayout itemLayout = new RelativeLayout(this);

            TextView textView = new TextView(this);
            textView.setId(("text"+entity.getId()).hashCode());
            textView.setText(entity.getName());
            textView.setTextSize(20);
            textView.setTextColor(Color.BLACK);
            textView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            update(entity);
                        }
                    }
            );

            itemLayout.addView(textView);

            RelativeLayout.LayoutParams textViewParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
            textViewParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            textView.setLayoutParams(textViewParams);

            FloatingActionButton floatingDeleteButton = new FloatingActionButton(this);
            floatingDeleteButton.setImageResource(android.R.drawable.ic_input_delete);
            floatingDeleteButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            delete(entity.getId(), entity.getName());
                        }
                    }
            );
            DrawableCompat.setTintList(DrawableCompat.wrap(floatingDeleteButton.getBackground()), getApplicationContext().getResources().getColorStateList(R.color.black));

            itemLayout.addView(floatingDeleteButton);

            RelativeLayout.LayoutParams deleteButtonParams = (RelativeLayout.LayoutParams) floatingDeleteButton.getLayoutParams();
            deleteButtonParams.addRule(RelativeLayout.ALIGN_PARENT_END);
            floatingDeleteButton.setLayoutParams(deleteButtonParams);

            cardView.addView(itemLayout);

            linearLayout.addView(cardView);

        }

    }

    private void add() {
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
    }

    private void delete(final String id, final String name) {
        new AlertDialog.Builder(this)
                .setTitle("Delete Item")
                .setMessage("Are you sure you want to delete " + name + "?" )
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        delete(id);
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void delete(String id) {
        AppDatabase.getAppDatabase(this.getApplicationContext()).entityDao().delete(id);
        retrieve();
        Toast.makeText(this, "Item Deleted", Toast.LENGTH_SHORT).show();
    }

    private void update(OnTheSpotEntity entity) {
        Intent intent = new Intent(this, AddActivity.class);
        intent.putExtra("entity", entity);
        startActivity(intent);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {}

}
