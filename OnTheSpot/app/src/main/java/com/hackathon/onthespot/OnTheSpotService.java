package com.hackathon.onthespot;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by djpichay on 2/7/18.
 */

public class OnTheSpotService extends Service {

    public static final String BROADCAST_ACTION = "Location";
    Intent intent;

    @Override
    public void onCreate() {
        Log.d("Service", "Creating Service");
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "Starting Service");
        OnTheSpotWhereabouts.newInstance(this.getApplicationContext());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        OnTheSpotWhereabouts.instance().stop();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
