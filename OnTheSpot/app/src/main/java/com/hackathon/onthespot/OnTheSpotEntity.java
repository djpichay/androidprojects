package com.hackathon.onthespot;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by djpichay on 2/6/18.
 */
@Entity(tableName = "onthespot")
public class OnTheSpotEntity implements Serializable {

    @PrimaryKey
    @NonNull
    private String id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "numbersToNotify")
    private String numbersToNotify;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "longitude")
    private double longitude;

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "lastSent")
    private long lastSent;

    public OnTheSpotEntity() {
        this.id = UUID.randomUUID().toString();
        this.lastSent = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumbersToNotify() {
        return numbersToNotify;
    }

    public void setNumbersToNotify(String numbersToNotify) {
        this.numbersToNotify = numbersToNotify;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public long getLastSent() {
        return lastSent;
    }

    public void setLastSent(long lastSent) {
        this.lastSent = lastSent;
    }

    @Override
    public String toString() {
        return "OnTheSpotEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", numbersToNotify=" + numbersToNotify +
                ", message='" + message + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", lastSent=" + lastSent +
                '}';
    }
}
