package com.hackathon.onthespot;

import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by djpichay on 2/13/18.
 */

public class OnTheSpotWhereaboutsWorker {

    public static void work(Context context, Double latitude, Double longitude) {

        DecimalFormat df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.CEILING);

        latitude = Double.parseDouble(df.format(latitude));
        longitude = Double.parseDouble(df.format(longitude));

        Log.d("LocationListener", "Location Changed to: " + longitude + " " + latitude);

        List<OnTheSpotEntity> entities = AppDatabase.getAppDatabase(context).entityDao().getAll();

        SettingsEntity toleranceEntity = AppDatabase.getAppDatabase(context).settingsDao().select(SettingsEnum.RADIUS.getColumn());
        Double radius = Double.parseDouble(toleranceEntity.getValue());
        radius = radius/100000D;

        SettingsEntity messageCycleEntity = AppDatabase.getAppDatabase(context).settingsDao().select(SettingsEnum.MESSAGE_CYCLE.getColumn());
        long messageCycle = Long.parseLong(messageCycleEntity.getValue());

        Log.d("LocationListener", "Configured " + radius+ " degree tolerance. Message Cycle: " + messageCycle);

        for (OnTheSpotEntity entity : entities) {

            Log.d("LocationListener", "Trying to send message for: " + entity);

            double entityLongitude = Double.parseDouble(df.format(entity.getLongitude()));
            double entityLatitude = Double.parseDouble(df.format(entity.getLatitude()));

            if (!(entityLongitude > longitude - radius && entityLongitude < longitude + radius)) {
                Log.d("LocationListener", "Not within the location's radius");
                continue;
            }

            if (!(entityLatitude > latitude - radius && entityLatitude < latitude + radius)) {
                Log.d("LocationListener", "Not within the location's radius");
                continue;
            }

            if (System.currentTimeMillis() - entity.getLastSent() < messageCycle * 60 * 60 * 1000) {
                Log.d("LocationListener", "This message was sent " + messageCycle + " hours ago");
                continue;
            }

            String[] numbers = entity.getNumbersToNotify().split(",");

            Log.d("LocationListener", "Sending message");

            for(String number : numbers) {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(number, null, "Sent from OnTheSpot: " + entity.getMessage(), null, null);
            }

            entity.setLastSent(System.currentTimeMillis());

            AppDatabase.getAppDatabase(context.getApplicationContext()).entityDao().delete(entity.getId());
            AppDatabase.getAppDatabase(context.getApplicationContext()).entityDao().insert(entity);

        }

    }

}
