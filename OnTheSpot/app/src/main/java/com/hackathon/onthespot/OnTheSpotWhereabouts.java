package com.hackathon.onthespot;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;

/**
 * Created by djpichay on 2/13/18.
 */

public class OnTheSpotWhereabouts {

    private static OnTheSpotWhereabouts instance;

    private static final String TAG = OnTheSpotWhereabouts.class.getSimpleName();

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private LocationSettingsRequest locationSettingsRequest;

    private OnTheSpotWhereabouts(final Context context) {

        SettingsEntity checkingIntervalSettings = AppDatabase.getAppDatabase(context).settingsDao().select(SettingsEnum.CHECK_INTERVAL.getColumn());
        long checkInterval = Long.parseLong(checkingIntervalSettings.getValue()) * 1000L;

        Log.d("OnTheSpotWhereabouts", "Checking location every: " + checkInterval);

        this.locationRequest = new LocationRequest();
        this.locationRequest.setInterval(checkInterval);
        this.locationRequest.setFastestInterval(checkInterval);
        this.locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(this.locationRequest);
        this.locationSettingsRequest = builder.build();

        this.locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location currentLocation = locationResult.getLastLocation();
                OnTheSpotWhereaboutsWorker.work(context, currentLocation.getLatitude(), currentLocation.getLongitude());
            }
        };

        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        this.mFusedLocationClient.requestLocationUpdates(this.locationRequest,
                this.locationCallback, Looper.myLooper());
    }

    public static OnTheSpotWhereabouts instance() {
        return instance;
    }

    public static OnTheSpotWhereabouts newInstance(Context context) {
        instance = new OnTheSpotWhereabouts(context);
        return instance;
    }

    public void stop() {
        Log.d(TAG, "stop() Stopping location tracking");
        this.mFusedLocationClient.removeLocationUpdates(this.locationCallback);
    }

}
