package com.hackathon.onthespot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

/**
 * Created by djpichay on 2/12/18.
 */

public class SettingsActivity extends AppCompatActivity {

    EditText checkingIntervalEditText;

    EditText toleranceEditText;

    EditText messageCycleEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        SettingsEntity checkingIntervalSetting = AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().select(SettingsEnum.CHECK_INTERVAL.getColumn());
        SettingsEntity toleranceSetting = AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().select(SettingsEnum.RADIUS.getColumn());
        SettingsEntity expirySetting = AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().select(SettingsEnum.MESSAGE_CYCLE.getColumn());

        checkingIntervalEditText = findViewById(R.id.checkingInterval);
        checkingIntervalEditText.setText(checkingIntervalSetting.getValue());

        toleranceEditText = findViewById(R.id.tolerance);
        toleranceEditText.setText(toleranceSetting.getValue());

        messageCycleEditText = findViewById(R.id.expiry);
        messageCycleEditText.setText(expirySetting.getValue());

    }

    public void apply(View view) {

        if (checkLength(checkingIntervalEditText)
                && checkLength(toleranceEditText)
                && checkLength(messageCycleEditText)
                && checkRangeOfInt(checkingIntervalEditText)
                && checkRangeOfInt(toleranceEditText)
                && checkRangeOfInt(messageCycleEditText)) {

            Intent intent = new Intent(this, MainActivity.class);

            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().deleteAll();
            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().insert(new SettingsEntity(SettingsEnum.CHECK_INTERVAL.getColumn(), checkingIntervalEditText.getText().toString()));
            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().insert(new SettingsEntity(SettingsEnum.RADIUS.getColumn(), toleranceEditText.getText().toString()));
            AppDatabase.getAppDatabase(this.getApplicationContext()).settingsDao().insert(new SettingsEntity(SettingsEnum.MESSAGE_CYCLE.getColumn(), messageCycleEditText.getText().toString()));

            stopService(new Intent(this, OnTheSpotService.class));

            intent.putExtra("appMessage", "Settings Updated");
            startActivity(intent);
        }

    }

    public void cancel(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private boolean checkLength(TextView view) {
        CharSequence cs = view.getText();
        String text = cs.toString();
        if (text.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkRangeOfInt(TextView view) {
        CharSequence cs = view.getText();
        String textInt = cs.toString();
        try {
            int intValue = Integer.parseInt(textInt);
        } catch (Exception e) {
            Toast.makeText(this, "You are setting an exceptionally high value", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {}

}
