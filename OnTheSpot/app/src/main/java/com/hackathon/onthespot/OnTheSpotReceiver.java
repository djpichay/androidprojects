package com.hackathon.onthespot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by djpichay on 2/7/18.
 */

public class OnTheSpotReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action  = intent.getAction();

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            context.startService(new Intent(context, OnTheSpotService.class));
        }
    }

}
