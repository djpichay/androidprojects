package com.hackathon.onthespot;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.android.ex.chips.recipientchip.DrawableRecipientChip;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by djpichay on 2/6/18.
 */

public class AddActivity extends AppCompatActivity {

    private String id;

    private TextView nameTextView;
    private TextView messageView;
    private TextView longitudeView;
    private TextView latitudeView;
    private RecipientEditTextView retv;

    private JSONObject locationJSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        id = "";

        nameTextView = findViewById(R.id.name);
        messageView = findViewById(R.id.message);
        longitudeView = findViewById(R.id.longitude);
        latitudeView = findViewById(R.id.latitude);

        retv = findViewById(R.id.recipientEditTextView);
        retv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        BaseRecipientAdapter baseRecipientAdapter = new BaseRecipientAdapter(BaseRecipientAdapter.QUERY_TYPE_PHONE, this);
        baseRecipientAdapter.setShowMobileOnly(false);
        retv.setAdapter(baseRecipientAdapter);

        Button saveButton = findViewById(R.id.save);
        saveButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        save();
                    }
                }
        );

        if (null != this.getIntent().getSerializableExtra("entity")) {
            OnTheSpotEntity entity = (OnTheSpotEntity) this.getIntent().getSerializableExtra("entity");
            nameTextView.setText(entity.getName());

            String[] numbers = entity.getNumbersToNotify().split(",");
            for (String number : numbers) {
                retv.append(number);
            }

            messageView.setText(entity.getMessage());
            longitudeView.setText(""+entity.getLongitude());
            latitudeView.setText(""+entity.getLatitude());
            id = entity.getId();
            saveButton.setText("Update");
        }

        Button cancelButton = findViewById(R.id.cancel);
        cancelButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cancel();
                    }
                }
        );

        Button searchButton = findViewById(R.id.searchLocation);
        searchButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        promptSearchLocation();
                    }
                }
        );

        Button enterManuallyButton = findViewById(R.id.enterManually);
        enterManuallyButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        longitudeView.setEnabled(true);
                        latitudeView.setEnabled(true);
                    }
                }
        );

    }

    private void save() {

        String appMessage = "Item Created";

        if (id.length() != 0) {
            AppDatabase.getAppDatabase(this.getApplicationContext()).entityDao().delete(id);
            appMessage = "Item Modified";
        }

        String numbersToNotify = "";

        DrawableRecipientChip[] chips = retv.getSortedRecipients();

        if (chips.length == 0) {
            numbersToNotify = retv.getText().toString();
            if (numbersToNotify.trim().length() == 0) {
                Toast.makeText(this, "No contacts specified", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            StringBuilder sb = new StringBuilder();
            for (DrawableRecipientChip chip : chips) {
                sb.append(chip.getValue() + ",");
            }
            numbersToNotify = sb.substring(0, sb.lastIndexOf(","));
        }

        if (checkLength(latitudeView) &&
                checkLength(longitudeView) &&
                checkLength(nameTextView) &&
                checkLength(messageView)) {

            OnTheSpotEntity entity = new OnTheSpotEntity();
            entity.setLatitude(Double.parseDouble(latitudeView.getText().toString()));
            entity.setLongitude(Double.parseDouble(longitudeView.getText().toString()));
            entity.setName(nameTextView.getText().toString());
            entity.setNumbersToNotify(numbersToNotify);
            entity.setMessage(messageView.getText().toString());

            AppDatabase.getAppDatabase(this.getApplicationContext()).entityDao().insert(entity);

            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("appMessage", appMessage);
            startActivity(intent);

        }

    }

    private void cancel() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void promptSearchLocation() {

        locationJSON = null;

        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.search_location, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        alertDialogBuilder.setTitle("Pick Your Spot");

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        Button searchButton = promptsView.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(new SearchLocationThread(promptsView)).start();
                    }
                }
        );

        final EditText userInput = promptsView
                .findViewById(R.id.locationInput);

        final LinearLayout linearLayout = promptsView.findViewById(R.id.searchLocationDetails);

        Button clearButton = promptsView.findViewById(R.id.clearButton);
        clearButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userInput.setText("");
                        linearLayout.removeAllViews();
                        locationJSON = null;
                    }
                }
        );

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (setLongLat(promptsView)) {
                            alertDialog.dismiss();
                        }
                    }
                });
            }
        });

        // show it
        alertDialog.show();

    }

    private boolean checkLength(TextView view) {
        CharSequence cs = view.getText();
        String text = cs.toString();
        if (text.trim().length() == 0) {
            Toast.makeText(this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public class SearchLocationThread implements Runnable {

        private View promptsView;

        SearchLocationThread(View promptsView) {
            this.promptsView = promptsView;
        }

        @Override
        public void run() {
            processLocationSearch(promptsView);
        }

    }

    private void processLocationSearch(final View promptsView) {

        try {

            locationJSON = null;

            EditText userInput = promptsView
                    .findViewById(R.id.locationInput);

            String location = userInput.getText().toString();

            if (location.trim().length() == 0) {
                setPromptLocationDetailsUIThread(promptsView, "Search a location", true);
                return;
            }

            location = URLEncoder.encode(location, "UTF-8");
            URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address="+location+"&key=AIzaSyDpV6tWkn7mmGfDVuI3qAENgKhNDD_4Onc");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);

            int responseCode = conn.getResponseCode();

            if (responseCode != HttpURLConnection.HTTP_OK) {
                setPromptLocationDetailsUIThread(promptsView, "There seems to be a problem on Google API", true);
                return;
            }

            String response = "";
            String line;
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            while ((line = br.readLine()) != null) {
                response += line;
            }

            JSONObject json = new JSONObject(response);
            JSONArray results = json.getJSONArray("results");
            if (results.length() == 0) {
                setPromptLocationDetailsUIThread(promptsView, "Cannot find given location", true);
                return;
            }
            if (results.length() > 1) {
                setPromptLocationDetailsUIThread(promptsView, "Narrow down your location for there are too many result.", true);
                return;
            }
            JSONObject result0 = results.getJSONObject(0);
            JSONObject geometry = result0.getJSONObject("geometry");
            locationJSON = geometry.getJSONObject("location");

            final String formattedAddress = result0.getString("formatted_address");

            setPromptLocationDetailsUIThread(promptsView, "The location searched is on: " + formattedAddress, false);

        } catch (Exception e) {
            setPromptLocationDetailsUIThread(promptsView, "Error encountered, make sure there is internet connection.", true);
        }

    }

    private boolean setLongLat(View promptsView) {
        if (null == locationJSON) {
            setPromptLocationDetails(promptsView, "Search a valid location first", true);
            return false;
        }
        try {
            longitudeView.setText(""+locationJSON.getDouble("lng"));
            latitudeView.setText(""+locationJSON.getDouble("lat"));
        } catch (Exception e) {
            setPromptLocationDetails(promptsView, "There seems to be a problem on Google API response", true);
            return false;
        }

        return true;
    }

    private void setPromptLocationDetailsUIThread(final View promptsView, final String details, final boolean error) {
        runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        setPromptLocationDetails(promptsView, details, error);
                    }
                }
        );
    }

    private void setPromptLocationDetails(View promptsView, String details, boolean error) {
        LinearLayout linearLayout = promptsView.findViewById(R.id.searchLocationDetails);
        linearLayout.removeAllViews();

        TextView textView = new TextView(this);
        textView.setText(details);
        textView.setTextSize(20);
        textView.setTextColor(Color.BLACK);
        if (error) {
            textView.setTextColor(Color.RED);
        }

        linearLayout.addView(textView);
    }

    @Override
    public void onBackPressed() {}

}
