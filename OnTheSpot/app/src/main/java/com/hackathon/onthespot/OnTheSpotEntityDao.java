package com.hackathon.onthespot;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by djpichay on 2/6/18.
 */
@Dao
public interface OnTheSpotEntityDao {

    @Query("select * from onthespot")
    List<OnTheSpotEntity> getAll();

    @Insert
    void insert(OnTheSpotEntity entity);

    @Query("delete from onthespot where id = :id")
    void delete(String id);

}
