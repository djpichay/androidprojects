package com.hackathon.onthespot;

/**
 * Created by djpichay on 2/13/18.
 */

public enum SettingsEnum {

    CHECK_INTERVAL("checkInterval"),
    RADIUS("radius"),
    MESSAGE_CYCLE("messageCycle"),
    ;

    private String column;

    SettingsEnum(String column) {
        this.column = column;
    }

    public String getColumn() {
        return this.column;
    }

}
