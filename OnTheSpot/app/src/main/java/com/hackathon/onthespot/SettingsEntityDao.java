package com.hackathon.onthespot;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

/**
 * Created by djpichay on 2/12/18.
 */
@Dao
public interface SettingsEntityDao {

    @Query("select count(id) from settings")
    int count();

    @Query("select * from settings where name = :name")
    SettingsEntity select(String name);

    @Insert
    void insert(SettingsEntity entity);

    @Query("delete from settings")
    void deleteAll();

}
