package kom.rebelde.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/3/18.
 */
public class Context implements Serializable {

    private int numberOfPlayers;
    private List<Player> players;
    private int currentPlayer;
    private List<Boolean> roleButtonPressed;
    private int missionNumber;
    private int missionFailedTracker;
    private int rebelScore;
    private int spiesScore;
    private int missionVetoTracker;

    public Context(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        this.players = new ArrayList<>();
        roleButtonPressed = new ArrayList<>();
        missionNumber = 1;
        rebelScore = 0;
        spiesScore = 0;
        missionFailedTracker = 0;
        missionVetoTracker = 0;
        for(int i = 0; i < numberOfPlayers; i++) {
            roleButtonPressed.add(false);
        }
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addToPlayers(Player player) {
        players.add(player);
    }

    public Player getCurrentPlayer() {
        if (currentPlayer == numberOfPlayers) {
            currentPlayer = 0;
            return null;
        }
        return players.get(currentPlayer);
    }

    public void incrementCurrentPlayer() {
        currentPlayer++;
    }

    public boolean isRoleButtonPressed() {
        return roleButtonPressed.get(currentPlayer);
    }

    public void setRoleButtonPressed() {
        roleButtonPressed.set(currentPlayer, true);
    }

    public int getMissionNumber() {
        return missionNumber;
    }

    public void incrementMissionNumber() {
        missionNumber++;
    }

    public void incrementRebelScore() {
        rebelScore++;
    }

    public boolean areRebelsVictor() {
        return (rebelScore==3);
    }

    public void incrementSpiesScore() {
        spiesScore++;
    }

    public boolean areSpiesVictor() {
        return (spiesScore==3 || missionVetoTracker==5);
    }

    public boolean isMissionFailed() {
        if (missionNumber != 4) {
            return (missionFailedTracker >= 1);
        }
        return (missionFailedTracker >= 2);
    }

    public void incrementMissionFailedTracker() {
        missionFailedTracker++;
    }

    public void resetMissionFailedTracker() {
        missionFailedTracker = 0;
    }

    public int getMissionVetoTracker() {
        return missionVetoTracker;
    }

    public void incrementMissionVetoTracker() {
        missionVetoTracker++;
    }

    public void resetMissionVetoTracker() {
        missionVetoTracker = 0;
    }

}
