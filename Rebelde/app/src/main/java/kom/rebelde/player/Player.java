package kom.rebelde.player;

import java.io.Serializable;

/**
 * Created by djpichay on 1/3/18.
 */
public class Player implements Serializable {

    private String name;
    private boolean spy;
    private boolean lead;
    private boolean missionMember;

    public Player(boolean spy) {
        this.spy = spy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSpy() {
        return spy;
    }

    public boolean hasLead() {
        return lead;
    }

    public void setLead(boolean lead) {
        this.lead = lead;
    }

    public boolean isMissionMember() {
        return missionMember;
    }

    public void setMissionMember(boolean missionMember) {
        this.missionMember = missionMember;
    }

}
