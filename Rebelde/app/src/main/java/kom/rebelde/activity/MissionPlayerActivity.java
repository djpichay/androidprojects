package kom.rebelde.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/4/18.
 */
public class MissionPlayerActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Player player = context.getCurrentPlayer();

        TextView nameView = (TextView) findViewById(R.id.mission_player_text);

        if (null == player) {
            Intent intent = new Intent(this, MissionSummaryActivity.class);
            intent.putExtra(CONTEXT, context);
            startActivity(intent);
            return;
        }

        nameView.setText(player.getName());

        if (!player.isMissionMember()) {
            next();
            return;
        }

    }

    @Override
    protected int getContentView() {
        return R.layout.mission_player;
    }

    public void next(View view) {
        RadioButton successRadio = (RadioButton) findViewById(R.id.tagumpay_radio);
        boolean success = successRadio.isChecked();
        RadioButton failureRadio = (RadioButton) findViewById(R.id.biguin_radio);
        boolean failure = failureRadio.isChecked();
        if (!success && !failure) {
            Toast.makeText(this, "Mamili kung Tagumpay o Bigo ang misyon", Toast.LENGTH_SHORT).show();
            return;
        }
        Player player = context.getCurrentPlayer();
        if (player.isSpy() && failure) {
            context.incrementMissionFailedTracker();
        }
        next();
    }

    public void check(View view) {
        Player player = context.getCurrentPlayer();
        if (!player.isSpy()) {
            RadioButton successRadio = (RadioButton) findViewById(R.id.tagumpay_radio);
            successRadio.setChecked(true);
        }
    }

    public void next() {
        context.incrementCurrentPlayer();
        Intent intent = new Intent(this, MissionPlayerActivity.class);
        intent.putExtra(CONTEXT, context);
        startActivity(intent);
    }

}
