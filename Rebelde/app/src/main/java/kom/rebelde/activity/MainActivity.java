package kom.rebelde.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import kom.rebelde.context.Context;
import kom.rebelde.player.Player;

public class MainActivity extends BaseActivity {

    protected int getContentView() {
        return R.layout.activity_main;
    }

    public void shuffle(View view) {

        try {

            EditText numberOfPlayersText = (EditText) findViewById(R.id.bilang_ng_manlalaro_text);

            String numberOfPlayersString = numberOfPlayersText.getText().toString();

            double numberOfPlayers = Double.parseDouble(numberOfPlayersString);

            if (numberOfPlayers < 5) {
                Toast.makeText(this, "Ang pinakamaliit na bilang ng manlalaro ay 5", Toast.LENGTH_SHORT).show();
                return;
            }

            Context context = new Context((int)numberOfPlayers);

            int numberOfSpies = (int) Math.ceil(numberOfPlayers/3);

            Set<Integer> spies = new HashSet<>();

            while (spies.size() != numberOfSpies) {
                spies.add(ThreadLocalRandom.current().nextInt(0, (int)numberOfPlayers));
            }

            for (int i = 0; i < numberOfPlayers; i++) {
                context.addToPlayers(new Player(spies.contains(i)));
            }

            Intent intent = new Intent(this, PlayerActivity.class);
            intent.putExtra(CONTEXT, context);
            startActivity(intent);

        } catch (Exception e) {
            Toast.makeText(this, "Maglagay ng bilang ng manlalaro", Toast.LENGTH_SHORT).show();
        }

    }

}
