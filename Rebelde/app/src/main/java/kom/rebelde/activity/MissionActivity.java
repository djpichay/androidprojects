package kom.rebelde.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import kom.rebelde.context.Context;
import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/3/18.
 */
public class MissionActivity extends BaseActivity {

    private LinearLayout missionControlLayout;

    private int requiredMissionMembers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView missionTextView = (TextView) findViewById(R.id.mission_text);

        int missionNumber = context.getMissionNumber();

        StringBuilder sb = new StringBuilder();
        if (missionNumber == 1) {
            sb.append("Unang Misyon");
        } else if (missionNumber == 2) {
            sb.append("Ikalawang Misyon");
        } else if (missionNumber == 3) {
            sb.append("Ikatlong Misyon");
        } else if (missionNumber == 4)  {
            sb.append("Ikaapat na Misyon");
        } else {
            sb.append("Huling Misyon");
        }

        int missionVetoTracker = context.getMissionVetoTracker();

        if (missionVetoTracker == 1) {
            sb.append(": Ikalawang Pagpupulong");
        } else if (missionVetoTracker == 2) {
            sb.append(": Ikatlong Pagpupulong");
        } else if (missionVetoTracker == 3) {
            sb.append(": Ikaapat na Pagpupulong");
        } else if (missionVetoTracker == 4)  {
            sb.append(": Huling Pagpupulong");
        }

        missionTextView.setText(sb.toString());

        TextView missionLeaderTextView = (TextView) findViewById(R.id.mission_leader_text);

        List<Player> players = context.getPlayers();

        boolean allPlayersAreLeaders = true;

        for (Player player : players) {
            allPlayersAreLeaders &= player.hasLead();
        }
        if (allPlayersAreLeaders) {
            for (Player player : players) {
                player.setLead(false);
            }
        }

        boolean leaderSelected = false;

        if (missionNumber == 1 && missionVetoTracker == 0) {
            leaderSelected = true;
            missionLeaderTextView.setText("Tagapangasiwa: " + getIntent().getStringExtra("firstLeader"));
        }

        while(!leaderSelected) {
            int leader = ThreadLocalRandom.current().nextInt(0, players.size());
            Player potentialLeader = players.get(leader);
            if (!potentialLeader.hasLead()) {
                potentialLeader.setLead(true);
                missionLeaderTextView.setText("Tagapangasiwa: " + potentialLeader.getName());
                leaderSelected = true;
            }
        }

        int numberOfSpies = (int) Math.ceil((double)players.size()/3);
        requiredMissionMembers = numberOfSpies;

        if (missionNumber!=1 && missionNumber != 3) {
            requiredMissionMembers++;
        }

        TextView missionReminderText = (TextView) findViewById(R.id.mission_member_reminder_text);
        missionReminderText.setText("Mamili ng " + requiredMissionMembers + " na ipapadala sa misyon");

        missionControlLayout = (LinearLayout) findViewById(R.id.mission_control_id);

        for (Player player : context.getPlayers()) {
            CheckBox checkBox = new CheckBox(getApplication());
            checkBox.setText(player.getName());
            checkBox.setTextColor(Color.WHITE);
            checkBox.setId(Math.abs(player.getName().hashCode()));
            missionControlLayout.addView(checkBox);
        }

    }

    protected int getContentView() {
        return R.layout.mission;
    }

    public void next(View view) {

        int membersSelected = 0;

        for (Player player : context.getPlayers()) {
            CheckBox checkbox = (CheckBox) findViewById(Math.abs(player.getName().hashCode()));
            if (checkbox.isChecked()) {
                membersSelected++;
            }
            player.setMissionMember(checkbox.isChecked());
        }

        if (membersSelected != requiredMissionMembers) {
            Toast.makeText(this, requiredMissionMembers + " ang kinakailangang ipadala sa misyon.", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, VotingActivity.class);
        intent.putExtra(CONTEXT, context);
        startActivity(intent);

    }

}
