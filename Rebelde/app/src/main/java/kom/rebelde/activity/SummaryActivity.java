package kom.rebelde.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/4/18.
 */
public class SummaryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> rebels = new ArrayList<>();
        List<String> spies = new ArrayList<>();

        for (Player player : context.getPlayers()) {
            if (player.isSpy()) {
                spies.add(player.getName());
            } else {
                rebels.add(player.getName());
            }
        }

        LinearLayout layout = (LinearLayout) findViewById(R.id.summary_id);

        if (context.areRebelsVictor()) {
            layout.addView(createTextView("Mga Rebelde:"));
            for(String rebel : rebels) {
                layout.addView(createTextView(rebel));
            }
            layout.addView(createTextView(""));
            layout.addView(createTextView("Mga Espiya:"));
            for(String spy : spies) {
                layout.addView(createTextView(spy));
            }
        } else {
            layout.addView(createTextView("Mga Espiya:"));
            for(String spy : spies) {
                layout.addView(createTextView(spy));
            }
            layout.addView(createTextView(""));
            layout.addView(createTextView("Mga Rebelde:"));
            for(String rebel : rebels) {
                layout.addView(createTextView(rebel));
            }
        }

    }

    private TextView createTextView(String text) {
        TextView textView = new TextView(getApplication());
        textView.setText(text);
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(20F);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    public void next(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        return;
    }

    @Override
    protected int getContentView() {
        return R.layout.summary;
    }

}
