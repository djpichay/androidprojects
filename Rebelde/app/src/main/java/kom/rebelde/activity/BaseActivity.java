package kom.rebelde.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import kom.rebelde.context.Context;

/**
 * Created by djpichay on 1/4/18.
 */
public abstract class BaseActivity extends AppCompatActivity {

    final String CONTEXT = "context";

    protected Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = (Context) getIntent().getSerializableExtra("context");
    }

    protected abstract int getContentView();

    @Override
    public void onBackPressed() {
        // do nothing
    }

}
