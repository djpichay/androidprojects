package kom.rebelde.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by djpichay on 1/4/18.
 */
public class MissionSummaryActivity extends BaseActivity {

    private boolean terminate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView missionStatusText = (TextView) findViewById(R.id.mission_summary_text);

        boolean vetoLimit = getIntent().getBooleanExtra("VetoLimit", false);

        if (vetoLimit) {
            missionStatusText.setText("Limang beses ng hindi nagkasundo sa misyon.");
        } else {
            if (!context.isMissionFailed()) {
                missionStatusText.setText("Matagumpay ang misyon!");
                context.incrementRebelScore();
            } else {
                missionStatusText.setText("Nabigo ang misyon!");
                context.incrementSpiesScore();
            }
        }

        TextView endText = (TextView) findViewById(R.id.ending_text);

        if (context.areRebelsVictor()) {
            endText.setText("Tagumpay para sa mga Rebelde!");
            terminate = true;
        } else if (context.areSpiesVictor()) {
            endText.setText("Tagumpay para sa mga Espiya!");
            terminate = true;
        } else {
            endText.setText("Maghanda sa susunod na Misyon");
        }

        context.incrementMissionNumber();
        context.resetMissionFailedTracker();
    }

    @Override
    protected int getContentView() {
        return R.layout.mission_summary;
    }

    public void next(View view) {
        if (terminate) {
            Intent intent = new Intent(this, SummaryActivity.class);
            intent.putExtra(CONTEXT, context);
            startActivity(intent);
            return;
        }
        Intent intent = new Intent(this, MissionActivity.class);
        intent.putExtra(CONTEXT, context);
        startActivity(intent);
        return;
    }

}
