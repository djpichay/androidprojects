package kom.rebelde.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/3/18.
 */
public class PlayerActivity extends BaseActivity {

    private EditText pangalanText;

    private Button revealRoleButton;

    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (null == context.getCurrentPlayer()) {
            Intent intent = new Intent(this, SpyIntroduceActivity.class);
            intent.putExtra(CONTEXT, context);
            startActivity(intent);
            return;
        }

        pangalanText = (EditText) findViewById(R.id.pangalan_text);
        revealRoleButton = (Button) findViewById(R.id.papel_button);
        nextButton = (Button) findViewById(R.id.susunod_na_manlalaro_button);

        String name = getIntent().getStringExtra("name");
        if (null != name && name.length() > 0) {
            pangalanText.setText(name);
        }

        if (context.isRoleButtonPressed()) {
            revealRoleButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.VISIBLE);
        }

    }

    public void next(View view) {

        String name = pangalanText.getText().toString();

        if (name.trim().length() == 0) {
            Toast.makeText(this, "Maglagay ng pangalan ng manlalaro", Toast.LENGTH_SHORT).show();
            return;
        }

        Player player = context.getCurrentPlayer();
        player.setName(name);

        context.incrementCurrentPlayer();

        Intent intent = new Intent(this, PlayerActivity.class);
        intent.putExtra("context", context);
        startActivity(intent);

    }

    public void revealAlignment(View view) {

        String name = pangalanText.getText().toString();
        context.setRoleButtonPressed();

        if (name.trim().length() == 0) {
            Toast.makeText(this, "Maglagay ng pangalan ng manlalaro", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, RoleActivity.class);
        intent.putExtra(CONTEXT, context);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    protected int getContentView() {
        return R.layout.player;
    }

}
