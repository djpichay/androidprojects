package kom.rebelde.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/4/18.
 */
public class VotingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final List<String> missionMembers = new ArrayList<>();
        for (Player player : context.getPlayers()) {
            if (player.isMissionMember()) {
                missionMembers.add(player.getName());
            }
        }

        TextView votingText = (TextView) findViewById(R.id.voting_text);
        StringBuilder sb = new StringBuilder("Ipadala sa misyon sina: ");
        for (String name : missionMembers) {
            sb.append(name + ", ");
        }
        votingText.setText(sb.substring(0, sb.lastIndexOf(",")));

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.yes_no_id);
        linearLayout.setGravity(Gravity.CENTER);

        LinearLayout choicePanel = new LinearLayout(this);
        choicePanel.setGravity(Gravity.RIGHT);
        choicePanel.setOrientation(LinearLayout.VERTICAL);

        LinearLayout votePanel = new LinearLayout(this);
        votePanel.setGravity(Gravity.LEFT);
        votePanel.setOrientation(LinearLayout.VERTICAL);

        createVotingPanel("Ipadala", choicePanel, votePanel);
        createVotingPanel("Ulitin", choicePanel, votePanel);

        linearLayout.addView(choicePanel);
        linearLayout.addView(votePanel);

    }

    private void createVotingPanel(String vote, LinearLayout choicePanel, LinearLayout votePanel) {
        LinearLayout choiceLayout = new LinearLayout(this);
        choiceLayout.setOrientation(LinearLayout.HORIZONTAL);
        choiceLayout.setGravity(Gravity.CENTER);

        TextView choiceName = new TextView(this);
        choiceName.setTextColor(Color.WHITE);
        choiceName.setText(vote);
        choiceName.setTextSize(20F);
        choiceLayout.addView(choiceName);

        Button dummyButton = new Button(this);
        dummyButton.setText("");
        dummyButton.setVisibility(View.INVISIBLE);
        choiceLayout.addView(dummyButton);

        choicePanel.addView(choiceLayout);

        LinearLayout voteLayout = new LinearLayout(this);
        voteLayout.setOrientation(LinearLayout.HORIZONTAL);
        voteLayout.setGravity(Gravity.CENTER);

        final EditText choiceVote = new EditText(this);
        choiceVote.setEnabled(false);
        choiceVote.setText("0");
        choiceVote.setTextColor(Color.WHITE);
        choiceVote.setId(vote.length());

        final int editTextId = choiceVote.getId();

        Button decrementButton = new Button(this);
        decrementButton.setLayoutParams(new LinearLayout.LayoutParams(100, 100));
        decrementButton.setTextSize(14F);
        decrementButton.setText("-");
        decrementButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int vote = Integer.parseInt(choiceVote.getText().toString()) - 1;
                        if (vote < 0) {
                            vote = 0;
                        }
                        setEditTextText(editTextId, "" + vote);
                    }
                }
        );
        voteLayout.addView(decrementButton);

        voteLayout.addView(choiceVote);

        Button incrementButton = new Button(this);
        incrementButton.setLayoutParams(new LinearLayout.LayoutParams(100, 100));
        incrementButton.setTextSize(14F);
        incrementButton.setText("+");
        incrementButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setEditTextText(editTextId, "" + (Integer.parseInt(choiceVote.getText().toString()) + 1));
                    }
                }
        );

        voteLayout.addView(incrementButton);

        votePanel.addView(voteLayout);
    }

    public void setEditTextText(int id, String text) {
        EditText editText = (EditText) findViewById(id);
        editText.setText(text);
    }

    @Override
    protected int getContentView() {
        return R.layout.voting;
    }

    public void next(View view) {

        EditText yesVotesEditText = (EditText) findViewById("Ipadala".length());
        int yesVotes = Integer.parseInt(yesVotesEditText.getText().toString());

        EditText noVotesEditText = (EditText) findViewById("Ulitin".length());
        int noVotes = Integer.parseInt(noVotesEditText.getText().toString());

        int numPlayers = context.getPlayers().size();
        if ((yesVotes+noVotes) != numPlayers) {
            Toast.makeText(this, numPlayers + " ang inaasahang boto.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (yesVotes > noVotes) {
            context.resetMissionVetoTracker();
            Intent intent = new Intent(this, MissionPlayerActivity.class);
            intent.putExtra(CONTEXT, context);
            startActivity(intent);
            return;
        }

        context.incrementMissionVetoTracker();

        if (context.areSpiesVictor()) {
            Intent intent = new Intent(this, MissionSummaryActivity.class);
            intent.putExtra(CONTEXT, context);
            intent.putExtra("VetoLimit", true);
            startActivity(intent);
            return;
        }

        Intent intent = new Intent(this, MissionActivity.class);
        intent.putExtra(CONTEXT, context);
        startActivity(intent);

    }

}
