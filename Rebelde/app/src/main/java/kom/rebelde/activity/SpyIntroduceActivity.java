package kom.rebelde.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/4/18.
 */
public class SpyIntroduceActivity extends BaseActivity {

    Player firstLeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView firstLeaderText = (TextView) findViewById(R.id.first_leader_text);

        List<Player> players = context.getPlayers();

        int random = ThreadLocalRandom.current().nextInt(0, players.size());

        firstLeader = players.get(random);
        firstLeader.setLead(true);

        firstLeaderText.setText("Tagapangasiwa: " + firstLeader.getName());
    }

    public void next(View view) {
        Intent intent = new Intent(this, MissionActivity.class);
        intent.putExtra(CONTEXT, context);
        intent.putExtra("firstLeader", firstLeader.getName());
        startActivity(intent);
    }

    protected int getContentView() {
        return R.layout.spy_introduce;
    }

}
