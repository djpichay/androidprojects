package kom.rebelde.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kom.rebelde.player.Player;

/**
 * Created by djpichay on 1/3/18.
 */
public class RoleActivity extends BaseActivity {

    private TextView roleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        roleText = (TextView) findViewById(R.id.role_text);

        Player player = context.getCurrentPlayer();

        if (player.isSpy()) {
            roleText.setText("Ikaw ay isa sa mga Espiya!");
        } else {
            roleText.setText("Ikaw ay isa sa mga Rebelde!");
        }

    }

    protected int getContentView() {
        return R.layout.role;
    }

    public void back(View view) {
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.putExtra(CONTEXT, context);
        intent.putExtra("name", getIntent().getStringExtra("name"));
        startActivity(intent);
    }

}
