package kom.espiya.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import kom.espiya.context.Context;

/**
 * Created by djpichay on 12/30/17.
 */
public class LocationActivity extends AppCompatActivity {

    private Context context;

    private TextView locationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = (Context) getIntent().getSerializableExtra("context");

        locationText = (TextView) findViewById(R.id.location_text);

        if (context.isCurrentPlayerSpy()) {
            locationText.setText("Ikaw ang Espiya!");
        } else {
            locationText.setText("Ang lugar ng operasyon ay ang " + context.getPlace().getName());
        }

    }

    public void back(View view) {
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.putExtra("context", context);
        intent.putExtra("name", getIntent().getStringExtra("name"));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

}
