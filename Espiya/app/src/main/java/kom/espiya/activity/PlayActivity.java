package kom.espiya.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import kom.espiya.context.Context;
import kom.espiya.enums.Place;

/**
 * Created by djpichay on 12/30/17.
 */
public class PlayActivity extends AppCompatActivity {

    private Context context;

    private TextView possibleLocationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = (Context) getIntent().getSerializableExtra("context");

        possibleLocationText = (TextView) findViewById(R.id.possible_location_text);

        List<String> locationList = new ArrayList<>();
        locationList.add(context.getPlace().getName());

        Place[] places = Place.values();

        while (locationList.size() != 7) {
            int random = ThreadLocalRandom.current().nextInt(0, places.length);
            String placeName = places[random].getName();
            if (!locationList.contains(placeName)) {
                locationList.add(placeName);
            }
        }

        Collections.shuffle(locationList);

        StringBuffer sb = new StringBuffer("Posibleng Lokasyon: ");
        for (String place : locationList) {
            sb.append(place + "\n");
        }

        possibleLocationText.setText(sb.toString());
    }

    public void reveal(View view) {

        new AlertDialog.Builder(this)
                .setTitle("Alamin")
                .setMessage("Gusto mo ng makita kung sino ang espiya at saan ang lokasyon?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        revealOnClick();
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    public void revealOnClick() {
        Intent intent = new Intent(this, SpyActivity.class);
        intent.putExtra("context", context);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

}
