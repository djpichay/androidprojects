package kom.espiya.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import kom.espiya.context.Context;
import kom.espiya.entity.Player;

/**
 * Created by djpichay on 12/30/17.
 */
public class SpyActivity extends AppCompatActivity {

    private Context context;

    private TextView spyText;

    private TextView locationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spy);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = (Context) getIntent().getSerializableExtra("context");

        spyText = (TextView) findViewById(R.id.spy_text);

        for (Player player : context.getPlayers()) {
            if (player.isSpy()) {
                spyText.setText("Ang Espiya ay si " + player.getName());
            }
        }

        locationText = (TextView) findViewById(R.id.location_text);
        locationText.setText("Ang Lokasyon ay ang " + context.getPlace().getName());

    }

    public void finish(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

}
