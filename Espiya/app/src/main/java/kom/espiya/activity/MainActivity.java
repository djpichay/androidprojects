package kom.espiya.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.ThreadLocalRandom;

import kom.espiya.context.Context;
import kom.espiya.enums.Place;

public class MainActivity extends AppCompatActivity {

    private EditText numberOfPlayersText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        numberOfPlayersText = (EditText) findViewById(R.id.bilang_ng_manlalaro_text);
    }

    public void shuffle(View view) {

        try {

            int numberOfPlaces = Place.values().length;

            int randomPlace = ThreadLocalRandom.current().nextInt(0, numberOfPlaces);

            Place place = Place.values()[randomPlace];

            String numberOfPlayersString = numberOfPlayersText.getText().toString();

            int numberOfPlayers = Integer.parseInt(numberOfPlayersString);

            int spyPosition = ThreadLocalRandom.current().nextInt(0, numberOfPlayers);

            Context context = new Context(numberOfPlayers, place, spyPosition);

            Intent intent = new Intent(this, PlayerActivity.class);
            intent.putExtra("context", context);
            startActivity(intent);

        } catch (Exception e) {
            Toast.makeText(this, "Maglagay ng bilang ng manlalaro", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

}
