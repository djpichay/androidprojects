package kom.espiya.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kom.espiya.context.Context;
import kom.espiya.entity.Player;

/**
 * Created by djpichay on 12/30/17.
 */
public class PlayerActivity extends AppCompatActivity {

    private Context context;

    private EditText pangalanText;

    private Button revealLocationButton;

    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        context = (Context) getIntent().getSerializableExtra("context");

        if (context.getCurrentPlayer() == context.getNumberOfPlayers()) {
            Intent intent = new Intent(this, PlayActivity.class);
            intent.putExtra("context", context);
            startActivity(intent);
            return;
        }

        pangalanText = (EditText) findViewById(R.id.pangalan_text);
        revealLocationButton = (Button) findViewById(R.id.lokasyon_button);
        nextButton = (Button) findViewById(R.id.susunod_na_manlalaro_button);

        String name = getIntent().getStringExtra("name");
        if (null != name && name.length() > 0) {
            pangalanText.setText(name);
        }

        if (context.isLocationButtonPressed()) {
            revealLocationButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.VISIBLE);
        }

    }

    public void next(View view) {

        String name = pangalanText.getText().toString();

        if (name.trim().length() == 0) {
            Toast.makeText(this, "Maglagay ng pangalan ng manlalaro", Toast.LENGTH_SHORT).show();
            return;
        }

        Player player = new Player(name, context.isCurrentPlayerSpy());
        context.addToPlayers(player);
        context.incrementCurrentPlayer();

        Intent intent = new Intent(this, PlayerActivity.class);
        intent.putExtra("context", context);
        startActivity(intent);

    }

    public void revealLocation(View view) {

        String name = pangalanText.getText().toString();
        context.setLocationButtonPressed();

        if (name.trim().length() == 0) {
            Toast.makeText(this, "Maglagay ng pangalan ng manlalaro", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, LocationActivity.class);
        intent.putExtra("context", context);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

}
