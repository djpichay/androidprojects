package kom.espiya.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kom.espiya.entity.Player;
import kom.espiya.enums.Place;

/**
 * Created by djpichay on 12/30/17.
 */
public class Context implements Serializable {

    private int numberOfPlayers;
    private Place place;
    private int spyPosition;
    private List<Player> players;
    private int currentPlayer;
    private List<Boolean> locationButtonPressed;

    public Context(int numberOfPlayers, Place place, int spyPosition) {
        this.numberOfPlayers = numberOfPlayers;
        this.place = place;
        this.spyPosition = spyPosition;
        this.players = new ArrayList<>();
        currentPlayer = 0;
        locationButtonPressed = new ArrayList<>();
        for (int i = 0; i < numberOfPlayers; i++) {
            locationButtonPressed.add(false);
        }
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public Place getPlace() {
        return place;
    }

    public int getSpyPosition() {
        return spyPosition;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addToPlayers(Player player) {
        players.add(player);
    }

    public int getCurrentPlayer() {
        return currentPlayer;
    }

    public void incrementCurrentPlayer() {
        currentPlayer++;
    }

    public boolean isLocationButtonPressed() {
        return locationButtonPressed.get(currentPlayer);
    }

    public void setLocationButtonPressed() {
        locationButtonPressed.set(currentPlayer, true);
    }

    public boolean isCurrentPlayerSpy() {
        return currentPlayer == spyPosition;
    }

}
