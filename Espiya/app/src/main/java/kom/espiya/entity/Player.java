package kom.espiya.entity;

import java.io.Serializable;

/**
 * Created by djpichay on 12/30/17.
 */
public class Player implements Serializable {

    private String name;
    private boolean spy;

    public Player(String name, boolean spy) {
        this.name = name;
        this.spy = spy;
    }

    public String getName() {
        return name;
    }

    public boolean isSpy() {
        return spy;
    }

}
