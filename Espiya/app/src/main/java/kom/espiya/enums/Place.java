package kom.espiya.enums;

/**
 * Created by djpichay on 12/30/17.
 */
public enum Place {

    PALENGKE("Palengke"),
    SIMBAHAN("Simbahan"),
    AKLATAN("Aklatan"),
    PAARALAN("Paaralan"),
    LABORATORYO("Laboratoryo"),
    PARADAHAN("Paradahan"),
    KAINAN("Kainan"),
    PARKE("Parke"),
    PALIPARAN("Paliparan"),
    BILANGGUAN("Bilangguan"),
    BANGKO("Bangko"),
    PASUGALAN("Pasugalan"),
    BARKO("Barko"),
    SINEHAN("Sinehan"),
    PULISYA("Pulisya"),
    MUSEO("Museo"),
    MINAHAN("Minahan"),
    SEMENTERYO("Sementeryo"),
    DAUNGAN("Daungan"),
    HARDIN("Hardin"),
    PABRIKA("Pabrika"),
    KARERAHAN("Karerahan"),
    MANSIYON("Mansiyon"),
    TREN("Tren"),
    TALYER("Talyer"),
    PAMILIHAN("Pamilihan"),
    TABING_DAGAT("Tabing Dagat"),
    TABING_ILOG("Tabing Ilog"),
    ;

    private String name;

    Place(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
